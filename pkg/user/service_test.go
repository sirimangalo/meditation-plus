package user

import (
	"context"
	"database/sql"
	"fmt"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/timezone"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func TestListOnline(t *testing.T) {
	user := NewFakeUser(1)
	userB := NewFakeUser(2)
	user.DisplayName = "MyTestDisplayName"

	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatalf("Error creating request: %v", err)
	}

	ctx := context.WithValue(request.Context(), security.UserContextKey, user)
	request = request.WithContext(ctx)
	service := &Service{}
	session := &test.FakeSessionStore{}
	templating := template.NewTemplatingWithFilter("test", session, "user")
	userRepository := &FakeRepository{Users: []model.User{*user, *userB}}

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		service.ListOnline(w, r, userRepository, templating)
	})

	handler.ServeHTTP(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
	if !strings.Contains(recorder.Body.String(), user.DisplayName) {
		t.Fatalf("Body did not contain %s", user.DisplayName)
	}
}

func TestCountOnline(t *testing.T) {
	user := NewFakeUser(1)
	userB := NewFakeUser(2)

	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatalf("Error creating request: %v", err)
	}

	ctx := context.WithValue(request.Context(), security.UserContextKey, user)
	request = request.WithContext(ctx)
	service := &Service{}
	userRepository := &FakeRepository{Users: []model.User{*user, *userB}}

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		service.CountOnline(w, r, userRepository)
	})

	handler.ServeHTTP(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
	test.AssertEquals(t, "2", recorder.Body.String())
}

func TestShow(t *testing.T) {
	user := NewFakeUser(1)
	userB := NewFakeUser(2)

	cases := map[string]struct {
		User    *model.User
		CurUser *model.User
		Result  int
	}{
		"404 on invalid user":            {CurUser: user, Result: http.StatusNotFound},
		"200 on valid user":              {User: user, CurUser: user, Result: http.StatusOK},
		"200 on valid user but no stats": {User: user, CurUser: userB, Result: http.StatusOK},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.CurUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &FakeRepository{User: testCase.User}
		templating := template.NewTemplatingWithFilter("test", session, "user")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.ShowProfile(w, r, userRepository, userRepository, templating)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}

		body, err := html.Parse(recorder.Body)
		if err != nil {
			t.Fatalf("Could not parse body: %v", err)
		}
		node := test.GetFirstElementByClassName(body, "stats")
		if recorder.Code == http.StatusOK && testCase.CurUser == testCase.User && node == nil {
			t.Fatal("Expected .stats to exist.")
		}

		if recorder.Code == http.StatusOK && testCase.CurUser != testCase.User && node != nil {
			t.Fatalf("Expected .stats not to exist.")
		}
	}
}

func TestAvatar(t *testing.T) {
	user := NewFakeUser(1)
	user.ImageId = sql.NullString{
		Valid:  true,
		String: "1",
	}

	cases := map[string]struct {
		User   *model.User
		Result int
	}{
		"404 on invalid user": {Result: http.StatusNotFound},
		"200 on valid user":   {User: user, Result: http.StatusOK},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest("GET", "/avatar/1/100", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, NewFakeUser(1))
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &FakeRepository{User: testCase.User}
		imageGetter := &FakeImageGetter{}
		templating := template.NewTemplatingWithFilter("test", session, "user")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.GetAvatar(w, r, userRepository, templating, imageGetter)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}

	}
}

func TestEditProfile(t *testing.T) {
	user := &model.User{}

	validData := map[string]string{
		"displayName": "User",
		"timezone":    "UTC",
	}

	cases := map[string]struct {
		Method   string
		User     *model.User
		FormData map[string]string
		Result   int
	}{
		"200 on GET": {
			Method: "GET",
			Result: http.StatusOK,
			User:   user,
		},
		"200 on invalid data": {
			Method: "POST",
			User:   user,
			Result: http.StatusOK,
		},
		"302 on valid data": {
			Method:   "POST",
			User:     user,
			FormData: validData,
			Result:   http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", test.CreateFormData(testCase.FormData))
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		ctx := context.WithValue(request.Context(), security.UserContextKey, NewFakeUser(1))
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &FakeRepository{User: testCase.User}
		templating := template.NewTemplatingWithFilter("test", session, "user")
		timezone := &timezone.FakeService{}

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.EditProfile(w, r, userRepository, templating, session, timezone, userRepository)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestDeleteProfile(t *testing.T) {
	user := &model.User{}

	cases := map[string]struct {
		Method   string
		User     *model.User
		FormData map[string]string
		Result   int
	}{
		"302 on DELETE": {
			Method: "POST",
			Result: http.StatusSeeOther,
			User:   user,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, NewFakeUser(1))
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &FakeRepository{User: testCase.User}
		templating := template.NewTemplatingWithFilter("test", session, "user")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.DeleteProfile(w, r, userRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}
