package user

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
)

type DiscordImageGetter interface {
	GetImage(filepath string, discordUserId string, discordImageId string, size string) error
}

type HttpDiscordImageGetter struct{}

func (h *HttpDiscordImageGetter) GetImage(filepath string, discordUserId string, discordImageId string, size string) error {
	url := fmt.Sprintf("https://cdn.discordapp.com/avatars/%s/%s.webp?size=%s", discordUserId, discordImageId, size)

	response, err := http.Get(url)
	if err != nil {
		return err
	}
	if response == nil {
		return errors.New("Did not receive a response.")
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return err
	}

	file, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	return nil
}
