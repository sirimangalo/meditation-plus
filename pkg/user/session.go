package user

import (
	"log/slog"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"net/http"
)

func NewDatabaseSessionUserProvider(userIdQuery IdQuery, persistQuery PersistQuery, sessionStore security.SessionStore) *DatabaseSessionUserProvider {
	return &DatabaseSessionUserProvider{
		userIdQuery:        userIdQuery,
		persistQuery:       persistQuery,
		sessionDestroyer:   sessionStore,
		sessionValueGetter: sessionStore,
	}
}

type DatabaseSessionUserProvider struct {
	userIdQuery        IdQuery
	persistQuery       PersistQuery
	sessionDestroyer   security.SessionDestroyer
	sessionValueGetter security.SessionValueGetter
}

func (sup *DatabaseSessionUserProvider) GetUser(r *http.Request) security.SecurityUser {
	user, ok := sup.sessionValueGetter.Get(r, "user").(*model.User)

	if user == nil || !ok {
		return nil
	}

	dbUser, err := sup.userIdQuery.FindById(r.Context(), int(user.Id))
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not load session user from database", "error", err)
		return nil
	}

	if dbUser == nil || user.AuthHasChanged(dbUser) {
		err = sup.sessionDestroyer.Destroy(r)

		if err != nil {
			slog.ErrorContext(r.Context(), "Could not destroy session", "error", err)
		}

		return nil
	}

	dbUser.Active()

	err = sup.persistQuery.Persist(r.Context(), dbUser)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not update user", "error", err)
	}

	return dbUser
}
