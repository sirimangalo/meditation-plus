package user

import (
	"encoding/json"
	"fmt"
	"io"
	"meditation-plus/pkg/errors"
	"net/http"

	"golang.org/x/oauth2"
)

var discordBaseUrl = "https://discord.com/api"

type Guild struct {
	ID string `json:"id"`
}

func isUserInDiscordGuild(accessToken string, guildId string) (bool, error) {
	url := fmt.Sprintf("%s/users/@me/guilds", discordBaseUrl)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil || req == nil {
		return false, err
	}

	req.Header.Add("Authorization", "Bearer "+accessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil || resp == nil {
		return false, fmt.Errorf("request failed: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("invalid server response: %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return false, fmt.Errorf("could not read body: %v", err)
	}

	var guilds []Guild
	err = json.Unmarshal(body, &guilds)
	if err != nil {
		return false, fmt.Errorf("could not parse body: %v", err)
	}

	for _, guild := range guilds {
		if guild.ID == guildId {
			return true, nil
		}
	}

	return false, nil
}

type DiscordAccessTokenChecker struct {
	GuildId string
}

func NewDiscordAccessTokenChecker(guildId string) *DiscordAccessTokenChecker {
	return &DiscordAccessTokenChecker{
		GuildId: guildId,
	}
}

func (d *DiscordAccessTokenChecker) CheckAccessToken(accessToken *oauth2.Token, userInfo map[string]interface{}) error {
	if accessToken == nil || userInfo == nil {
		return fmt.Errorf("Invalid access token")
	}

	found, err := isUserInDiscordGuild(accessToken.AccessToken, d.GuildId)

	if err != nil {
		return fmt.Errorf("Could not query guilds: %v", err)
	}

	if !found {
		return errors.ErrUserNotInGuild
	}

	return nil
}
