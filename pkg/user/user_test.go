package user

import (
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/test"
	"testing"
)

func TestShouldCreateValidUser(t *testing.T) {
	values := make(map[string]interface{})
	values["id"] = "id"
	values["username"] = "username"
	values["email"] = "email"
	values["global_name"] = "global_name"
	user, err := UserFromDiscord(values)
	if err != nil {
		t.Fatalf("Expected error to be nil")
	}
	test.AssertEquals(t, user.UpstreamId, "id")
	test.AssertEquals(t, user.Username, "username")
	test.AssertEquals(t, user.Email, "email")
	test.AssertEquals(t, user.DisplayName, "global_name")
}

func TestShouldCreateUserWithoutGlobalName(t *testing.T) {
	values := make(map[string]interface{})
	values["id"] = "id"
	values["username"] = "username"
	values["email"] = "email"
	user, err := UserFromDiscord(values)
	if err != nil {
		t.Fatalf("Expected error to be nil")
	}
	test.AssertEquals(t, user.UpstreamId, "id")
	test.AssertEquals(t, user.Username, "username")
	test.AssertEquals(t, user.Email, "email")
	test.AssertEquals(t, user.DisplayName, "username")
}

func TestShouldReturnErrorOnInvalidData(t *testing.T) {
	values := make(map[string]interface{})
	values["id"] = "id"
	values["email"] = "email"
	values["global_name"] = "global_name"
	user, err := UserFromDiscord(values)
	if user != nil {
		t.Fatalf("Expected user to be nil")
	}
	test.AssertEquals(t, "username is invalid", err.Error())
}

func TestShouldErrorOnEmptyEmail(t *testing.T) {
	values := make(map[string]interface{})
	values["id"] = "id"
	values["username"] = "username"
	values["email"] = ""
	values["global_name"] = "global_name"
	user, err := UserFromDiscord(values)
	if user != nil {
		t.Fatalf("Expected user to be nil")
	}
	test.AssertEquals(t, security.ErrUserDataInvalid, err)
}

func TestIsGrantedUser(t *testing.T) {
	cases := map[string][]bool{
		model.USER_ROLES_USER:          {true, false, false, false},
		model.USER_ROLES_TEACHER:       {true, true, false, false},
		model.USER_ROLES_ADMIN:         {true, false, true, false},
		model.USER_ROLES_ADMIN_TEACHER: {true, true, true, true},
	}
	roles := []string{
		model.USER_ROLES_USER,
		model.USER_ROLES_TEACHER,
		model.USER_ROLES_ADMIN,
		model.USER_ROLES_ADMIN_TEACHER,
	}

	for role, bools := range cases {
		user := &model.User{Role: role}
		for key, expected := range bools {
			if user.IsGranted(roles[key]) != expected {
				t.Fatalf("Expected %v for role %s when user having role %s", expected, roles[key], role)
			}
		}
	}
}
