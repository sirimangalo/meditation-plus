package user

import (
	"context"
	"database/sql"
	"meditation-plus/pkg/database"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"time"
)

const USERS_PER_PAGE = 15

type IdQuery interface {
	FindById(ctx context.Context, id int) (*model.User, error)
}
type UsernameQuery interface {
	FindByUsername(ctx context.Context, username string) (*model.User, error)
}
type TeacherQuery interface {
	FindTeacher(ctx context.Context) ([]model.User, error)
}
type PaginateQuery interface {
	Paginate(ctx context.Context, page int, search string) ([]model.User, error)
}
type PersistQuery interface {
	Persist(ctx context.Context, u *model.User) error
}
type DeleteQuery interface {
	Delete(ctx context.Context, u *model.User) error
}
type CountQuery interface {
	Count(ctx context.Context, search string) (int, error)
}
type ListOnlineQuery interface {
	ListOnline(ctx context.Context) ([]model.User, error)
}
type StatsQuery interface {
	GetStats(ctx context.Context, userId int) (*MeditationStats, error)
}

type Repository struct{}

func NewRepository() *Repository {
	return &Repository{}
}

func (r *Repository) Persist(ctx context.Context, u *model.User) error {
	queries := model.New(database.Pool)

	if u.Id != 0 {
		_, err := queries.UpdateUser(ctx, model.UpdateUserParams{
			Username:     u.Username,
			Email:        u.Email,
			Role:         u.Role,
			DisplayName:  u.DisplayName,
			UpstreamId:   u.UpstreamId,
			Welcomed:     u.Welcomed,
			Timezone:     u.Timezone,
			ImageId:      u.ImageId,
			OldUserUid:   u.OldUserUid,
			LastActivity: u.LastActivity,
			CreatedAt:    u.CreatedAt,
			Id:           u.Id,
		})

		if err != nil {
			return err
		}
	} else {
		result, err := queries.CreateUser(ctx, model.CreateUserParams{
			Username:     u.Username,
			Email:        u.Email,
			Role:         u.Role,
			DisplayName:  u.DisplayName,
			UpstreamId:   u.UpstreamId,
			Welcomed:     u.Welcomed,
			Timezone:     u.Timezone,
			ImageId:      u.ImageId,
			OldUserUid:   u.OldUserUid,
			LastActivity: u.LastActivity,
			CreatedAt:    u.CreatedAt,
		})

		if err != nil {
			return err
		}

		lastId, err := result.LastInsertId()

		if err != nil {
			return err
		}

		u.Id = lastId
	}

	return nil
}

func (r *Repository) FindAll(ctx context.Context) ([]model.User, error) {
	queries := model.New(database.Pool)

	users, err := queries.ListUsers(ctx)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r *Repository) FindByUsername(ctx context.Context, username string) (*model.User, error) {
	queries := model.New(database.Pool)

	user, err := queries.FindUserByUsername(ctx, username)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *Repository) FindById(ctx context.Context, id int) (*model.User, error) {
	queries := model.New(database.Pool)

	user, err := queries.FindUserById(ctx, int64(id))

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *Repository) FindTeacher(ctx context.Context) ([]model.User, error) {
	queries := model.New(database.Pool)

	users, err := queries.ListTeacher(ctx, model.ListTeacherParams{
		Role:   model.USER_ROLES_TEACHER,
		Role_2: model.USER_ROLES_ADMIN_TEACHER,
	})

	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r *Repository) Paginate(ctx context.Context, page int, search string) ([]model.User, error) {
	queries := model.New(database.Pool)

	perPage := USERS_PER_PAGE

	users, err := queries.PaginateUsers(ctx, model.PaginateUsersParams{
		Limit:  int32(perPage),
		Offset: int32((page - 1) * perPage),
		Search: search,
	})

	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r *Repository) ProvideUserByIdentifier(identifier string) (security.SecurityUser, error) {
	queries := model.New(database.Pool)

	user, err := queries.FindUserByUpsteamId(context.Background(), identifier)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *Repository) Count(ctx context.Context, search string) (int, error) {
	queries := model.New(database.Pool)

	result, err := queries.CountUsers(ctx, model.CountUsersParams{Search: search})

	if err != nil {
		return 0, err
	}

	return int(result), nil
}

func (r *Repository) ListOnline(ctx context.Context) ([]model.User, error) {
	queries := model.New(database.Pool)

	return queries.ListOnlineUsers(ctx)
}

func (r *Repository) CreateUser(userData map[string]interface{}) (security.SecurityUser, error) {
	user, err := UserFromDiscord(userData)
	if err != nil {
		return nil, err
	}

	err = r.Persist(context.Background(), user)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *Repository) UpdateUser(user security.SecurityUser, userData map[string]interface{}) error {
	err := user.(*model.User).UpdateFromDiscord(userData)

	if err != nil {
		return err
	}

	err = r.Persist(context.Background(), user.(*model.User))
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) GetStats(ctx context.Context, userId int) (*MeditationStats, error) {
	queries := model.New(database.Pool)

	row, err := queries.GetUserStats(ctx, model.GetUserStatsParams{
		UserId:   int64(userId),
		UserId_2: int64(userId),
		End:      time.Now(),
	})

	if err != nil {
		return nil, err
	}

	return &MeditationStats{
		Walking:  int64(row.Walking),
		Sitting:  int64(row.Sitting),
		Sessions: int16(row.Sessions),
	}, nil
}

func (r *Repository) Delete(ctx context.Context, u *model.User) error {
	queries := model.New(database.Pool)

	err := queries.DeleteUser(ctx, u.Id)

	return err
}
