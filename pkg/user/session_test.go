package user

import (
	"context"
	"meditation-plus/pkg/model"
	"net/http"
	"testing"
)

type fakeSessionRepository struct {
	User *model.User
	err  error
}

func (q *fakeSessionRepository) FindById(ctx context.Context, id int) (*model.User, error) {
	return q.User, q.err
}
func (q *fakeSessionRepository) Persist(ctx context.Context, user *model.User) error {
	return q.err
}

type fakeSession struct {
	User          *model.User
	CalledDestroy int
}

func (q *fakeSession) Get(r *http.Request, name string) interface{} {
	return q.User
}
func (q *fakeSession) Destroy(r *http.Request) error {
	q.CalledDestroy++
	return nil
}

func TestShouldReturnValidUser(t *testing.T) {
	user := &model.User{}
	query := &fakeSessionRepository{
		User: user,
		err:  nil,
	}
	session := &fakeSession{
		User: user,
	}
	sup := &DatabaseSessionUserProvider{
		userIdQuery:        query,
		persistQuery:       query,
		sessionDestroyer:   session,
		sessionValueGetter: session,
	}

	request, _ := http.NewRequest("GET", "/", nil)
	result := sup.GetUser(request)

	if result == nil {
		t.Errorf("Expected a user")
	}

	if session.CalledDestroy != 0 {
		t.Errorf("Expected a valid session")
	}
}

func TestShouldInvalidateUser(t *testing.T) {
	user := &model.User{}
	secondUser := &model.User{
		Username: "different",
	}
	query := &fakeSessionRepository{
		User: secondUser,
		err:  nil,
	}
	session := &fakeSession{
		User: user,
	}
	sup := &DatabaseSessionUserProvider{
		userIdQuery:        query,
		sessionDestroyer:   session,
		sessionValueGetter: session,
	}

	request, _ := http.NewRequest("GET", "/", nil)
	result := sup.GetUser(request)

	if result != nil {
		t.Errorf("Expected nil")
	}

	if session.CalledDestroy != 1 {
		t.Errorf("Expected a destroyed session")
	}
}
