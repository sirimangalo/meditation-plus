package user

import (
	"context"
	"database/sql"
	"errors"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/validation"
	"net/http"
	"time"
)

type ListMigratedByOldUserUidQuery interface {
	ListMigratedByOldUserUid(ctx context.Context, userId string) ([]model.MigratedMeditation, error)
}

type MeditationStats struct {
	Walking  int64
	Sitting  int64
	Sessions int16
}

func UserFromDiscord(userData map[string]interface{}) (*model.User, error) {
	username, ok := userData["username"].(string)
	if !ok {
		return nil, errors.New("username is invalid")
	}
	id, ok := userData["id"].(string)
	if !ok {
		return nil, errors.New("id is invalid")
	}
	email, ok := userData["email"].(string)
	if !ok || email == "" {
		return nil, security.ErrUserDataInvalid

	}
	globalName, ok := userData["global_name"].(string)
	if !ok {
		globalName = username
	}
	avatar, ok := userData["avatar"].(string)
	if !ok {
		avatar = ""
	}

	return &model.User{
		UpstreamId:  id,
		Username:    username,
		Email:       email,
		Role:        model.USER_ROLES_USER,
		DisplayName: globalName,
		Timezone: sql.NullString{
			Valid: false,
		},
		ImageId: sql.NullString{
			Valid:  avatar != "",
			String: avatar,
		},
		CreatedAt: time.Now(),
	}, nil
}

type UserProfileRequest struct {
	DisplayName string
	Timezone    string
}

func (r *UserProfileRequest) Validate() validation.ValidationErrors {
	var errors validation.ValidationErrors = make(validation.ValidationErrors, 0)

	validation.Required(errors, "displayName", r.DisplayName, "")
	validation.Length(errors, "displayName", r.DisplayName, 1, 255, "")

	validation.Length(errors, "timezone", r.Timezone, 0, 255, "")
	validation.Timezone(errors, "timezone", r.Timezone, "")

	return errors
}

func (r *UserProfileRequest) UpdateModel(user *model.User) error {
	user.DisplayName = r.DisplayName
	user.Timezone = sql.NullString{
		Valid:  true,
		String: r.Timezone,
	}

	return nil
}

func UserProfileRequestFromForm(r *http.Request) *UserProfileRequest {
	return &UserProfileRequest{
		DisplayName: r.PostFormValue("displayName"),
		Timezone:    r.PostFormValue("timezone"),
	}
}

func UserProfileRequestFromModel(user *model.User) *UserProfileRequest {
	return &UserProfileRequest{
		DisplayName: user.DisplayName,
		Timezone:    user.Timezone.String,
	}
}

type UserRequest struct {
	UserProfileRequest UserProfileRequest
	Role               string
	OldUserUid         string
}

func (r *UserRequest) Validate() validation.ValidationErrors {
	var errors validation.ValidationErrors = r.UserProfileRequest.Validate()

	validation.Required(errors, "role", r.Role, "")
	validation.StringOneOf(
		errors,
		"role",
		r.Role,
		[]string{model.USER_ROLES_USER, model.USER_ROLES_ADMIN, model.USER_ROLES_TEACHER, model.USER_ROLES_ADMIN_TEACHER},
		"",
	)
	validation.Length(errors, "role", r.Role, 1, 255, "")
	validation.Length(errors, "oldUserUid", r.OldUserUid, 0, 255, "")

	return errors
}

func (r *UserRequest) UpdateModel(user *model.User) error {
	user.Role = r.Role
	user.OldUserUid = sql.NullString{
		Valid:  r.OldUserUid != "",
		String: r.OldUserUid,
	}
	err := r.UserProfileRequest.UpdateModel(user)

	if err != nil {
		return err
	}

	return nil
}

func UserRequestFromForm(r *http.Request) *UserRequest {
	return &UserRequest{
		Role:               r.PostFormValue("role"),
		OldUserUid:         r.PostFormValue("oldUserUid"),
		UserProfileRequest: *UserProfileRequestFromForm(r),
	}
}

func UserRequestFromModel(user *model.User) *UserRequest {
	return &UserRequest{
		Role:               user.Role,
		OldUserUid:         user.OldUserUid.String,
		UserProfileRequest: *UserProfileRequestFromModel(user),
	}
}
