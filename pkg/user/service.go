package user

import (
	"context"
	"fmt"
	"log/slog"
	"meditation-plus/pkg/errors"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/timezone"
	"meditation-plus/pkg/validation"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func (s *Service) ListOnline(
	w http.ResponseWriter,
	r *http.Request,
	listOnlineQuery ListOnlineQuery,
	templating *template.Templating,
) {
	list, err := listOnlineQuery.ListOnline(r.Context())

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not list users", "error", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	templating.Render("user/online.html", map[string]interface{}{
		"users": list,
	}, w, r)
}

func (s *Service) CountOnline(
	w http.ResponseWriter,
	r *http.Request,
	listOnlineQuery ListOnlineQuery,
) {
	list, err := listOnlineQuery.ListOnline(r.Context())

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not list users", "error", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write([]byte(strconv.Itoa(len(list))))
}

func (s *Service) ShowProfile(
	w http.ResponseWriter,
	r *http.Request,
	byUsernameQuery UsernameQuery,
	statsQuery StatsQuery,
	templating *template.Templating,
) {
	var stats *MeditationStats
	currentUser := security.GetUser(r).(*model.User)
	username := r.PathValue("username")
	user, err := s.getUserByUsername(r.Context(), username, byUsernameQuery)
	if err != nil {
		templating.RenderError(errors.GetErrorHttpCode(err), w, r)
		return
	}

	if currentUser.GetId() == user.GetId() {
		stats, err = statsQuery.GetStats(r.Context(), currentUser.GetId())
	}

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not read user stats.", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	templating.Render("user/profile.html", map[string]interface{}{
		"entity": user,
		"stats":  stats,
	}, w, r)
}

func (s *Service) GetAvatar(
	w http.ResponseWriter,
	r *http.Request,
	byUsernameQuery UsernameQuery,
	templating *template.Templating,
	imageGetter DiscordImageGetter,
) {
	username := r.PathValue("username")
	size := r.PathValue("size")
	user, err := s.getUserByUsername(r.Context(), username, byUsernameQuery)
	if err != nil {
		templating.RenderError(errors.GetErrorHttpCode(err), w, r)
		return
	}

	discordUserId := user.UpstreamId
	discordImageId := user.ImageId.String

	if !user.ImageId.Valid || strings.Contains(discordUserId, ".") || strings.Contains(discordImageId, ".") {
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}

	tempDir := os.TempDir()
	filepath := fmt.Sprintf("%s/%s_%s_%s.webp", tempDir, discordUserId, discordImageId, size)

	if !fileExists(filepath) {
		err := imageGetter.GetImage(filepath, discordUserId, discordImageId, size)

		if err != nil {
			slog.ErrorContext(r.Context(), "Could not create avatar file.", "error", err)
			templating.RenderError(http.StatusNotFound, w, r)
			return
		}
	}

	avatar, err := os.Open(filepath)

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not read avatar file.", "error", err)
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}
	avatarInfo, err := avatar.Stat()

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not read avatar info.", "error", err)
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}

	w.Header().Set("Content-Type", "image/webp")
	w.Header().Set("Content-Length", fmt.Sprintf("%d", avatarInfo.Size()))

	http.ServeContent(w, r, avatarInfo.Name(), avatarInfo.ModTime(), avatar)
}

func fileExists(filepath string) bool {
	info, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		return false
	}
	if err != nil {
		return false
	}
	return !info.IsDir()
}

func (s *Service) EditProfile(
	w http.ResponseWriter,
	r *http.Request,
	persistQuery PersistQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
	timezoneLister timezone.Lister,
	listListMigratedByOldUserUidQuery ListMigratedByOldUserUidQuery,
) {
	user := security.GetUser(r).(*model.User)

	violations := make(validation.ValidationErrors)
	formRequest := UserProfileRequestFromModel(user)

	if r.Method == http.MethodPost {
		formRequest = UserProfileRequestFromForm(r)

		violations = formRequest.Validate()

		if len(violations) == 0 {
			err := s.updateProfile(r.Context(), formRequest, user, persistQuery)
			if err != nil {
				templating.RenderError(errors.GetErrorHttpCode(err), w, r)
				return
			}

			flashHandler.AddFlash(r, "notice", "Your profile has been updated.")
			http.Redirect(w, r, fmt.Sprintf("/profile/%s/", user.Username), http.StatusSeeOther)
			return
		}
	}

	timezones := timezoneLister.List()

	templating.Render("user/edit-profile.html", map[string]interface{}{
		"entity":         user,
		"timezones":      timezones,
		"errors":         violations,
		"data":           formRequest,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) DeleteProfile(
	w http.ResponseWriter,
	r *http.Request,
	deleteQuery DeleteQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	user := security.GetUser(r).(*model.User)

	err := deleteQuery.Delete(r.Context(), user)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not delete user", "userId", user.GetId(), "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	flashHandler.AddFlash(r, "notice", "Your profile has been deleted.")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (s *Service) getUserByUsername(ctx context.Context, username string, byUsernameQuery UsernameQuery) (*model.User, error) {
	user, err := byUsernameQuery.FindByUsername(ctx, username)
	if err != nil {
		slog.ErrorContext(ctx, "Could not find user by username", "error", err, "username", username)
		return nil, errors.NewServiceError(http.StatusInternalServerError, err)
	}
	if user == nil {
		slog.WarnContext(ctx, "Could not find user by username", "username", username)
		return nil, errors.NewServiceError(http.StatusNotFound, nil)
	}

	return user, nil
}

func (s *Service) updateProfile(ctx context.Context, formRequest *UserProfileRequest, user *model.User, persistQuery PersistQuery) error {
	err := formRequest.UpdateModel(user)
	if err != nil {
		slog.ErrorContext(ctx, "Could not convert request to model", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	err = persistQuery.Persist(ctx, user)

	if err != nil {
		slog.ErrorContext(ctx, "Could not save user", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	return nil
}
