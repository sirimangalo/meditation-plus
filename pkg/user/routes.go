package user

import (
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/timezone"
	"net/http"
)

type Routes struct {
	repository                   *Repository
	flashHandler                 security.FlashHandler
	templating                   *template.Templating
	service                      *Service
	timezoneService              *timezone.Service
	imageGetter                  DiscordImageGetter
	listMigratedByOldUserIdQuery ListMigratedByOldUserUidQuery
}

func NewRoutes(
	repository *Repository,
	flashHandler security.FlashHandler,
	templating *template.Templating,
	service *Service,
	timezoneService *timezone.Service,
	imageGetter DiscordImageGetter,
	listMigratedByOldUserIdQuery ListMigratedByOldUserUidQuery,
) *Routes {
	return &Routes{
		repository:                   repository,
		flashHandler:                 flashHandler,
		templating:                   templating,
		service:                      service,
		timezoneService:              timezoneService,
		imageGetter:                  imageGetter,
		listMigratedByOldUserIdQuery: listMigratedByOldUserIdQuery,
	}
}

func (ur *Routes) Index(w http.ResponseWriter, r *http.Request) {
	ur.service.List(w, r, ur.repository, ur.repository, ur.templating)
}

func (ur *Routes) GetAvatar(w http.ResponseWriter, r *http.Request) {
	ur.service.GetAvatar(w, r, ur.repository, ur.templating, ur.imageGetter)
}

func (ur *Routes) CountOnline(w http.ResponseWriter, r *http.Request) {
	ur.service.CountOnline(w, r, ur.repository)
}

func (ur *Routes) ListOnline(w http.ResponseWriter, r *http.Request) {
	ur.service.ListOnline(w, r, ur.repository, ur.templating)
}

func (ur *Routes) Edit(w http.ResponseWriter, r *http.Request) {
	ur.service.Edit(w, r, ur.repository, ur.repository, ur.templating, ur.flashHandler, ur.timezoneService, ur.listMigratedByOldUserIdQuery)
}

func (ur *Routes) EditProfile(w http.ResponseWriter, r *http.Request) {
	ur.service.EditProfile(w, r, ur.repository, ur.templating, ur.flashHandler, ur.timezoneService, ur.listMigratedByOldUserIdQuery)
}

func (ur *Routes) DeleteProfile(w http.ResponseWriter, r *http.Request) {
	ur.service.DeleteProfile(w, r, ur.repository, ur.templating, ur.flashHandler)
}

func (ur *Routes) ShowProfile(w http.ResponseWriter, r *http.Request) {
	ur.service.ShowProfile(w, r, ur.repository, ur.repository, ur.templating)
}

func (ur *Routes) Delete(w http.ResponseWriter, r *http.Request) {
	ur.service.Delete(w, r, ur.repository, ur.repository, ur.templating, ur.flashHandler)
}
