package user

import (
	"context"
	"log/slog"
	"meditation-plus/pkg/errors"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/timezone"
	"meditation-plus/pkg/validation"
	"net/http"
	"strconv"
)

type Service struct {
}

func (s *Service) List(
	w http.ResponseWriter,
	r *http.Request,
	paginateQuery PaginateQuery,
	countQuery CountQuery,
	templating *template.Templating,
) {
	query := r.URL.Query()
	search := query.Get("q")

	if search == "" {
		search = "%"
	}

	page, _ := strconv.Atoi(query.Get("page"))

	page = page - 1

	if page < 0 {
		page = 0
	}

	count, err := countQuery.Count(r.Context(), search)

	if err != nil {
		slog.ErrorContext(r.Context(), "Error while counting", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	users, err := paginateQuery.Paginate(r.Context(), page+1, search)

	if err != nil {
		slog.ErrorContext(r.Context(), "Error while paginting", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	pageCount := (count-1)/USERS_PER_PAGE + 1
	pages := model.GetPages(pageCount, page+1)

	templating.Render("admin/user/index.html", map[string]interface{}{
		"users":  users,
		"count":  count,
		"search": query.Get("q"),
		"page":   page + 1,
		"pages":  pages,
	}, w, r)
}

func (s *Service) Edit(
	w http.ResponseWriter,
	r *http.Request,
	findById IdQuery,
	persistQuery PersistQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
	timezoneLister timezone.Lister,
	listMigratedByOldUserIdQuery ListMigratedByOldUserUidQuery,
) {
	userId, _ := strconv.Atoi(r.PathValue("userId"))

	user, err := s.getUserToEdit(r.Context(), userId, findById)
	if err != nil {
		templating.RenderError(errors.GetErrorHttpCode(err), w, r)
		return
	}

	violations := make(validation.ValidationErrors)
	formRequest := UserRequestFromModel(user)

	if r.Method == http.MethodPost {
		formRequest = UserRequestFromForm(r)

		violations = formRequest.Validate()

		if len(violations) == 0 {
			err := s.update(r.Context(), formRequest, user, persistQuery)
			if err != nil {
				templating.RenderError(errors.GetErrorHttpCode(err), w, r)
				return
			}

			flashHandler.AddFlash(r, "notice", "The user has been updated.")
			http.Redirect(w, r, "/admin/users/", http.StatusSeeOther)
			return
		}
	}

	timezones := timezoneLister.List()

	templating.Render("admin/user/edit.html", map[string]interface{}{
		"entity":         user,
		"errors":         violations,
		"data":           formRequest,
		"timezones":      timezones,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) getUserToEdit(ctx context.Context, userId int, findById IdQuery) (*model.User, error) {
	user, err := findById.FindById(ctx, userId)
	if err != nil {
		slog.ErrorContext(ctx, "Could not find user by id", "error", err)
		return nil, errors.NewServiceError(http.StatusInternalServerError, err)
	}
	if user == nil {
		slog.WarnContext(ctx, "Could not find user by id", "userId", userId)
		return nil, errors.NewServiceError(http.StatusNotFound, nil)
	}

	return user, nil
}

func (s *Service) update(ctx context.Context, formRequest *UserRequest, user *model.User, persistQuery PersistQuery) error {
	err := formRequest.UpdateModel(user)
	if err != nil {
		slog.ErrorContext(ctx, "Could not convert request to model", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	err = persistQuery.Persist(ctx, user)

	if err != nil {
		slog.ErrorContext(ctx, "Could not save user", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	return nil
}

func (s *Service) Delete(
	w http.ResponseWriter,
	r *http.Request,
	findById IdQuery,
	deleteQuery DeleteQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	userId, _ := strconv.Atoi(r.PathValue("userId"))

	user, err := s.getUserToEdit(r.Context(), userId, findById)
	if err != nil {
		templating.RenderError(errors.GetErrorHttpCode(err), w, r)
		return
	}

	err = deleteQuery.Delete(r.Context(), user)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not delete user", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	flashHandler.AddFlash(r, "notice", "The user has been deleted.")
	http.Redirect(w, r, "/admin/users/", http.StatusSeeOther)
}
