package user

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIsUserInDiscordGuild_Success(t *testing.T) {
	guilds := []Guild{
		{ID: "12345"},
		{ID: "67890"},
	}

	guildsJson, _ := json.Marshal(guilds)

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if got, want := r.URL.Path, "/users/@me/guilds"; got != want {
			t.Errorf("got %s, want %s", got, want)
		}
		if got, want := r.Header.Get("Authorization"), "Bearer fake-access-token"; got != want {
			t.Errorf("got %s, want %s", got, want)
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(guildsJson)
	}))
	defer server.Close()

	discordBaseUrl = server.URL

	// user is in guild
	result, err := isUserInDiscordGuild("fake-access-token", "12345")
	if got := err; got != nil {
		t.Errorf("got %s, want %v", got, nil)
	}
	if got, want := result, true; got != want {
		t.Errorf("got %v, want %v", got, want)
	}

	// user is not in guild
	result, err = isUserInDiscordGuild("fake-access-token", "non-existing-guild")
	if got := err; got != nil {
		t.Errorf("got %s, want %v", got, nil)
	}
	if got, want := result, false; got != want {
		t.Errorf("got %v, want %v", got, want)
	}
}
