package user

import (
	"context"
	"meditation-plus/pkg/database"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/test"
	"testing"
)

func testRepositoryCreateUser(repo *Repository, name string) (*model.User, error) {
	user, err := repo.CreateUser(map[string]interface{}{
		"username": name,
		"id":       name,
		"email":    name,
	})

	if user != nil {
		return user.(*model.User), err
	}

	return nil, err
}

func TestRepositoryCreate(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	user, err := testRepositoryCreateUser(repo, "create_test")
	test.AssertNilErr(t, err)
	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}
	test.AssertGreaterOrEquals(t, 1, user.GetId())

	_ = repo.Delete(context.Background(), user)
}

func TestRepositoryUpdate(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	user, err := testRepositoryCreateUser(repo, "update_test")
	test.AssertNilErr(t, err)

	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}

	err = repo.UpdateUser(user, map[string]interface{}{
		"username": "update_test",
		"id":       "update_test",
		"email":    "changed_email",
	})

	test.AssertNilErr(t, err)

	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}

	user, err = repo.FindById(context.Background(), user.GetId())
	test.AssertNilErr(t, err)
	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}
	test.AssertEquals(t, "changed_email", user.Email)

	_ = repo.Delete(context.Background(), user)
}

func TestRepositoryFindAll(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	list, err := repo.FindAll(context.Background())
	test.AssertNilErr(t, err)
	test.AssertGreaterOrEquals(t, 3, len(list))
}

func TestRepositoryFindByUsername(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	user, err := repo.FindByUsername(context.Background(), "user")
	test.AssertNilErr(t, err)
	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}
	test.AssertEquals(t, int64(1), user.Id)
}

func TestRepositoryFindTeacher(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	list, err := repo.FindTeacher(context.Background())
	test.AssertNilErr(t, err)
	if len(list) < 1 {
		t.Fatalf("Expected list to contain elements")
	}
	test.AssertEquals(t, int64(2), list[0].Id)
}

func TestRepositoryPaginate(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	list, err := repo.Paginate(context.Background(), 1, "%")
	test.AssertNilErr(t, err)
	test.AssertGreaterOrEquals(t, 3, len(list))

	list, err = repo.Paginate(context.Background(), 1, "teacher")
	test.AssertNilErr(t, err)
	if len(list) < 1 {
		t.Fatalf("Expected list to contain elements")
	}
	test.AssertEquals(t, int64(2), list[0].Id)
}

func TestRepositoryCount(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	count, err := repo.Count(context.Background(), "%")
	test.AssertNilErr(t, err)
	test.AssertGreaterOrEquals(t, 3, count)

	count, err = repo.Count(context.Background(), "teacher")
	test.AssertNilErr(t, err)
	test.AssertEquals(t, 1, count)
}

func TestRepositoryProvideUserByIdentifier(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	user, err := repo.ProvideUserByIdentifier("teacher")
	test.AssertNilErr(t, err)
	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}
	test.AssertEquals(t, 2, user.GetId())
}

func TestRepositoryListOnline(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	list, err := repo.ListOnline(context.Background())
	test.AssertNilErr(t, err)
	test.AssertEquals(t, 0, len(list))

	user, err := repo.FindById(context.Background(), 1)
	test.AssertNilErr(t, err)
	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}
	user.Active()
	_ = repo.Persist(context.Background(), user)

	list, err = repo.ListOnline(context.Background())
	test.AssertNilErr(t, err)
	test.AssertEquals(t, 1, len(list))
}

func TestRepositoryDelete(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	user, err := testRepositoryCreateUser(repo, "delete_test")
	test.AssertNilErr(t, err)

	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}

	err = repo.Delete(context.Background(), user)
	test.AssertNilErr(t, err)

	user, err = repo.FindById(context.Background(), user.GetId())
	test.AssertNilErr(t, err)
	if user != nil {
		t.Fatalf("Expected to receive nil user (got: %v)", user)
	}
}
