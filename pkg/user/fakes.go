package user

import (
	"context"
	"database/sql"
	"meditation-plus/pkg/model"
	"os"
)

type FakeRepository struct {
	User    *model.User
	Teacher []model.User
	Users   []model.User
}

func (q *FakeRepository) FindById(ctx context.Context, id int) (*model.User, error) {
	return q.User, nil
}
func (q *FakeRepository) FindByUsername(ctx context.Context, username string) (*model.User, error) {
	return q.User, nil
}
func (q *FakeRepository) FindTeacher(ctx context.Context) ([]model.User, error) {
	return q.Teacher, nil
}
func (q *FakeRepository) Delete(ctx context.Context, user *model.User) error {
	return nil
}
func (q *FakeRepository) Persist(ctx context.Context, user *model.User) error {
	return nil
}
func (q *FakeRepository) Paginate(ctx context.Context, page int, search string) ([]model.User, error) {
	return q.Users, nil
}
func (q *FakeRepository) Count(ctx context.Context, search string) (int, error) {
	return 1, nil
}
func (q *FakeRepository) ListMigratedByOldUserUid(ctx context.Context, userId string) ([]model.MigratedMeditation, error) {
	return nil, nil
}
func (q *FakeRepository) ListOnline(ctx context.Context) ([]model.User, error) {
	return q.Users, nil
}
func (q *FakeRepository) GetStats(ctx context.Context, userId int) (*MeditationStats, error) {
	return &MeditationStats{
		Sitting: int64(32768),
		Walking: int64(327680),
	}, nil
}

func NewFakeUser(id int) *model.User {
	return &model.User{
		Id:       int64(id),
		Welcomed: true,
		Timezone: sql.NullString{
			String: "Europe/Berlin",
			Valid:  true,
		},
	}
}

func NewFakeUserWithRole(id int, role string) *model.User {
	return &model.User{
		Id:   int64(id),
		Role: role,
	}
}

type FakeImageGetter struct{}

func (f *FakeImageGetter) GetImage(filepath string, discordUserId string, discordImageId string, size string) error {
	file, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString("ok")
	if err != nil {
		return err
	}
	return nil
}
