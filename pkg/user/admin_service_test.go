package user

import (
	"context"
	"fmt"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/timezone"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestList(t *testing.T) {
	users := []model.User{
		*NewFakeUser(1),
		*NewFakeUser(2),
	}

	cases := map[string]struct {
		Users  []model.User
		Result int
	}{
		"should list": {Users: users, Result: http.StatusOK},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, NewFakeUser(1))
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &FakeRepository{Users: testCase.Users}
		templating := template.NewTemplatingWithFilter("test", session, "admin/user")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.List(w, r, userRepository, userRepository, templating)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestEdit(t *testing.T) {
	user := &model.User{}

	validData := map[string]string{
		"username":    "username",
		"email":       "email@sirimangalo.org",
		"role":        "ADMIN",
		"displayName": "User",
		"timezone":    "UTC",
	}

	cases := map[string]struct {
		Method   string
		User     *model.User
		FormData map[string]string
		Result   int
	}{
		"404 on missing user POST": {
			Method: "POST",
			Result: http.StatusNotFound,
		},
		"404 on missing user GET": {
			Method: "GET",
			User:   nil,
			Result: http.StatusNotFound,
		},
		"200 on invalid data": {
			Method: "POST",
			User:   user,
			Result: http.StatusOK,
		},
		"302 on valid data": {
			Method:   "POST",
			User:     user,
			FormData: validData,
			Result:   http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", test.CreateFormData(testCase.FormData))
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		if testCase.User != nil {
			request.SetPathValue("userId", fmt.Sprintf("%d", testCase.User.Id))
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, NewFakeUser(1))
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &FakeRepository{User: testCase.User}
		templating := template.NewTemplatingWithFilter("test", session, "admin/user")
		timezones := &timezone.FakeService{}

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Edit(w, r, userRepository, userRepository, templating, session, timezones, userRepository)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestDelete(t *testing.T) {
	user := &model.User{}

	cases := map[string]struct {
		Method string
		User   *model.User
		Result int
	}{
		"404 on missing user": {
			Method: "POST",
			User:   nil,
			Result: http.StatusNotFound,
		},
		"302 on valid request": {
			Method: "POST",
			User:   user,
			Result: http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		if testCase.User != nil {
			request.SetPathValue("userId", fmt.Sprintf("%d", testCase.User.Id))
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, NewFakeUser(1))
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &FakeRepository{User: testCase.User}
		templating := template.NewTemplatingWithFilter("test", session, "admin/user")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Delete(w, r, userRepository, userRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}
