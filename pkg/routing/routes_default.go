package routing

import (
	"context"
	"log/slog"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/user"
	"net/http"
	"strings"
)

type requestContextType string

const RequestContextKey = requestContextType("request")

func healthzHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

func handleAdminIndex(
	templating *template.Templating,
) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		templating.Render("admin/index.html", map[string]interface{}{}, w, r)
	})
}

func handleLogin(
	templating *template.Templating,
	sessionValueGetter security.SessionValueGetter,
) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if _, ok := sessionValueGetter.Get(r, "user").(*model.User); ok {
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}

		templating.Render("pages/login.html", map[string]interface{}{}, w, r)
	})
}

func handlePrivacy(
	templating *template.Templating,
) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		templating.Render("pages/privacy.html", map[string]interface{}{}, w, r)
	})
}

func handleLogout(
	session security.SessionStore,
	persistQuery user.PersistQuery,
) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := security.GetUser(r).(*model.User)
		user.Logout()
		err := persistQuery.Persist(r.Context(), user)

		if err != nil {
			slog.ErrorContext(r.Context(), "Could not update user", "error", err)
			return
		}

		err = session.Destroy(r)
		if err != nil {
			slog.ErrorContext(r.Context(), "Could not destroy session", "error", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

func logRequests(sessionValueGetter security.SessionValueGetter, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), RequestContextKey, r.URL.Path)
		r = r.WithContext(ctx)

		if !strings.HasPrefix(r.URL.Path, "/healthz") {
			user, _ := sessionValueGetter.Get(r, "user").(*model.User)
			var userId int64
			if user != nil {
				userId = user.Id
			}
			slog.Info("Request", "method", r.Method, "path", r.URL.Path, "host", r.Host, "referer", r.Referer(), "remote_addr", r.RemoteAddr, "user_id", userId)
		}

		next.ServeHTTP(w, r)
	})
}
