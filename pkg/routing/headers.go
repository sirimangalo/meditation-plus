package routing

import (
	"net/http"
	"time"
)

const OneYear = 365 * 24 * time.Hour

func securityHeaders(appEnv string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if appEnv != "test" {
			w.Header().Add("Strict-Transport-Security", "max-age=31536000; includeSubdomains; preload")
			w.Header().Add("Cross-Origin-Opener-Policy", "same-origin")
		}

		w.Header().Add("Referrer-Policy", "no-referrer, strict-origin-when-cross-origin")
		w.Header().Add("Permissions-Policy", "accelerometer=(),autoplay=(),camera=(),display-capture=(),encrypted-media=(),fullscreen=(),geolocation=(),gyroscope=(),magnetometer=(),microphone=(),midi=(),payment=(),picture-in-picture=(),publickey-credentials-get=(),screen-wake-lock=(),sync-xhr=(self),usb=(),xr-spatial-tracking=()")
		w.Header().Add("Cross-Origin-Embedder-Policy", "require-corp")
		w.Header().Add("Cross-Origin-Resource-Policy", "same-site")
		w.Header().Add("Content-Security-Policy", "default-src 'self'; base-uri 'none'; form-action 'self' https://discord.com; frame-ancestors 'none'; object-src 'none'")
		next.ServeHTTP(w, r)
	})
}

func cachingHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Expires", time.Now().Add(OneYear).Format(time.RFC1123))
		next.ServeHTTP(w, r)
	})
}
