package routing

import (
	"meditation-plus/pkg/appointment"
	"meditation-plus/pkg/banner"
	"meditation-plus/pkg/gzip"
	"meditation-plus/pkg/meditation"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/timezone"
	"meditation-plus/pkg/user"
	"meditation-plus/web"
	"net/http"
)

func Routes(
	session security.SessionStore,
	sessionValueGetter security.SessionValueGetter,
	sessionUserProvider security.SessionUserProvider,
	templating *template.Templating,
	userRepository *user.Repository,
	userService *user.Service,
	appointmentRepository *appointment.Repository,
	appointmentService *appointment.Service,
	meditationRepository *meditation.Repository,
	meditationService *meditation.Service,
	bannerService *banner.Service,
	timezoneService *timezone.Service,
	authenticator security.Authenticator,
	testAuthenticator security.Authenticator,
	imageGetter user.DiscordImageGetter,
	appEnv string,
) http.Handler {
	rolesTeacherAdmin := []string{model.USER_ROLES_ADMIN, model.USER_ROLES_TEACHER}
	rolesAdmin := []string{model.USER_ROLES_ADMIN}

	r := http.NewServeMux()
	r.Handle("GET /static/assets/", cachingHeaders(gzip.GzipFileserverMiddleware(http.FS(web.Content))))
	r.Handle("GET /static/data/", cachingHeaders(gzip.GzipFileserverMiddleware(http.FS(web.Data))))
	r.Handle("GET /static/favicon/", cachingHeaders(gzip.GzipFileserverMiddleware(http.FS(web.Favicon))))
	r.Handle("GET /dev/static/", http.StripPrefix("/dev/static/", http.FileServer(http.Dir("./web/static/"))))
	r.HandleFunc("GET /login/{$}", handleLogin(templating, sessionValueGetter))
	r.HandleFunc("GET /privacy/{$}", handlePrivacy(templating))
	r.HandleFunc("GET /auth/{$}", security.HandleAuth(authenticator, session))

	auth := security.NewAuthMiddleware(sessionUserProvider, "/login/", templating, session)
	r.HandleFunc("GET /welcome/{$}", auth.Handle(handleWelcome(templating, userRepository)))

	appointment := appointment.NewRoutes(appointmentRepository, userRepository, session, templating, appointmentService)
	r.HandleFunc("GET /appointments/{$}", auth.Handle(appointment.Index))
	r.HandleFunc("POST /appointments/book/{id}/{$}", auth.Handle(appointment.Book))
	r.HandleFunc("POST /appointments/unbook/{id}/{$}", auth.Handle(appointment.Cancel))
	r.HandleFunc("GET /admin/{$}", auth.HandleRole(handleAdminIndex(templating), rolesTeacherAdmin))
	r.HandleFunc("GET /admin/appointments/{$}", auth.HandleRole(appointment.AdminIndex, rolesTeacherAdmin))
	r.HandleFunc("GET /admin/appointments/{teacherId}/{$}", auth.HandleRole(appointment.AdminList, rolesTeacherAdmin))
	r.HandleFunc("GET /admin/appointments/{teacherId}/new/{$}", auth.HandleRole(appointment.New, rolesTeacherAdmin))
	r.HandleFunc("POST /admin/appointments/{teacherId}/new/{$}", auth.HandleRole(appointment.New, rolesTeacherAdmin))
	r.HandleFunc("GET /admin/appointments/{appointmentId}/edit/{$}", auth.HandleRole(appointment.Edit, rolesTeacherAdmin))
	r.HandleFunc("POST /admin/appointments/{appointmentId}/edit/{$}", auth.HandleRole(appointment.Edit, rolesTeacherAdmin))
	r.HandleFunc("POST /admin/appointments/{appointmentId}/delete/{$}", auth.HandleRole(appointment.Delete, rolesTeacherAdmin))

	userR := user.NewRoutes(userRepository, session, templating, userService, timezoneService, imageGetter, meditationRepository)
	r.HandleFunc("GET /online/count/{$}", auth.Handle(userR.CountOnline))
	r.HandleFunc("GET /online/{$}", auth.Handle(userR.ListOnline))
	r.HandleFunc("GET /profile/{username}/{$}", auth.Handle(userR.ShowProfile))
	r.HandleFunc("GET /avatar/{username}/{size}/{$}", auth.Handle(userR.GetAvatar))
	r.HandleFunc("GET /edit-profile/{$}", auth.Handle(userR.EditProfile))
	r.HandleFunc("POST /edit-profile/{$}", auth.Handle(userR.EditProfile))
	r.HandleFunc("POST /delete-profile/{$}", auth.Handle(userR.DeleteProfile))
	r.HandleFunc("GET /admin/users/{$}", auth.HandleRole(userR.Index, rolesAdmin))
	r.HandleFunc("GET /admin/users/{userId}/edit/{$}", auth.HandleRole(userR.Edit, rolesAdmin))
	r.HandleFunc("POST /admin/users/{userId}/edit/{$}", auth.HandleRole(userR.Edit, rolesAdmin))
	r.HandleFunc("POST /admin/users/{userId}/delete/{$}", auth.HandleRole(userR.Delete, rolesAdmin))

	meditation := meditation.NewRoutes(meditationRepository, userRepository, session, templating, meditationService, sessionValueGetter)
	r.HandleFunc("GET /", auth.Handle(meditation.Index))
	r.HandleFunc("GET /start/{$}", auth.Handle(meditation.Start))
	r.HandleFunc("POST /start/{$}", auth.Handle(meditation.Start))
	r.HandleFunc("POST /stop/{$}", auth.Handle(meditation.Stop))
	r.HandleFunc("GET /meditations/log/{$}", auth.Handle(meditation.LogPast))
	r.HandleFunc("POST /meditations/log/{$}", auth.Handle(meditation.LogPast))
	r.HandleFunc("GET /meditations/import/{$}", auth.Handle(meditation.Import))
	r.HandleFunc("POST /meditations/import/{$}", auth.Handle(meditation.Import))
	r.HandleFunc("GET /meditations/convert/{$}", auth.Handle(meditation.Convert))
	r.HandleFunc("POST /meditations/convert/{$}", auth.Handle(meditation.Convert))
	r.HandleFunc("GET /meditations/convert/login/{$}", auth.Handle(meditation.Login))
	r.HandleFunc("POST /meditations/convert/login/{$}", auth.Handle(meditation.Login))
	r.HandleFunc("GET /meditations/{$}", auth.Handle(meditation.List))
	r.HandleFunc("POST /meditations/delete/{id}/{$}", auth.Handle(meditation.Delete))

	banner := banner.NewRoutes(session, templating, bannerService)
	r.HandleFunc("GET /admin/banner/{$}", auth.HandleRole(banner.Edit, rolesAdmin))
	r.HandleFunc("POST /admin/banner/{$}", auth.HandleRole(banner.Edit, rolesAdmin))

	r.HandleFunc("GET /healthz/{$}", healthzHandler)

	r.HandleFunc("GET /logout/{$}", auth.Handle(handleLogout(session, userRepository)))

	if appEnv == "test" {
		r.HandleFunc("GET /auth/test", security.HandleAuth(testAuthenticator, session))
	}

	return logRequests(sessionValueGetter, securityHeaders(appEnv, security.CsrfMiddleware(appEnv, r)))
}
