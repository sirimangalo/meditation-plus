package routing

import (
	"log/slog"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/user"
	"net/http"
)

func handleWelcome(
	templating *template.Templating,
	persistQuery user.PersistQuery,
) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := security.GetUser(r).(*model.User)
		user.Welcomed = true
		err := persistQuery.Persist(r.Context(), user)

		if err != nil {
			slog.ErrorContext(r.Context(), "Could not update user.", "err", err)
			templating.RenderError(http.StatusInternalServerError, w, r)
			return
		}

		templating.Render("pages/welcome.html", map[string]interface{}{}, w, r)
	})
}
