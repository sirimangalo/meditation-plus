package test

import (
	"net/http"
)

type FakeSessionStore struct {
	GetData string
}

func (f *FakeSessionStore) Get(r *http.Request, name string) interface{} {
	return f.GetData
}

func (f *FakeSessionStore) Add(r *http.Request, name string, value interface{}) {
}
func (f *FakeSessionStore) Middleware(next http.Handler) http.Handler {
	return next
}
func (f *FakeSessionStore) Destroy(r *http.Request) error {
	return nil
}
func (f *FakeSessionStore) AddFlash(r *http.Request, flashType string, message string) {
}
func (f *FakeSessionStore) GetFlash(r *http.Request, flashType string) string {
	return ""
}

type FakeErrorRenderer struct {
	Error int
}

func (er *FakeErrorRenderer) RenderError(errorCode int, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(errorCode)
}
