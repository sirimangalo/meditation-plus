package test

import (
	"bytes"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func AssertEquals(t *testing.T, expected any, value any) {
	if expected != value {
		t.Fatalf("Expected %s to match %s", value, expected)
	}
}

func AssertGreaterOrEquals(t *testing.T, expectedGreaterOrEqual int, value int) {
	if value < expectedGreaterOrEqual {
		t.Fatalf("Expected %d to be greater or equal to %d", value, expectedGreaterOrEqual)
	}
}

func AssertNilErr(t *testing.T, err error) {
	if err != nil {
		t.Fatalf("Expected to receive no error (got: %v)", err)
	}
}

func CreateFormData(data map[string]string) *bytes.Buffer {
	body := new(bytes.Buffer)
	for key, value := range data {
		body.WriteString(key)
		body.WriteString("=")
		body.WriteString(value)
		body.WriteString("&")
	}
	return body
}

type checkNodeFn func(*html.Node) bool

func GetFirstElement(n *html.Node, checkNodeFn checkNodeFn) *html.Node {
	if n == nil {
		return nil
	}
	if n.Type == html.ElementNode && checkNodeFn(n) {
		return n
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if result := GetFirstElement(c, checkNodeFn); result != nil {
			return result
		}
	}
	return nil
}

func GetFirstElementByClassName(n *html.Node, className string) *html.Node {
	if n == nil {
		return nil
	}
	return GetFirstElement(n, func(n *html.Node) bool {
		for _, attr := range n.Attr {
			if attr.Key == "class" && strings.Contains(attr.Val, className) {
				return true
			}
		}

		return false
	})
}

func NodeContainsText(n *html.Node, text string) bool {
	if n.Type == html.TextNode && strings.Contains(n.Data, text) {
		return true
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if NodeContainsText(c, text) {
			return true
		}
	}
	return false
}
