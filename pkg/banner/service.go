package banner

import (
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"net/http"
)

type Service struct {
}

func (s *Service) Edit(
	w http.ResponseWriter,
	r *http.Request,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	if r.Method == http.MethodPost {
		SetBanner(r.PostFormValue("banner"))

		flashHandler.AddFlash(r, "notice", "The notification banner has been set.")
		http.Redirect(w, r, "/admin/", http.StatusSeeOther)
		return
	}

	templating.Render("admin/banner/edit.html", map[string]interface{}{
		"banner":         GetBanner(),
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}
