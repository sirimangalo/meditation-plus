package banner

import (
	"fmt"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/test"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestEdit(t *testing.T) {
	validData := map[string]string{
		"banner": "MyBannerText",
	}
	cases := map[string]struct {
		Method   string
		FormData map[string]string
		Result   int
	}{
		"200 on GET": {
			Method: "GET",
			Result: http.StatusOK,
		},
		"302 on POST": {
			Method:   "POST",
			Result:   http.StatusSeeOther,
			FormData: validData,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", test.CreateFormData(testCase.FormData))
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		session := &test.FakeSessionStore{}
		service := &Service{}
		templating := template.NewTemplatingWithFilter("test", session, "admin/banner")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Edit(w, r, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}

		if expected, ok := testCase.FormData["banner"]; ok {
			test.AssertEquals(t, expected, GetBanner())
		}
	}
}
