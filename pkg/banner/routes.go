package banner

import (
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"net/http"
)

type Routes struct {
	flashHandler security.FlashHandler
	templating   *template.Templating
	service      *Service
}

func NewRoutes(
	flashHandler security.FlashHandler,
	templating *template.Templating,
	service *Service,
) *Routes {
	return &Routes{
		flashHandler: flashHandler,
		templating:   templating,
		service:      service,
	}
}

func (br *Routes) Edit(w http.ResponseWriter, r *http.Request) {
	br.service.Edit(
		w,
		r,
		br.templating,
		br.flashHandler,
	)
}
