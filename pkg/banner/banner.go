package banner

import "sync"

var (
	data   string
	dataMu sync.RWMutex
)

func SetBanner(value string) {
	dataMu.Lock()
	defer dataMu.Unlock()
	data = value
}

func GetBanner() string {
	dataMu.RLock()
	defer dataMu.RUnlock()
	return data
}
