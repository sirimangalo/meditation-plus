package mail

import (
	"context"
	"crypto/tls"
	"fmt"
	"log/slog"
	"net"
	"net/smtp"
	"regexp"
	"strings"
	"time"
)

type Mailer interface {
	SendEmail(ctx context.Context, to, subject, body string) error
	RetrySendEmail(ctx context.Context, to, subject, body string, retries int, delay time.Duration)
}

type SmtpMailer struct {
	From        string
	Password    string
	Host        string
	Port        string
	UseStartTls bool
}

func NewSmtpMailer(from string, password string, host string, port string, useStartTls bool) *SmtpMailer {
	return &SmtpMailer{
		From:        from,
		Password:    password,
		Host:        host,
		Port:        port,
		UseStartTls: useStartTls,
	}
}

func (s *SmtpMailer) sanitizeHeader(value string) string {
	return regexp.MustCompile(`[\r\n]+`).ReplaceAllString(value, " ")
}

func (s *SmtpMailer) SendEmail(ctx context.Context, to, subject, body string) error {
	to = s.sanitizeHeader(to)
	subject = s.sanitizeHeader(subject)
	body = strings.ReplaceAll(body, "\r\n", "\n")

	message := []byte(fmt.Sprintf("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s", s.From, to, subject, body))

	var client *smtp.Client
	var err error

	if s.UseStartTls {
		client, err = s.connectStartTls()
	} else {
		client, err = s.connectTls()
	}

	if err != nil {
		return err
	}

	defer client.Close()

	if err = client.Mail(s.From); err != nil {
		return fmt.Errorf("Failed to set sender: %v", err)
	}
	if err = client.Rcpt(to); err != nil {
		return fmt.Errorf("Failed to set recipient: %v", err)
	}

	wc, err := client.Data()
	if err != nil {
		return fmt.Errorf("Failed to send data: %v", err)
	}
	defer wc.Close()

	_, err = wc.Write(message)
	if err != nil {
		return fmt.Errorf("Failed to write message: %v", err)
	}

	slog.InfoContext(ctx, "Sent email", "subject", subject)

	return nil
}

func (s *SmtpMailer) connectTls() (*smtp.Client, error) {
	tlsConfig := &tls.Config{
		ServerName: s.Host,
	}
	auth := smtp.PlainAuth("", s.From, s.Password, s.Host)

	conn, err := tls.Dial("tcp", s.Host+":"+s.Port, tlsConfig)
	if err != nil {
		return nil, fmt.Errorf("Failed to connect to SMTP server: %v", err)
	}

	client, err := smtp.NewClient(conn, s.Host)
	if err != nil {
		return nil, fmt.Errorf("Failed to create SMTP client: %v", err)
	}

	if err = client.Auth(auth); err != nil {
		return nil, fmt.Errorf("Failed to authenticate with SMTP server: %v", err)
	}

	return client, nil
}

func (s *SmtpMailer) connectStartTls() (*smtp.Client, error) {
	tlsConfig := &tls.Config{
		ServerName: s.Host,
	}
	auth := smtp.PlainAuth("", s.From, s.Password, s.Host)

	conn, err := net.Dial("tcp", s.Host+":"+s.Port)
	if err != nil {
		return nil, fmt.Errorf("Failed to connect to SMTP server: %v", err)
	}

	client, err := smtp.NewClient(conn, s.Host)
	if err != nil {
		return nil, fmt.Errorf("Failed to create SMTP client: %v", err)
	}

	if err = client.StartTLS(tlsConfig); err != nil {
		return nil, fmt.Errorf("Failed to start TLS: %v", err)
	}

	if err = client.Auth(auth); err != nil {
		return nil, fmt.Errorf("Failed to authenticate with SMTP server: %v", err)
	}

	return client, nil
}

// Use with go routine:
// go mailer.RetrySendEmail(to, subject, body, 3, time.Hour)
func (s *SmtpMailer) RetrySendEmail(ctx context.Context, to, subject, body string, retries int, delay time.Duration) {
	var lastErr error
	for i := 0; i < retries; i++ {
		err := s.SendEmail(ctx, to, subject, body)
		if err == nil {
			return
		}

		lastErr = err
		slog.WarnContext(ctx, "Failed to send email", "attempt", i+1, "retries", retries, "error", err)

		if i < retries-1 {
			time.Sleep(delay)
		}
	}

	slog.ErrorContext(ctx, "Failed to send email after multiple attempts.", "lastError", lastErr)
}
