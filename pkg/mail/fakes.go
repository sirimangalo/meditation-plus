package mail

import (
	"context"
	"time"
)

type FakeSentEmail struct {
	To      string
	Subject string
	Body    string
}

type FakeMailer struct {
	SentEmails []*FakeSentEmail
}

func (f *FakeMailer) SendEmail(ctx context.Context, to, subject, body string) error {
	f.SentEmails = append(f.SentEmails, &FakeSentEmail{
		To:      to,
		Subject: subject,
		Body:    body,
	})
	return nil
}

func (f *FakeMailer) RetrySendEmail(ctx context.Context, to, subject, body string, retries int, delay time.Duration) {

}
