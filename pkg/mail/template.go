package mail

import (
	"bytes"
	"context"
	"meditation-plus/pkg/appointment"
	"meditation-plus/pkg/model"
	"meditation-plus/web"
	"text/template"
	"time"
)

type TemplateScheduleUpdate struct {
	RecipientUser        *model.User
	NewAppointments      []model.Appointment
	CanceledAppointments []model.Appointment
	Now                  time.Time
}

func SendScheduleUpdateMail(to string, templateData TemplateScheduleUpdate, mailer Mailer) error {
	funcs := template.FuncMap{
		"DisplayInZone":          appointment.DisplayInZone,
		"DisplayWeekdayInZone":   appointment.DisplayWeekdayInZone,
		"GetNextAppointmentTime": appointment.GetNextAppointmentTime,
	}

	tmpl, err := template.New("schedule_update.tpl").Funcs(funcs).ParseFS(web.Templates, "templates/mail/schedule_update.tpl")
	if err != nil {
		return err
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, templateData)
	if err != nil {
		return err
	}

	err = mailer.SendEmail(context.Background(), to, "Appointment Schedule Updates", buf.String())
	if err != nil {
		return err
	}

	return nil
}
