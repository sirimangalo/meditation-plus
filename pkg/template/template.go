package template

import (
	"bytes"
	"fmt"
	"html/template"
	"io/fs"
	"log/slog"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/validation"
	"meditation-plus/web"
	"net/http"
	"path/filepath"
	"strings"
	"time"
)

var templates map[string]*template.Template
var funcMaps []template.FuncMap

type Templating struct {
	AppEnv       string
	Filter       string
	GlobalData   map[string]interface{}
	FlashHandler security.FlashHandler
}

func NewTemplating(appEnv string, flashHandler security.FlashHandler) *Templating {
	return NewTemplatingWithFilter(appEnv, flashHandler, "")
}

func NewTemplatingWithFilter(appEnv string, flashHandler security.FlashHandler, filter string) *Templating {
	t := Templating{
		AppEnv:       appEnv,
		Filter:       filter,
		GlobalData:   make(map[string]interface{}),
		FlashHandler: flashHandler,
	}
	t.ReadManifest()
	t.ParseTemplates()

	return &t
}

func RegisterFuncMap(funcMap template.FuncMap) {
	funcMaps = append(funcMaps, funcMap)
}

func (t *Templating) AddGlobalData(globalData map[string]interface{}) {
	for key, value := range globalData {
		t.GlobalData[key] = value
	}
}

func (t *Templating) ParseTemplates() {
	var err error

	funcMap := template.FuncMap{
		"GetJsAsset":      t.GetJsAsset,
		"GetAsset":        t.GetAsset,
		"GetCssAssets":    t.GetCssAssets,
		"FormInputClass":  t.FormInputClass,
		"FormInputId":     t.FormInputId,
		"FormLabel":       t.FormLabel,
		"FormErrors":      t.FormErrors,
		"DisplayDatetime": displayDatetime,
		"FormatDateIso":   formatDateIso,
	}

	for _, entry := range funcMaps {
		for name, function := range entry {
			funcMap[name] = function
		}
	}

	templates, err = findAndParseTemplates("templates", funcMap, t.Filter)
	if err != nil {
		panic(err)
	}
}

func displayDatetime(date time.Time) string {
	return date.Format("2006-01-02 15:04")
}

func formatDateIso(date time.Time) string {
	return date.Format(time.RFC3339)
}

func (t *Templating) Render(file string, data map[string]interface{}, w http.ResponseWriter, r *http.Request) {
	if templates == nil {
		t.ParseTemplates()
	}

	slog.DebugContext(r.Context(), "Render template", "template", file)

	data = t.getDefaultData(data, r)

	if !t.templateExists(file) {
		slog.ErrorContext(r.Context(), "Template does not exist", "template", file)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	var buf bytes.Buffer
	err := templates[file].ExecuteTemplate(&buf, "base", data)
	if err != nil {
		slog.ErrorContext(r.Context(), "Error rendering template", "template", file, "error", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	_, err = buf.WriteTo(w)
	if err != nil {
		slog.ErrorContext(r.Context(), "Error writing to buffer", "error", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func (t *Templating) templateExists(file string) bool {
	for key := range templates {
		if key == file {
			return true
		}
	}

	return false
}

func (t *Templating) getDefaultData(data map[string]interface{}, r *http.Request) map[string]interface{} {
	if data == nil {
		data = map[string]interface{}{}
	}

	data["user"] = r.Context().Value(security.UserContextKey)
	data["flashNotice"] = t.FlashHandler.GetFlash(r, "notice")
	data["flashWarn"] = t.FlashHandler.GetFlash(r, "warn")
	data["flashError"] = t.FlashHandler.GetFlash(r, "error")

	for key, value := range t.GlobalData {
		data[key] = value
	}

	return data
}

func (t *Templating) RenderError(errorCode int, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(errorCode)
	t.Render(fmt.Sprintf("errors/%d.html", errorCode), map[string]interface{}{}, w, r)
}

func (t *Templating) FormInputClass(name string, errors validation.ValidationErrors) template.HTML {
	return t.renderFormWidget("input-class", map[string]interface{}{
		"name":   name,
		"errors": errors,
	})
}

func (t *Templating) FormInputId(name string) template.HTML {
	return t.renderFormWidget("input-id", map[string]interface{}{
		"name": name,
	})
}

func (t *Templating) FormLabel(name string, label string) template.HTML {
	return t.renderFormWidget("label", map[string]interface{}{
		"name":  name,
		"label": label,
	})
}

func (t *Templating) FormErrors(name string, errors validation.ValidationErrors) template.HTML {
	return t.renderFormWidget("errors", map[string]interface{}{
		"name":   name,
		"errors": errors,
	})
}

func (t *Templating) renderFormWidget(widget string, data map[string]interface{}) template.HTML {
	buf := bytes.Buffer{}
	widgetTemplate, exists := templates["widgets/"+widget+".html"]

	if !exists {
		slog.Error("Widget is missing", "widget", widget)
		return "Widget is missing."
	}

	err := widgetTemplate.ExecuteTemplate(&buf, widget+".html", data)

	if err != nil {
		slog.Error("Could not render form widget", "error", err)
		return ""
	}
	return template.HTML(buf.String())
}

func findAndParseTemplates(rootDir string, funcMap template.FuncMap, filter string) (map[string]*template.Template, error) {

	baseTemplates := []string{
		rootDir + "/base.html",
		rootDir + "/components/header.html",
		rootDir + "/partials/appointment/form.html",
		rootDir + "/partials/user/form.html",
		rootDir + "/partials/meditation/active.html",
		rootDir + "/widgets/errors.html",
		rootDir + "/widgets/label.html",
		rootDir + "/widgets/input-class.html",
		rootDir + "/widgets/input-id.html",
	}
	cache := map[string]*template.Template{}
	cleanRoot := filepath.Clean(rootDir)
	pfx := len(cleanRoot) + 1

	err := fs.WalkDir(web.Templates, rootDir, func(path string, d fs.DirEntry, e1 error) error {
		if !d.IsDir() && strings.HasSuffix(path, ".html") {
			if e1 != nil {
				return e1
			}

			name := path[pfx:]
			if filter != "" && !strings.HasPrefix(name, "errors") && !strings.HasPrefix(name, filter) {
				return nil
			}

			templates := append(baseTemplates, rootDir+"/"+name)
			t, e2 := template.New(name).Funcs(funcMap).ParseFS(web.Templates, templates...)
			if e2 != nil {
				slog.Error("Failed to parse template", "template", name, "filter", filter, "error", e2)
				return e2
			}
			cache[name] = t
		}

		return nil
	})

	return cache, err
}
