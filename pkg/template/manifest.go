package template

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log/slog"
	"meditation-plus/web"
	"os"
	"strings"
)

var manifest map[string]Asset

type Asset struct {
	Js  string `json:"js"`
	Css string `json:"css,omitempty"`
	Out string `json:"out,omitempty"`
}

var prefix string

func (t *Templating) ReadManifest() {
	slog.Debug("Reading manifest")

	if t.AppEnv == "dev" {
		prefix = "/dev/static"

		manifestJson, err := os.ReadFile("./web/static/assets/manifest.json")
		if err != nil {
			panic(err)
		}

		if err := json.Unmarshal(manifestJson, &manifest); err != nil {
			slog.Error("Error while parsing manifest", "error", err)
			return
		}
		return
	}

	prefix = "/static"

	if err := json.Unmarshal(web.ManifestJson, &manifest); err != nil {
		slog.Error("Error while parsing manifest", "error", err)
		return
	}
}

func (t *Templating) GetAsset(asset string) string {
	if t.AppEnv == "dev" {
		t.ReadManifest()
	}

	if asset, ok := manifest[asset]; ok {
		filename := strings.Replace(asset.Out, "web/static/", "", -1)
		return fmt.Sprintf("%s/%s", prefix, filename)
	}

	filename := strings.Replace(asset, "web/static/", "", -1)
	return fmt.Sprintf("%s/%s", prefix, filename)
}

func (t *Templating) GetJsAsset(entrypoint string) template.HTML {
	if t.AppEnv == "dev" {
		t.ReadManifest()
	}

	var assets string

	if asset, ok := manifest[entrypoint]; ok {
		filename := strings.Replace(asset.Js, "web/static/", "", -1)
		assets += fmt.Sprintf(`<script src="%s/%s" type="module"></script>`, prefix, filename)
	} else {
		return template.HTML("")
	}

	return template.HTML(assets)
}

func (t *Templating) GetCssAssets(entrypoint string) template.HTML {
	if t.AppEnv == "dev" {
		t.ReadManifest()
	}

	var assets string

	if asset, ok := manifest[entrypoint]; ok {
		if asset.Css != "" {
			filename := strings.Replace(asset.Css, "web/static/", "", -1)
			assets += fmt.Sprintf(`<link href="%s/%s" rel="stylesheet">`, prefix, filename)
		}
	} else {
		return template.HTML("")
	}

	return template.HTML(assets)
}
