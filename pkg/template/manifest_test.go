package template

import (
	"html/template"
	"meditation-plus/pkg/test"
	"testing"
)

func TestShouldHandleJsOnly(t *testing.T) {
	templ := &Templating{}
	manifest = make(map[string]Asset)
	manifest["entry"] = Asset{
		Js: "web/static/assets/entry.js",
	}
	result := templ.GetJsAsset("entry")
	test.AssertEquals(t, template.HTML(`<script src="/assets/entry.js" type="module"></script>`), result)
	result = templ.GetCssAssets("entry")
	test.AssertEquals(t, template.HTML(""), result)
}

func TestShouldHandleRawAsset(t *testing.T) {
	templ := &Templating{}
	manifest = make(map[string]Asset)
	manifest["entry"] = Asset{
		Out: "web/static/assets/img.svg",
	}
	result := templ.GetAsset("entry")
	test.AssertEquals(t, "/assets/img.svg", result)
}

func TestShouldHandleRawAssetEvenIfNotExisting(t *testing.T) {
	templ := &Templating{}
	manifest = make(map[string]Asset)
	manifest["entry"] = Asset{
		Out: "web/static/assets/img.svg",
	}
	result := templ.GetAsset("entry/not/existing.svg")
	test.AssertEquals(t, "/entry/not/existing.svg", result)
}

func TestShouldHandleCssAndJs(t *testing.T) {
	templ := &Templating{}
	manifest = make(map[string]Asset)
	manifest["entry"] = Asset{
		Js:  "web/static/assets/entry.js",
		Css: "web/static/assets/entry.css",
	}
	result := templ.GetJsAsset("entry")
	test.AssertEquals(t, template.HTML(`<script src="/assets/entry.js" type="module"></script>`), result)
	result = templ.GetCssAssets("entry")
	test.AssertEquals(t, template.HTML(`<link href="/assets/entry.css" rel="stylesheet">`), result)
}
