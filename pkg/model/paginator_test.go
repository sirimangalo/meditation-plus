package model

import (
	"testing"
)

func TestGetPages(t *testing.T) {
	cases := map[string]struct {
		PageCount int
		CurPage   int
		Result    []int
	}{
		"should not reduce under 7 pages":                     {PageCount: 6, CurPage: 1, Result: []int{1, 2, 3, 4, 5, 6}},
		"should reduce after 7 pages":                         {PageCount: 7, CurPage: 1, Result: []int{1, 2, 3, 4, 0, 7}},
		"should reduce when at the end":                       {PageCount: 7, CurPage: 7, Result: []int{1, 0, 4, 5, 6, 7}},
		"should reduce when before the end":                   {PageCount: 7, CurPage: 5, Result: []int{1, 0, 4, 5, 6, 7}},
		"should reduce when after the beginning":              {PageCount: 7, CurPage: 3, Result: []int{1, 2, 3, 4, 0, 7}},
		"should reduce when after the beginning before split": {PageCount: 7, CurPage: 4, Result: []int{1, 0, 3, 4, 5, 0, 7}},
		"should reduce when at the middle more pages":         {PageCount: 9, CurPage: 5, Result: []int{1, 0, 4, 5, 6, 0, 9}},
	}

	for index, testCase := range cases {
		result := GetPages(testCase.PageCount, testCase.CurPage)
		if !compareSlices(result, testCase.Result) {
			t.Errorf("%s: Got %v, want %v", index, result, testCase.Result)
		}
	}
}

func compareSlices(slice1, slice2 []int) bool {
	if len(slice1) != len(slice2) {
		return false
	}

	for i := range slice1 {
		if slice1[i] != slice2[i] {
			return false
		}
	}

	return true
}
