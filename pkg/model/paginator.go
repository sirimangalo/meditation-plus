package model

const splitAtPage = 7
const showPages = 4

func GetPages(pageCount int, currentPage int) []int {
	var pages []int

	if pageCount < splitAtPage {
		for i := 1; i <= pageCount; i++ {
			pages = append(pages, i)
		}
		return pages
	}

	pages = append(pages, 1)

	isAtBeginning := currentPage < showPages
	isAtEnd := currentPage > pageCount-3

	if isAtBeginning {
		for i := 2; i <= showPages; i++ {
			pages = append(pages, i)
		}
		return append(pages, 0, pageCount)
	}

	if isAtEnd {
		pages = append(pages, 0)
		for i := pageCount - 3; i <= pageCount; i++ {
			pages = append(pages, i)
		}
		return pages
	}

	// is at middle
	pages = append(pages, 0)
	for i := currentPage - 1; i <= currentPage+1; i++ {
		pages = append(pages, i)
	}
	return append(pages, 0, pageCount)
}
