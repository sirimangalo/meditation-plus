package model

import (
	"database/sql"
	"meditation-plus/pkg/security"
	"time"
)

const USER_ROLES_ADMIN_TEACHER = "ADMIN_TEACHER"
const USER_ROLES_ADMIN = "ADMIN"
const USER_ROLES_TEACHER = "TEACHER"
const USER_ROLES_USER = "USER"

func (u *User) GetId() int {
	return int(u.Id)
}

func (u *User) IsGranted(role string) bool {
	if role == u.Role {
		return true
	}

	switch u.Role {
	case USER_ROLES_TEACHER:
		return role == USER_ROLES_USER
	case USER_ROLES_ADMIN:
		return role == USER_ROLES_USER
	case USER_ROLES_ADMIN_TEACHER:
		return true
	default:
		return false
	}
}

func (u *User) AuthHasChanged(otherUser *User) bool {
	return u.Username != otherUser.Username || u.Email != otherUser.Email || u.Role != otherUser.Role
}

func (u *User) UpdateFromDiscord(userData map[string]interface{}) error {
	email, ok := userData["email"].(string)
	if !ok || email == "" {
		return security.ErrUserDataInvalid
	}

	avatar, ok := userData["avatar"].(string)
	if !ok {
		avatar = ""
	}

	u.Email = email
	u.ImageId = sql.NullString{
		Valid:  avatar != "",
		String: avatar,
	}

	return nil
}

func (u *User) Active() {
	u.LastActivity = sql.NullTime{
		Valid: true,
		Time:  time.Now(),
	}
}

func (u *User) Logout() {
	u.LastActivity = sql.NullTime{
		Valid: false,
	}
}
