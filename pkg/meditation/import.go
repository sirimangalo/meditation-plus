package meditation

import (
	"context"
	"fmt"
	"log/slog"
	"meditation-plus/pkg/errors"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/validation"
	"net/http"
)

func (s *Service) Import(
	w http.ResponseWriter,
	r *http.Request,
	persistQuery PersistQuery,
	userInRangeQuery UserInRangeQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	user := security.GetUser(r).(*model.User)
	violations := make(validation.ValidationErrors)
	formRequest := &MeditationUploadRequest{}

	if r.Method == http.MethodPost {
		formRequest = MeditationUploadRequestFromForm(r)

		if formRequest == nil {
			violations["file"] = append(violations["file"], "Upload file is missing.")
		} else {
			violations = formRequest.Validate()
		}

		if len(violations) == 0 {
			imported, validationErrors, err := s.doImport(r.Context(), formRequest, user, persistQuery, userInRangeQuery)
			if err != nil {
				templating.RenderError(errors.GetErrorHttpCode(err), w, r)
				return
			}

			if len(validationErrors) == 0 {
				flashHandler.AddFlash(r, "notice", fmt.Sprintf("%d meditation sessions have been created.", imported))
				http.Redirect(w, r, "/", http.StatusSeeOther)

				return
			}

			violations = validationErrors
		}
	}

	templating.Render("meditation/import.html", map[string]interface{}{
		"errors":         violations,
		"data":           formRequest,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) doImport(
	ctx context.Context,
	formRequest *MeditationUploadRequest,
	user *model.User,
	persistQuery PersistQuery,
	userInRangeQuery UserInRangeQuery,
) (int, validation.ValidationErrors, error) {
	var validationErrors validation.ValidationErrors = make(validation.ValidationErrors, 0)

	if formRequest == nil {
		slog.Error("FormRequest is nil")
		return 0, validationErrors, errors.NewServiceError(http.StatusInternalServerError, nil)
	}

	meditations, err := formRequest.ToModel(user)
	if err != nil {
		slog.Error("Could not convert request to model", "error", err)
		return 0, validationErrors, errors.NewServiceError(http.StatusInternalServerError, err)
	}

	count := 0

	for index, meditationRequest := range meditations {
		validationErrors = meditationRequest.Validate(ctx, index, user, userInRangeQuery)

		if len(validationErrors) > 0 {
			return count, validationErrors, nil
		}
		meditation, err := meditationRequest.ToModel(user)
		if err != nil {
			slog.ErrorContext(ctx, "Could not convert request to model", "error", err)
			return count, validationErrors, errors.NewServiceError(http.StatusInternalServerError, err)
		}

		err = persistQuery.Persist(ctx, meditation)

		if err != nil {
			slog.ErrorContext(ctx, "Could not save meditation", "error", err)
			return count, validationErrors, errors.NewServiceError(http.StatusInternalServerError, err)
		}

		count++
	}

	return count, validationErrors, nil
}
