package meditation

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/user"
	"net/http"
)

func (s *Service) Convert(
	w http.ResponseWriter,
	r *http.Request,
	listMigratedByOldUserUidNotMatched ListMigratedByOldUserUidNotMatchedQuery,
	convertMeditationsQuery ConvertMeditationsQuery,
	deleteMeditationByUserBeforeDateQuery DeleteMeditationByUserBeforeDateQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	user := security.GetUser(r).(*model.User)

	if !user.OldUserUid.Valid {
		http.Redirect(w, r, "/meditations/convert/login/", http.StatusSeeOther)
		return
	}

	var sessions []model.MigratedMeditation
	var err error

	sessions, err = listMigratedByOldUserUidNotMatched.ListMigratedByOldUserUidNotMatched(r.Context(), user.OldUserUid.String)

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not list migrated sessions", "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	if r.Method == http.MethodPost {
		err := s.doConvert(r.Context(), user, convertMeditationsQuery, deleteMeditationByUserBeforeDateQuery)

		if err != nil {
			templating.RenderError(http.StatusInternalServerError, w, r)
			return
		}

		flashHandler.AddFlash(r, "notice", "Meditation sessions have been imported.")
		http.Redirect(w, r, "/", http.StatusSeeOther)

		return
	}

	templating.Render("meditation/convert.html", map[string]interface{}{
		"sessions":       sessions,
		"maxIndex":       len(sessions) - 1,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) doConvert(
	ctx context.Context,
	user *model.User,
	convertMeditationsQuery ConvertMeditationsQuery,
	deleteMeditationByUserBeforeDateQuery DeleteMeditationByUserBeforeDateQuery,
) error {
	err := deleteMeditationByUserBeforeDateQuery.DeleteMeditationByUserBeforeDate(ctx, user.Id, user.CreatedAt)

	if err != nil {
		slog.ErrorContext(ctx, "Could not delete meditations", "error", err)
		return err
	}

	err = convertMeditationsQuery.ConvertMeditations(ctx, user.Id, user.OldUserUid.String)

	if err != nil {
		slog.ErrorContext(ctx, "Could not save meditation", "error", err)
		return err
	}

	return nil
}

type loginResponse struct {
	Profile struct {
		ID string `json:"_id"`
	} `json:"profile"`
}

func (s *Service) Login(
	w http.ResponseWriter,
	r *http.Request,
	persistQuery user.PersistQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	loginErr := false
	user := security.GetUser(r).(*model.User)

	if user.OldUserUid.Valid {
		http.Redirect(w, r, "/meditations/convert/", http.StatusSeeOther)
		return
	}

	if r.Method == http.MethodPost {
		ok, err := s.doLogin(r.Context(), r, user, persistQuery)

		if err != nil {
			slog.ErrorContext(r.Context(), err.Error())
			templating.RenderError(http.StatusInternalServerError, w, r)
			return
		}

		if ok {
			flashHandler.AddFlash(r, "notice", "Login was successful.")
			http.Redirect(w, r, "/meditations/convert/", http.StatusSeeOther)

			return
		}

		loginErr = true
	}

	templating.Render("meditation/login.html", map[string]interface{}{
		"loginErr":       loginErr,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) doLogin(
	ctx context.Context,
	r *http.Request,
	user *model.User,
	persistQuery user.PersistQuery,
) (bool, error) {
	url := "https://meditation-old.sirimangalo.org/auth/login"

	payload := map[string]string{
		"email":    r.PostFormValue("username"),
		"password": r.PostFormValue("password"),
	}

	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		return false, fmt.Errorf("Error while encoding json: %s", err)
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payloadBytes))
	if err != nil {
		return false, fmt.Errorf("Error while creating request: %s", err)
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil || resp == nil {
		return false, fmt.Errorf("Error while sending request: %s", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return false, nil
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return false, fmt.Errorf("Error while reading response: %s", err)
	}

	var loginResponse loginResponse
	err = json.Unmarshal(body, &loginResponse)
	if err != nil {
		return false, fmt.Errorf("Error while decoding response: %s", err)
	}

	user.OldUserUid = sql.NullString{
		Valid:  true,
		String: loginResponse.Profile.ID,
	}

	err = persistQuery.Persist(ctx, user)

	if err != nil {
		return true, fmt.Errorf("Could not update user: %s", err)
	}

	return true, nil
}
