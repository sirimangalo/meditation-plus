package meditation

import (
	"context"
	"fmt"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/user"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"golang.org/x/net/html"
)

func TestIndex(t *testing.T) {
	userA := user.NewFakeUser(1)
	userNotWelcomed := user.NewFakeUser(2)
	userNotWelcomed.Welcomed = false
	userNoTimezone := user.NewFakeUser(3)
	userNoTimezone.Timezone.String = ""
	userNoTimezone.Timezone.Valid = false

	activeMeditation := &model.Meditation{
		UserId: userA.Id,
	}
	list := []model.FindPastMeditationsRow{
		{UserId: 1, UserName: "Username", DisplayName: "Displayname", Sitting: 0, Walking: 30},
	}
	listCur := []model.FindCurrentMeditationsRow{
		{UserId: 1, UserName: "Username", DisplayName: "Displayname", Sitting: 0, Walking: 30},
	}

	cases := map[string]struct {
		Method           string
		ActiveMeditation *model.Meditation
		PastMeditations  []model.FindPastMeditationsRow
		CurMeditations   []model.FindCurrentMeditationsRow
		SecurityUser     security.SecurityUser
		BodyContains     string
		Result           int
	}{
		"200": {
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"should show active session": {
			SecurityUser:     userA,
			ActiveMeditation: activeMeditation,
			Result:           http.StatusOK,
		},
		"should show meditations": {
			SecurityUser:    userA,
			PastMeditations: list,
			Result:          http.StatusOK,
		},
		"should show current meditations": {
			SecurityUser:   userA,
			CurMeditations: listCur,
			Result:         http.StatusOK,
		},
		"should redirect when not welcomed": {
			SecurityUser: userNotWelcomed,
			Result:       http.StatusSeeOther,
		},
		"should show timezone hint": {
			SecurityUser: userNoTimezone,
			Result:       http.StatusOK,
			BodyContains: "Your profile is missing a timezone setting",
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		meditationRepository := &FakeRepository{ActiveMeditation: testCase.ActiveMeditation, PastMeditations: testCase.PastMeditations, CurrentMeditations: testCase.CurMeditations}
		templating := template.NewTemplatingWithFilter("test", session, "meditation")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Index(w, r, meditationRepository, meditationRepository, meditationRepository, meditationRepository, templating)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}

		if testCase.ActiveMeditation != nil {
			bodyString := recorder.Body.String()
			body, _ := html.Parse(recorder.Body)
			node := test.GetFirstElementByClassName(body, "status")
			if node == nil || !test.NodeContainsText(node, "Meditating") {
				fmt.Printf("Body %s", bodyString)
				t.Fatalf(`Expected .status to contain text "%s" for test case %s`, "Meditating", index)
			}
		}

		if testCase.PastMeditations != nil {
			bodyString := recorder.Body.String()
			body, _ := html.Parse(recorder.Body)
			node := test.GetFirstElementByClassName(body, "profile-link")
			if node == nil || !test.NodeContainsText(node, "Displayname") {
				fmt.Printf("Body %s", bodyString)
				t.Fatalf(`Expected .meditation-list-entry to contain text "%s" for test case %s`, "Displayname", index)
			}
		}

		if testCase.BodyContains != "" {
			bodyString := recorder.Body.String()
			if !strings.Contains(bodyString, testCase.BodyContains) {
				fmt.Printf("Body %s", bodyString)
				t.Fatalf(`Expected body to contain text "%s" for test case %s`, testCase.BodyContains, index)
			}
		}
	}
}

func TestPast(t *testing.T) {
	userA := user.NewFakeUser(1)
	userA.CreatedAt, _ = time.Parse(time.DateOnly, "2020-01-01")

	validData := map[string]string{
		"walking":    "60",
		"sitting":    "30",
		"start_date": "2020-01-01",
		"end_date":   "2020-01-01",
		"start_time": "10:00",
	}

	cases := map[string]struct {
		Method       string
		FormData     map[string]string
		SecurityUser security.SecurityUser
		Result       int
	}{
		"200 on GET": {
			Method:       "GET",
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"200 with errors on invalid data": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"303 on valid data": {
			Method:       "POST",
			FormData:     validData,
			SecurityUser: userA,
			Result:       http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", test.CreateFormData(testCase.FormData))
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		meditationRepository := &FakeRepository{}
		templating := template.NewTemplatingWithFilter("test", session, "meditation")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.LogPast(w, r, meditationRepository, meditationRepository, meditationRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestStart(t *testing.T) {
	userA := user.NewFakeUser(1)

	validData := map[string]string{
		"walking": "60",
		"sitting": "30",
	}
	autofillData := map[string]string{
		"walking": "",
		"sitting": "30",
	}
	activeMeditation := &model.Meditation{
		UserId: userA.Id,
	}

	cases := map[string]struct {
		Method           string
		ActiveMeditation *model.Meditation
		FormData         map[string]string
		SecurityUser     security.SecurityUser
		Result           int
	}{
		"200 with errors on invalid data": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"303 on valid data": {
			Method:       "POST",
			FormData:     validData,
			SecurityUser: userA,
			Result:       http.StatusSeeOther,
		},
		"303 on autofill data": {
			Method:       "POST",
			FormData:     autofillData,
			SecurityUser: userA,
			Result:       http.StatusSeeOther,
		},
		"400 when there already is an active session": {
			Method:           "POST",
			FormData:         validData,
			ActiveMeditation: activeMeditation,
			SecurityUser:     userA,
			Result:           http.StatusBadRequest,
		},
		"303 when there already is an active session for GET": {
			Method:           "GET",
			ActiveMeditation: activeMeditation,
			SecurityUser:     userA,
			Result:           http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", test.CreateFormData(testCase.FormData))
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		meditationRepository := &FakeRepository{ActiveMeditation: testCase.ActiveMeditation}
		templating := template.NewTemplatingWithFilter("test", session, "meditation")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Start(w, r, meditationRepository, meditationRepository, meditationRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestStop(t *testing.T) {
	userA := user.NewFakeUser(1)

	activeMeditation := &model.Meditation{
		UserId:  userA.Id,
		End:     time.Now().Add(time.Duration(60) * time.Minute),
		Walking: 30,
		Sitting: 30,
	}

	activeMeditationWithRestSitting := &model.Meditation{
		UserId:  userA.Id,
		End:     time.Now().Add(time.Duration(30) * time.Minute),
		Walking: 20,
		Sitting: 40,
	}

	activeMeditationWithRestWalking := &model.Meditation{
		UserId:  userA.Id,
		End:     time.Now().Add(time.Duration(30) * time.Minute),
		Walking: 50,
		Sitting: 10,
	}

	cases := map[string]struct {
		Method           string
		ActiveMeditation *model.Meditation
		SecurityUser     security.SecurityUser
		RemainingSitting int
		RemainingWalking int
		Result           int
	}{
		"400 on no active session": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusBadRequest,
		},
		"303 on valid request and should delete if 0": {
			Method:           "POST",
			ActiveMeditation: activeMeditation,
			SecurityUser:     userA,
			RemainingSitting: 0,
			RemainingWalking: 0,
			Result:           http.StatusSeeOther,
		},
		"303 on valid request and with rest walking": {
			Method:           "POST",
			ActiveMeditation: activeMeditationWithRestWalking,
			SecurityUser:     userA,
			RemainingSitting: 0,
			RemainingWalking: 30,
			Result:           http.StatusSeeOther,
		},
		"303 on valid request and with rest sitting": {
			Method:           "POST",
			ActiveMeditation: activeMeditationWithRestSitting,
			SecurityUser:     userA,
			RemainingSitting: 10,
			RemainingWalking: 20,
			Result:           http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		meditationRepository := &FakeRepository{ActiveMeditation: testCase.ActiveMeditation}
		templating := template.NewTemplatingWithFilter("test", session, "meditation")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Stop(w, r, meditationRepository, meditationRepository, meditationRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}

		if testCase.ActiveMeditation != nil {
			remainingTime := testCase.ActiveMeditation.Sitting + testCase.ActiveMeditation.Walking

			if testCase.ActiveMeditation.Sitting != int16(testCase.RemainingSitting) {
				t.Fatalf("Expected meditation session to have a remaining sitting time of %d, but received %d.", testCase.RemainingSitting, testCase.ActiveMeditation.Sitting)
			}

			if testCase.ActiveMeditation.Walking != int16(testCase.RemainingWalking) {
				t.Fatalf("Expected meditation session to have a remaining walking time of %d, but received %d.", testCase.RemainingWalking, testCase.ActiveMeditation.Walking)
			}

			if remainingTime == 0 && !meditationRepository.Deleted {
				t.Fatalf("Expected empty meditation session to have been deleted.")
			}

			if remainingTime > 0 && !meditationRepository.Persisted {
				t.Fatalf("Expected meditation session to have been updated.")
			}
		}
	}
}

func TestList(t *testing.T) {
	meditations := []model.Meditation{
		{},
		{},
	}

	cases := map[string]struct {
		Meditations []model.Meditation
		Result      int
	}{
		"should list": {Meditations: meditations, Result: http.StatusOK},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, user.NewFakeUser(1))
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		repo := &FakeRepository{UserMeditations: testCase.Meditations}
		templating := template.NewTemplatingWithFilter("test", session, "meditation")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.List(w, r, repo, repo, templating)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestDelete(t *testing.T) {
	ownUser := user.NewFakeUser(1)
	otherUser := user.NewFakeUser(2)
	meditation := &model.Meditation{UserId: ownUser.Id}
	meditationOtherUser := &model.Meditation{UserId: otherUser.Id}

	cases := map[string]struct {
		Method     string
		User       *model.User
		Meditation *model.Meditation
		Result     int
	}{
		"404 on wrong user": {
			Method:     "POST",
			User:       ownUser,
			Meditation: meditationOtherUser,
			Result:     http.StatusNotFound,
		},
		"302 on valid request": {
			Method:     "POST",
			User:       ownUser,
			Meditation: meditation,
			Result:     http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		if testCase.User != nil {
			request.SetPathValue("id", fmt.Sprintf("%d", testCase.Meditation.Id))
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.User)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		repo := &FakeRepository{Meditation: testCase.Meditation}
		templating := template.NewTemplatingWithFilter("test", session, "admin/user")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Delete(w, r, repo, repo, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}
