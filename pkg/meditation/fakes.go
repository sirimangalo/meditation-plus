package meditation

import (
	"context"
	"meditation-plus/pkg/model"
	"time"
)

type FakeRepository struct {
	ActiveMeditation   *model.Meditation
	Meditation         *model.Meditation
	CurrentMeditations []model.FindCurrentMeditationsRow
	PastMeditations    []model.FindPastMeditationsRow
	UserMeditations    []model.Meditation
	MeditationInRange  bool
	Persisted          bool
	Deleted            bool
}

func (q *FakeRepository) Persist(ctx context.Context, meditation *model.Meditation) error {
	q.Persisted = true
	return nil
}
func (q *FakeRepository) Delete(ctx context.Context, meditation *model.Meditation) error {
	q.Deleted = true
	return nil
}
func (q *FakeRepository) FindActiveByUserId(ctx context.Context, userId int) (*model.Meditation, error) {
	return q.ActiveMeditation, nil
}
func (q *FakeRepository) UserHasMeditationInRange(ctx context.Context, userId int, start time.Time, end time.Time) (bool, error) {
	return q.MeditationInRange, nil
}
func (r *FakeRepository) FindCurrentMeditations(ctx context.Context) ([]model.FindCurrentMeditationsRow, error) {
	return r.CurrentMeditations, nil
}
func (r *FakeRepository) FindPastMeditations(ctx context.Context) ([]model.FindPastMeditationsRow, error) {
	return r.PastMeditations, nil
}
func (r *FakeRepository) UpdateDailyStats(ctx context.Context, day time.Time) error {
	return nil
}
func (r *FakeRepository) GetDailyStats(ctx context.Context) ([]model.MeditationStatsDaily, error) {
	return nil, nil
}
func (r *FakeRepository) Paginate(ctx context.Context, userId int64, page int) ([]model.Meditation, error) {
	return r.UserMeditations, nil
}
func (r *FakeRepository) Count(ctx context.Context, userId int64) (int, error) {
	return len(r.UserMeditations), nil
}
func (r *FakeRepository) FindById(ctx context.Context, id int) (*model.Meditation, error) {
	return r.Meditation, nil
}
