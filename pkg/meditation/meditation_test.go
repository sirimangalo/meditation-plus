package meditation

import (
	"database/sql"
	"log/slog"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/user"
	"testing"
	"time"
)

func TestPastMeditationSingleDate(t *testing.T) {
	user := user.NewFakeUser(1)
	request := &MeditationPastRequest{
		MeditationStartRequest: &MeditationStartRequest{
			Sitting: "30",
			Walking: "30",
		},
		StartDate: "2020-01-01",
		EndDate:   "2020-01-01",
		StartTime: "10:00",
	}
	meditations, _ := request.ToModel(user)

	if len(meditations) != 1 {
		t.Fatalf("Expected one meditation session, got %d.", len(meditations))
	}
}
func TestPastMeditationTwoDates(t *testing.T) {
	user := user.NewFakeUser(1)
	user.Timezone = sql.NullString{
		Valid:  true,
		String: "Europe/Berlin",
	}
	request := &MeditationPastRequest{
		MeditationStartRequest: &MeditationStartRequest{
			Sitting: "30",
			Walking: "30",
		},
		StartDate: "2020-01-01",
		EndDate:   "2020-01-02",
		StartTime: "23:00",
	}
	meditations, _ := request.ToModel(user)

	if len(meditations) != 2 {
		slog.Info("Meditations", "Meditations", meditations)
		t.Fatalf("Expected two meditation session, got %d.", len(meditations))
	}

	test.AssertEquals(t, "2020-01-01 23:00:00", meditations[0].Start.Format(time.DateTime))
	test.AssertEquals(t, "2020-01-02 00:00:00", meditations[0].End.Format(time.DateTime))
	test.AssertEquals(t, "2020-01-02 23:00:00", meditations[1].Start.Format(time.DateTime))
	test.AssertEquals(t, "2020-01-03 00:00:00", meditations[1].End.Format(time.DateTime))
}
