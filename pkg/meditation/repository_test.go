package meditation

import (
	"context"
	"meditation-plus/pkg/database"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/user"
	"testing"
	"time"
)

func testRepositoryCreateUser(repo *user.Repository, name string) (*model.User, error) {
	user, err := repo.CreateUser(map[string]interface{}{
		"username": name,
		"id":       name,
		"email":    name,
	})

	if user != nil {
		return user.(*model.User), err
	}

	return nil, err
}

func TestRepositorysUpdate(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	meditation := &model.Meditation{
		Walking: 10,
		Sitting: 5,
		Start:   time.Now(),
		UserId:  2,
	}
	_ = repo.Persist(context.Background(), meditation)

	if meditation.Id == 0 {
		t.Fatalf("Expected meditation to have an id")
	}

	meditation.UserId = 3

	_ = repo.Persist(context.Background(), meditation)
	meditation, err := repo.FindById(context.Background(), int(meditation.Id))
	test.AssertNilErr(t, err)
	if meditation == nil {
		t.Fatalf("Expected meditation not to be nil")
	}
	test.AssertEquals(t, int64(3), meditation.UserId)
}

func TestRepositoryGetStats(t *testing.T) {
	database.SetUpRepositoryTest()
	userRepo := user.NewRepository()
	repo := NewRepository()
	stats, err := userRepo.GetStats(context.Background(), 1)
	test.AssertNilErr(t, err)
	if stats == nil {
		t.Fatalf("Expected stats not to be nil")
	}
	test.AssertEquals(t, int64(0), stats.Sitting)
	test.AssertEquals(t, int64(0), stats.Walking)
	test.AssertEquals(t, int16(0), stats.Sessions)

	meditation := &model.Meditation{
		Walking: 10,
		Sitting: 5,
		Start:   time.Now(),
		UserId:  1,
	}
	_ = repo.Persist(context.Background(), meditation)

	meditation = &model.Meditation{
		Walking: 11,
		Sitting: 6,
		Start:   time.Now(),
		UserId:  1,
	}
	_ = repo.Persist(context.Background(), meditation)

	stats, err = userRepo.GetStats(context.Background(), 1)
	test.AssertNilErr(t, err)
	if stats == nil {
		t.Fatalf("Expected stats not to be nil")
	}
	test.AssertEquals(t, int64(11), stats.Sitting)
	test.AssertEquals(t, int64(21), stats.Walking)
	test.AssertEquals(t, int16(2), stats.Sessions)
}

func TestRepositoryFindActiveByUserId(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()
	userRepo := user.NewRepository()
	user, err := testRepositoryCreateUser(userRepo, "active_by_user_id")
	test.AssertNilErr(t, err)
	if user == nil {
		t.Fatalf("Expected user not to be nil")
	}
	res, err := repo.FindActiveByUserId(context.Background(), user.GetId())
	test.AssertNilErr(t, err)
	if res != nil {
		t.Fatalf("Expected not to find an active meditation")
	}

	meditation := &model.Meditation{
		Walking: 11,
		Sitting: 6,
		Start:   time.Now(),
		End:     time.Now().AddDate(0, 0, 1),
		UserId:  int64(user.GetId()),
	}
	_ = repo.Persist(context.Background(), meditation)

	res, err = repo.FindActiveByUserId(context.Background(), user.GetId())
	test.AssertNilErr(t, err)
	if res == nil {
		t.Fatalf("Expected to find an active meditation")
	}
}

func TestRepositoryFindCurrentMeditations(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	meditation := &model.Meditation{
		Walking: 11,
		Sitting: 6,
		Start:   time.Now(),
		End:     time.Now().AddDate(0, 0, 1),
		UserId:  1,
	}

	_ = repo.Persist(context.Background(), meditation)
	res, err := repo.FindCurrentMeditations(context.Background())
	test.AssertNilErr(t, err)

	if len(res) == 0 {
		t.Fatalf("Expected to find current meditations")
	}
}

func TestRepositoryFindPastMeditations(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	res, err := repo.FindPastMeditations(context.Background())
	test.AssertNilErr(t, err)

	if len(res) != 0 {
		t.Fatalf("Expected not to find past meditations")
	}

	meditation := &model.Meditation{
		Walking: 11,
		Sitting: 6,
		Start:   time.Now(),
		End:     time.Now().Add(time.Duration(60) * -time.Minute),
		UserId:  1,
	}

	_ = repo.Persist(context.Background(), meditation)

	res, err = repo.FindPastMeditations(context.Background())
	test.AssertNilErr(t, err)

	if len(res) == 0 {
		t.Fatalf("Expected to find past meditations")
	}
}

func TestRepositoryUserHasMeditationInRange(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	start := time.Now().Add(time.Duration(240) * time.Hour).Truncate(time.Second)
	end := time.Now().Add(time.Duration(241) * time.Hour).Truncate(time.Second)

	res, err := repo.UserHasMeditationInRange(context.Background(), 1, start, end)
	test.AssertNilErr(t, err)

	if res {
		t.Fatalf("Expected not find to meditation in range")
	}

	meditation := &model.Meditation{
		Walking: 11,
		Sitting: 6,
		UserId:  1,
		Start:   start,
		End:     end,
	}

	err = repo.Persist(context.Background(), meditation)
	test.AssertNilErr(t, err)

	res, err = repo.UserHasMeditationInRange(context.Background(), 1, start, end)
	test.AssertNilErr(t, err)

	if !res {
		t.Fatalf("Expected to find meditation in range")
	}
}

func TestRepositoryDelete(t *testing.T) {
	database.SetUpRepositoryTest()
	repo := NewRepository()

	meditation := &model.Meditation{
		Walking: 11,
		Sitting: 6,
		UserId:  1,
	}

	err := repo.Persist(context.Background(), meditation)
	test.AssertNilErr(t, err)

	err = repo.Delete(context.Background(), meditation)
	test.AssertNilErr(t, err)

	meditation, err = repo.FindById(context.Background(), int(meditation.Id))
	test.AssertNilErr(t, err)
	if meditation != nil {
		t.Fatalf("Expected to receive nil meditation (got: %v)", meditation)
	}
}
