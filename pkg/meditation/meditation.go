package meditation

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"log/slog"
	"math"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/validation"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"
)

func StopMeditation(m *model.Meditation) {
	now := time.Now()

	if m.End.Before(now) {
		return
	}

	diff := m.End.Sub(now)
	minutes := int16(math.Ceil(diff.Minutes()))

	if minutes <= m.Sitting {
		m.Sitting -= minutes
	} else {
		remainingMinutes := minutes - m.Sitting
		m.Sitting = 0
		m.Walking -= remainingMinutes
	}

	m.End = now
}

type MeditationStartRequest struct {
	Walking string
	Sitting string
}

func (r *MeditationStartRequest) Validate() validation.ValidationErrors {
	var errors validation.ValidationErrors = make(validation.ValidationErrors, 0)

	validation.Int(errors, "sitting", r.Sitting, "")
	validation.Int(errors, "walking", r.Walking, "")

	walking, _ := strconv.Atoi(r.Walking)
	sitting, _ := strconv.Atoi(r.Sitting)

	duration := walking + sitting

	validation.Min(errors, "sitting", 0, int(sitting), "")
	validation.Min(errors, "walking", 0, int(walking), "")
	validation.Min(errors, "sitting", 1, int(duration), "You cannot post empty meditation sessions.")
	validation.Max(errors, "sitting", 5*60, int(sitting), "")
	validation.Max(errors, "walking", 5*60, int(walking), "")

	return errors
}

func (r *MeditationStartRequest) ToModel(user *model.User) (*model.Meditation, error) {
	walking, _ := strconv.Atoi(r.Walking)
	sitting, _ := strconv.Atoi(r.Sitting)
	localicedNow := getUserLocalizedNow(user)
	end := localicedNow.Add(time.Duration(walking+sitting) * time.Minute)

	return &model.Meditation{
		Walking:   int16(walking),
		Sitting:   int16(sitting),
		End:       end,
		Start:     localicedNow,
		Timezone:  localicedNow.Location().String(),
		UserId:    user.Id,
		CreatedAt: time.Now(),
	}, nil
}

func MeditationStartRequestFromForm(r *http.Request) *MeditationStartRequest {
	sitting := r.PostFormValue("sitting")
	walking := r.PostFormValue("walking")

	if sitting == "" && walking != "" {
		sitting = "0"
	}

	if sitting != "" && walking == "" {
		walking = "0"
	}

	return &MeditationStartRequest{
		Sitting: sitting,
		Walking: walking,
	}
}

type MeditationPastRequest struct {
	MeditationStartRequest *MeditationStartRequest
	StartDate              string
	EndDate                string
	StartTime              string
}

func (r *MeditationPastRequest) Validate(ctx context.Context, user *model.User, userInRangeQuery UserInRangeQuery) validation.ValidationErrors {
	var errors validation.ValidationErrors = r.MeditationStartRequest.Validate()

	validation.Required(errors, "start_date", r.StartDate, "")
	validation.Required(errors, "end_date", r.EndDate, "")
	validation.Required(errors, "start_time", r.StartTime, "")

	validation.Date(errors, "start_date", r.StartDate, time.DateOnly, "")
	validation.Date(errors, "end_date", r.EndDate, time.DateOnly, "")
	validation.Date(errors, "start_time", r.StartTime, "15:04", "This value is not a valid time.")

	startDate, _ := time.Parse(time.DateOnly, r.StartDate)
	endDate, _ := time.Parse(time.DateOnly, r.EndDate)

	startDate = forceUserDateTimezone(startDate, user)
	endDate = forceUserDateTimezone(endDate, user)

	validation.DateBeforeOrSame(errors, "start_date", startDate, endDate, "The start date has to be before the end date.")
	validation.DateBeforeOrSame(errors, "start_date", endDate, time.Now(), "You cannot log future meditations.")

	// return early to skip errors while converting to model
	if len(errors) > 0 {
		return errors
	}

	meditations, err := r.ToModel(user)
	if err != nil {
		slog.ErrorContext(ctx, "Could not check for conflicting meditations.", "err", err)
		errors["start_date"] = append(errors["start_date"], "Could not check for conflicting meditations.")
		return errors
	}

	for _, meditation := range meditations {
		conflict, err := userInRangeQuery.UserHasMeditationInRange(ctx, user.GetId(), meditation.Start, meditation.End)
		if err != nil {
			slog.ErrorContext(ctx, "Could not check for conflicting meditations.", "err", err)
			errors["start_date"] = append(errors["start_date"], "Could not check for conflicting meditations.")
			return errors
		}

		if conflict {
			errors["start_date"] = append(errors["start_date"], "You have a conflicting meditation session in this time range.")
		}
	}

	return errors
}

func (r *MeditationPastRequest) ToModel(user *model.User) ([]*model.Meditation, error) {
	startDate, _ := time.Parse(time.DateOnly, r.StartDate)
	endDate, _ := time.Parse(time.DateOnly, r.EndDate)
	startTime, _ := time.Parse("15:04", r.StartTime)

	startDate = forceUserDateTimezone(startDate, user)
	endDate = forceUserDateTimezone(endDate, user)
	startTime = forceUserDateTimezone(startTime, user)

	diff := endDate.Sub(startDate)
	days := int(math.Ceil(diff.Hours() / 24))

	meditations := make([]*model.Meditation, 0)

	for i := 0; i <= days; i++ {
		walking, _ := strconv.Atoi(r.MeditationStartRequest.Walking)
		sitting, _ := strconv.Atoi(r.MeditationStartRequest.Sitting)
		currentStartDate := startDate.AddDate(0, 0, i)
		currentStartDate = setTimeOfDate(currentStartDate, startTime)
		currentEndDate := currentStartDate.Add(time.Duration(walking+sitting) * time.Minute)

		meditation := &model.Meditation{
			Walking:   int16(walking),
			Sitting:   int16(sitting),
			End:       currentEndDate,
			Start:     currentStartDate,
			Timezone:  currentStartDate.Location().String(),
			UserId:    user.Id,
			CreatedAt: time.Now(),
		}
		meditations = append(meditations, meditation)
	}

	return meditations, nil
}

func MeditationPastRequestFromForm(r *http.Request) *MeditationPastRequest {
	startRequest := MeditationStartRequestFromForm(r)
	return &MeditationPastRequest{
		MeditationStartRequest: startRequest,
		StartDate:              r.PostFormValue("start_date"),
		EndDate:                r.PostFormValue("end_date"),
		StartTime:              r.PostFormValue("start_time"),
	}
}

func getUserLocalizedNow(user *model.User) time.Time {
	location := getUserTimezone(user)
	return time.Now().In(location)
}

func getUserTimezone(user *model.User) *time.Location {
	timezone := "UTC"

	if user.Timezone.Valid {
		timezone = user.Timezone.String
	}

	location, err := time.LoadLocation(timezone)
	if err != nil {
		location, _ = time.LoadLocation("")
	}
	return location
}

func forceUserDateTimezone(t time.Time, user *model.User) time.Time {
	location := getUserTimezone(user)
	return time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond(), location)
}

func setTimeOfDate(date time.Time, setTime time.Time) time.Time {
	return time.Date(
		date.Year(),
		date.Month(),
		date.Day(),
		setTime.Hour(),
		setTime.Minute(),
		0,
		0,
		date.Location(),
	)
}

type MeditationUploadRequest struct {
	File   multipart.File
	Header *multipart.FileHeader
}

func (r *MeditationUploadRequest) Validate() validation.ValidationErrors {
	var errors validation.ValidationErrors = make(validation.ValidationErrors, 0)

	if r.Header == nil || (r.Header.Header.Get("Content-Type") != "text/csv" && r.Header.Header.Get("Content-Type") != "application/octet-stream") {
		errors["file"] = append(errors["file"], "Invalid CSV file.")
		return errors
	}

	if r.Header == nil || r.Header.Size > 5*1024*1024 {
		errors["file"] = append(errors["file"], "The maximum filesize is 5 MB.")
		return errors
	}

	records, err := readCsvFile(r.File)
	if err != nil {
		errors["file"] = append(errors["file"], "Could not read csv file.")
		return errors
	}

	if len(records) < 2 {
		errors["file"] = append(errors["file"], "No records found in this file.")
		return errors
	}

	if len(records[0]) != 4 {
		errors["file"] = append(errors["file"], "Invalid number of columns.")
		return errors
	}

	return errors
}

func (r *MeditationUploadRequest) ToModel(user *model.User) ([]*MeditationImportRequest, error) {
	_, _ = r.File.Seek(0, io.SeekStart)
	records, err := readCsvFile(r.File)

	if err != nil {
		return nil, err
	}

	now := time.Now()

	rows := make([]*MeditationImportRequest, 0)
	for index, row := range records {
		if index == 0 {
			continue
		}
		rows = append(rows, &MeditationImportRequest{
			CreatedAt: now,
			StartDate: row[0],
			EndDate:   row[1],
			MeditationStartRequest: &MeditationStartRequest{
				Walking: row[2],
				Sitting: row[3],
			},
		})
	}

	return rows, nil
}

func readCsvFile(file multipart.File) ([][]string, error) {
	defer file.Close()

	csvReader := csv.NewReader(file)
	records, err := csvReader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}

func MeditationUploadRequestFromForm(r *http.Request) *MeditationUploadRequest {
	maxSize := 5
	err := r.ParseMultipartForm(int64(maxSize) << 20)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not read form", "err", err)
		return nil
	}

	file, header, err := r.FormFile("file")
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not read file", "err", err)
		return nil
	}

	return &MeditationUploadRequest{
		File:   file,
		Header: header,
	}
}

type MeditationImportRequest struct {
	MeditationStartRequest *MeditationStartRequest
	StartDate              string
	EndDate                string
	CreatedAt              time.Time
}

func (r *MeditationImportRequest) Validate(ctx context.Context, index int, user *model.User, userInRangeQuery UserInRangeQuery) validation.ValidationErrors {
	var errors validation.ValidationErrors = r.MeditationStartRequest.Validate()

	validation.Required(errors, "file", r.StartDate, fmt.Sprintf("Row %d: Missing value for Start Date.", index))
	validation.Required(errors, "file", r.EndDate, fmt.Sprintf("Row %d: Missing value for End Date.", index))

	validation.Date(errors, "file", r.StartDate, time.RFC3339, fmt.Sprintf("Row %d: Invalid Start Date.", index))
	validation.Date(errors, "file", r.EndDate, time.RFC3339, fmt.Sprintf("Row %d: Invalid End Date.", index))

	startDate, _ := time.Parse(time.RFC3339, r.StartDate)
	endDate, _ := time.Parse(time.RFC3339, r.EndDate)

	validation.DateBeforeOrSame(errors, "file", startDate, endDate, fmt.Sprintf("Row %d: The start date has to be before the end date.", index))
	validation.DateBeforeOrSame(errors, "file", endDate, time.Now(), fmt.Sprintf("Row %d: You cannot log future meditations.", index))

	// return early to skip errors while converting to model
	if len(errors) > 0 {
		return errors
	}

	meditation, err := r.ToModel(user)
	if err != nil {
		slog.ErrorContext(ctx, "Could not check for conflicting meditations.", "err", err)
		errors["file"] = append(errors["file"], fmt.Sprintf("Row %d: Could not check for conflicting meditations.", index))
		return errors
	}

	conflict, err := userInRangeQuery.UserHasMeditationInRange(ctx, user.GetId(), meditation.Start, meditation.End)
	if err != nil {
		slog.ErrorContext(ctx, "Could not check for conflicting meditations.", "err", err)
		errors["file"] = append(errors["file"], fmt.Sprintf("Row %d: Could not check for conflicting meditations.", index))
		return errors
	}

	if conflict {
		errors["file"] = append(errors["file"], fmt.Sprintf("Row %d: You have a conflicting meditation session in this time range.", index))
	}

	return errors
}

func (r *MeditationImportRequest) ToModel(user *model.User) (*model.Meditation, error) {
	startDate, _ := time.Parse(time.RFC3339, r.StartDate)
	endDate, _ := time.Parse(time.RFC3339, r.EndDate)

	walking, _ := strconv.Atoi(r.MeditationStartRequest.Walking)
	sitting, _ := strconv.Atoi(r.MeditationStartRequest.Sitting)

	meditation := &model.Meditation{
		Walking:   int16(walking),
		Sitting:   int16(sitting),
		End:       endDate,
		Start:     startDate,
		Timezone:  startDate.Location().String(),
		UserId:    user.Id,
		Imported:  true,
		CreatedAt: r.CreatedAt,
	}

	return meditation, nil
}
