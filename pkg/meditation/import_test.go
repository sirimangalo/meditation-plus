package meditation

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log/slog"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/user"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestImport(t *testing.T) {
	userA := user.NewFakeUser(1)

	cases := map[string]struct {
		Method       string
		File         string
		SecurityUser security.SecurityUser
		Result       int
	}{
		"200 on GET": {
			Method:       "GET",
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"200 with errors on invalid data": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"303 on valid data": {
			Method:       "POST",
			File:         "../../etc/csv-upload/valid.csv",
			SecurityUser: userA,
			Result:       http.StatusSeeOther,
		},
		"200 with validation errors in csv": {
			Method:       "POST",
			File:         "../../etc/csv-upload/invalid.csv",
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"200 with invalid csv": {
			Method:       "POST",
			File:         "../../etc/csv-upload/nocsv.csv",
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		var request *http.Request
		var err error
		if testCase.File != "" {
			b, w := createMultipartFormData(t, "file", testCase.File)
			request, err = http.NewRequest(testCase.Method, "/", &b)
			if err != nil {
				t.Fatalf("Error creating request: %v", err)
			}
			request.Header.Set("Content-Type", w.FormDataContentType())
		} else {
			request, err = http.NewRequest(testCase.Method, "/", nil)
			if err != nil {
				t.Fatalf("Error creating request: %v", err)
			}
			request.Header.Set("Content-Type", "multipart/form-data")
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		meditationRepository := &FakeRepository{}
		templating := template.NewTemplatingWithFilter("test", session, "meditation")

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Import(w, r, meditationRepository, meditationRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func createMultipartFormData(t *testing.T, fieldName, fileName string) (bytes.Buffer, *multipart.Writer) {
	var b bytes.Buffer
	var err error
	w := multipart.NewWriter(&b)
	var fw io.Writer
	file := mustOpen(fileName)
	if fw, err = w.CreateFormFile(fieldName, file.Name()); err != nil {
		t.Errorf("Error creating writer: %v", err)
	}
	if _, err = io.Copy(fw, file); err != nil {
		t.Errorf("Error with io.Copy: %v", err)
	}
	w.Close()
	return b, w
}

func mustOpen(f string) *os.File {
	r, err := os.Open(f)
	if err != nil {
		pwd, _ := os.Getwd()
		fmt.Println("PWD: ", pwd)
		slog.Error("Could not open file", "file", pwd)
		panic(err)
	}
	return r
}
