package meditation

import (
	"context"
	"encoding/json"
	stdTemplate "html/template"
	"log/slog"
	"meditation-plus/pkg/banner"
	"meditation-plus/pkg/errors"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/validation"
	"net/http"
	"strconv"
	"time"
)

type Service struct {
}

func (s *Service) Index(
	w http.ResponseWriter,
	r *http.Request,
	findActiveQuery ActiveByUserIdQuery,
	findCurrentQuery CurrentQuery,
	findPastQuery PastQuery,
	getDailyStatsQuery GetDailyStatsQuery,
	templating *template.Templating,
) {
	if r.URL.Path != "/" {
		slog.WarnContext(r.Context(), "Route not found", "path", r.URL.Path)
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}

	user := security.GetUser(r).(*model.User)

	if !user.Welcomed {
		http.Redirect(w, r, "/welcome/", http.StatusSeeOther)
		return
	}

	activeSession, err := findActiveQuery.FindActiveByUserId(r.Context(), user.GetId())
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not query for active session", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	currentSessions, err := findCurrentQuery.FindCurrentMeditations(r.Context())
	if err != nil {
		slog.ErrorContext(r.Context(), "Could find current sessions", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	pastSessions, err := findPastQuery.FindPastMeditations(r.Context())
	if err != nil {
		slog.ErrorContext(r.Context(), "Could find current sessions", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	stats, err := getDailyStatsQuery.GetDailyStats(r.Context())
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not query daily stats", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}
	jsonStats, err := json.Marshal(stats)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could convert stats to json", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	templating.Render("meditation/index.html", map[string]interface{}{
		"activeSession":     activeSession,
		"currentSessions":   currentSessions,
		"pastSessions":      pastSessions,
		"stats":             stdTemplate.HTML(string(jsonStats)),
		"banner":            banner.GetBanner(),
		"circles":           make([]int, 8),
		"additionalCircles": len(currentSessions) - 8,
		security.CsrfTag:    security.CsrfField(r),
	}, w, r)
}

func (s *Service) Start(
	w http.ResponseWriter,
	r *http.Request,
	findActiveQuery ActiveByUserIdQuery,
	persistQuery PersistQuery,
	updateDailyStatsQuery UpdateDailyStatsQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	redirect := s.redirectOnActiveSession(w, r, findActiveQuery, templating)
	if redirect {
		return
	}

	user := security.GetUser(r).(*model.User)
	violations := make(validation.ValidationErrors)
	formRequest := &MeditationStartRequest{}

	if r.Method == http.MethodPost {
		formRequest = MeditationStartRequestFromForm(r)

		violations = formRequest.Validate()

		if len(violations) == 0 {
			err := s.start(r.Context(), formRequest, user, persistQuery, findActiveQuery, updateDailyStatsQuery)
			if err != nil {
				templating.RenderError(errors.GetErrorHttpCode(err), w, r)
				return
			}

			http.Redirect(w, r, "/", http.StatusSeeOther)

			return
		}
	}

	templating.Render("meditation/new.html", map[string]interface{}{
		"errors":         violations,
		"data":           formRequest,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) start(
	ctx context.Context,
	formRequest *MeditationStartRequest,
	user *model.User,
	persistQuery PersistQuery,
	findActiveQuery ActiveByUserIdQuery,
	updateDailyStatsQuery UpdateDailyStatsQuery,
) error {
	activeSession, err := findActiveQuery.FindActiveByUserId(ctx, int(user.Id))
	if err != nil {
		slog.ErrorContext(ctx, "Could not query active session", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	if activeSession != nil {
		slog.WarnContext(ctx, "Active meditation session already present.")
		return errors.NewServiceError(http.StatusBadRequest, nil)
	}

	meditation, err := formRequest.ToModel(user)
	if err != nil {
		slog.ErrorContext(ctx, "Could not convert request to model", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	err = persistQuery.Persist(ctx, meditation)

	if err != nil {
		slog.ErrorContext(ctx, "Could not save meditation", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	err = updateDailyStatsQuery.UpdateDailyStats(ctx, time.Now())

	if err != nil {
		slog.ErrorContext(ctx, "Could not update daily stats", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	return nil
}

func (s *Service) logPast(
	ctx context.Context,
	formRequest *MeditationPastRequest,
	user *model.User,
	persistQuery PersistQuery,
	findActiveQuery ActiveByUserIdQuery,
) error {
	meditations, err := formRequest.ToModel(user)
	if err != nil {
		slog.ErrorContext(ctx, "Could not convert request to model", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	for _, meditation := range meditations {
		err = persistQuery.Persist(ctx, meditation)

		if err != nil {
			slog.ErrorContext(ctx, "Could not save meditation", "error", err)
			return errors.NewServiceError(http.StatusInternalServerError, err)
		}
	}

	return nil
}

func (s *Service) redirectOnActiveSession(
	w http.ResponseWriter,
	r *http.Request,
	findActiveQuery ActiveByUserIdQuery,
	templating *template.Templating,
) bool {
	if r.Method == "POST" {
		return false
	}

	user := security.GetUser(r)

	activeSession, err := findActiveQuery.FindActiveByUserId(r.Context(), user.GetId())
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not query for active session", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return true
	}

	if activeSession != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return true
	}

	return false
}

func (s *Service) LogPast(
	w http.ResponseWriter,
	r *http.Request,
	findActiveQuery ActiveByUserIdQuery,
	persistQuery PersistQuery,
	userInRangeQuery UserInRangeQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	redirect := s.redirectOnActiveSession(w, r, findActiveQuery, templating)
	if redirect {
		return
	}

	user := security.GetUser(r).(*model.User)
	violations := make(validation.ValidationErrors)

	now := getUserLocalizedNow(user)
	formRequest := &MeditationPastRequest{
		StartDate:              now.Format(time.DateOnly),
		EndDate:                now.Format(time.DateOnly),
		StartTime:              now.Format("15:00"),
		MeditationStartRequest: &MeditationStartRequest{},
	}

	if r.Method == http.MethodPost {
		formRequest = MeditationPastRequestFromForm(r)

		violations = formRequest.Validate(r.Context(), user, userInRangeQuery)

		if len(violations) == 0 {
			err := s.logPast(r.Context(), formRequest, user, persistQuery, findActiveQuery)
			if err != nil {
				templating.RenderError(errors.GetErrorHttpCode(err), w, r)
				return
			}

			flashHandler.AddFlash(r, "notice", "The meditations have been created.")
			http.Redirect(w, r, "/", http.StatusSeeOther)

			return
		}
	}

	templating.Render("meditation/log.html", map[string]interface{}{
		"errors":         violations,
		"data":           formRequest,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) Stop(
	w http.ResponseWriter,
	r *http.Request,
	findActiveQuery ActiveByUserIdQuery,
	persistQuery PersistQuery,
	deleteQuery DeleteQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	user := security.GetUser(r)

	activeSession, err := findActiveQuery.FindActiveByUserId(r.Context(), user.GetId())
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not query for active session", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	if activeSession == nil {
		slog.ErrorContext(r.Context(), "Could not find an active session")
		templating.RenderError(http.StatusBadRequest, w, r)
		return
	}

	StopMeditation(activeSession)

	if activeSession.Walking+activeSession.Sitting < 1 {
		err = deleteQuery.Delete(r.Context(), activeSession)
	} else {
		err = persistQuery.Persist(r.Context(), activeSession)
	}

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not save meditation", "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
	}

	flashHandler.AddFlash(r, "notice", "The meditation has been stopped.")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (s *Service) List(
	w http.ResponseWriter,
	r *http.Request,
	paginateQuery PaginateQuery,
	countQuery CountQuery,
	templating *template.Templating,
) {
	query := r.URL.Query()
	user := security.GetUser(r)

	page, _ := strconv.Atoi(query.Get("page"))

	page = page - 1

	if page < 0 {
		page = 0
	}

	count, err := countQuery.Count(r.Context(), int64(user.GetId()))

	if err != nil {
		slog.ErrorContext(r.Context(), "Error while counting", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	meditations, err := paginateQuery.Paginate(r.Context(), int64(user.GetId()), page+1)

	if err != nil {
		slog.ErrorContext(r.Context(), "Error while paginting", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	pageCount := (count-1)/MEDITATIONS_PER_PAGE + 1
	pages := model.GetPages(pageCount, page+1)

	templating.Render("meditation/list.html", map[string]interface{}{
		"meditations":    meditations,
		"count":          count,
		"page":           page + 1,
		"pages":          pages,
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) Delete(
	w http.ResponseWriter,
	r *http.Request,
	findById IdQuery,
	deleteQuery DeleteQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	user := security.GetUser(r)
	id, _ := strconv.Atoi(r.PathValue("id"))

	meditation, err := findById.FindById(r.Context(), id)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not find meditation by id", "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}
	if meditation == nil || meditation.UserId != int64(user.GetId()) {
		slog.WarnContext(r.Context(), "Could not find meditation by id", "id", id, "userId", user.GetId())
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}

	err = deleteQuery.Delete(r.Context(), meditation)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not delete meditation", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	flashHandler.AddFlash(r, "notice", "The meditation has been deleted.")
	http.Redirect(w, r, "/meditations/", http.StatusSeeOther)
}
