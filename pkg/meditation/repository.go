package meditation

import (
	"context"
	"database/sql"
	"meditation-plus/pkg/database"
	"meditation-plus/pkg/model"
	"time"
)

const MEDITATIONS_PER_PAGE = 15

type Repository struct{}

type IdQuery interface {
	FindById(ctx context.Context, id int) (*model.Meditation, error)
}
type ActiveByUserIdQuery interface {
	FindActiveByUserId(ctx context.Context, userId int) (*model.Meditation, error)
}
type PersistQuery interface {
	Persist(ctx context.Context, a *model.Meditation) error
}
type DeleteQuery interface {
	Delete(ctx context.Context, a *model.Meditation) error
}
type UserInRangeQuery interface {
	UserHasMeditationInRange(ctx context.Context, userId int, start time.Time, end time.Time) (bool, error)
}
type PastQuery interface {
	FindPastMeditations(ctx context.Context) ([]model.FindPastMeditationsRow, error)
}
type CurrentQuery interface {
	FindCurrentMeditations(ctx context.Context) ([]model.FindCurrentMeditationsRow, error)
}
type UpdateDailyStatsQuery interface {
	UpdateDailyStats(ctx context.Context, day time.Time) error
}
type GetDailyStatsQuery interface {
	GetDailyStats(ctx context.Context) ([]model.MeditationStatsDaily, error)
}
type ListMigratedByOldUserUidQuery interface {
	ListMigratedByOldUserUid(ctx context.Context, userId string) ([]model.MigratedMeditation, error)
}
type ListMigratedByOldUserUidNotMatchedQuery interface {
	ListMigratedByOldUserUidNotMatched(ctx context.Context, userId string) ([]model.MigratedMeditation, error)
}
type ConvertMeditationsQuery interface {
	ConvertMeditations(ctx context.Context, userId int64, oldUserUid string) error
}
type DeleteMeditationByUserBeforeDateQuery interface {
	DeleteMeditationByUserBeforeDate(ctx context.Context, userId int64, date time.Time) error
}
type PaginateQuery interface {
	Paginate(ctx context.Context, userId int64, page int) ([]model.Meditation, error)
}
type CountQuery interface {
	Count(ctx context.Context, userId int64) (int, error)
}

func NewRepository() *Repository {
	return &Repository{}
}

func (r *Repository) Persist(ctx context.Context, m *model.Meditation) error {
	queries := model.New(database.Pool)

	if m.Id != 0 {
		_, err := queries.UpdateMeditation(ctx, model.UpdateMeditationParams{
			Walking:   m.Walking,
			Sitting:   m.Sitting,
			End:       m.End,
			Start:     m.Start,
			Timezone:  m.Timezone,
			UserId:    m.UserId,
			CreatedAt: m.CreatedAt,
			Id:        m.Id,
		})

		if err != nil {
			return err
		}
	} else {
		result, err := queries.CreateMeditation(ctx, model.CreateMeditationParams{
			Walking:   m.Walking,
			Sitting:   m.Sitting,
			End:       m.End,
			Start:     m.Start,
			Timezone:  m.Timezone,
			UserId:    m.UserId,
			CreatedAt: m.CreatedAt,
		})

		if err != nil {
			return err
		}

		lastId, err := result.LastInsertId()

		if err != nil {
			return err
		}

		m.Id = lastId
	}

	return nil
}

func (r *Repository) FindById(ctx context.Context, id int) (*model.Meditation, error) {
	queries := model.New(database.Pool)
	meditation, err := queries.FindMeditationById(ctx, int64(id))

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &meditation, nil
}

func (r *Repository) FindActiveByUserId(ctx context.Context, userId int) (*model.Meditation, error) {
	queries := model.New(database.Pool)
	meditation, err := queries.FindActiveMeditationByUserId(ctx, model.FindActiveMeditationByUserIdParams{
		UserId: int64(userId),
		End:    time.Now(),
	})

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &meditation, nil
}

func (r *Repository) FindCurrentMeditations(ctx context.Context) ([]model.FindCurrentMeditationsRow, error) {
	queries := model.New(database.Pool)
	return queries.FindCurrentMeditations(ctx, model.FindCurrentMeditationsParams{
		Now: time.Now(),
	})
}

func (r *Repository) FindPastMeditations(ctx context.Context) ([]model.FindPastMeditationsRow, error) {
	queries := model.New(database.Pool)
	threeHoursAgo := time.Now().Add(time.Duration(-3) * time.Hour)
	return queries.FindPastMeditations(ctx, model.FindPastMeditationsParams{
		End:   time.Now(),
		End_2: threeHoursAgo,
	})
}

func (r *Repository) UserHasMeditationInRange(ctx context.Context, userId int, start time.Time, end time.Time) (bool, error) {
	queries := model.New(database.Pool)
	count, err := queries.UserHasMeditationInRange(ctx, model.UserHasMeditationInRangeParams{
		UserId: int64(userId),
		Start:  start,
		End:    end,
	})

	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (r *Repository) Delete(ctx context.Context, a *model.Meditation) error {
	queries := model.New(database.Pool)
	return queries.DeleteMeditation(ctx, a.Id)
}

func (r *Repository) UpdateDailyStats(ctx context.Context, day time.Time) error {
	queries := model.New(database.Pool)

	now := time.Now()

	result, err := queries.GetDailyMeditationStats(ctx, model.GetDailyMeditationStatsParams{
		FromDay: now,
		ToDay:   now,
	})

	if err != nil {
		return err
	}

	if len(result) == 0 {
		err = queries.CreateDailyMeditationStats(ctx, now)

		if err != nil {
			return err
		}
	}

	return queries.UpdateDailyMeditationStats(ctx, model.UpdateDailyMeditationStatsParams{
		Day: day,
	})
}

func (r *Repository) GetDailyStats(ctx context.Context) ([]model.MeditationStatsDaily, error) {
	queries := model.New(database.Pool)
	now := time.Now()
	lastMonth := now.AddDate(0, -1, 0)

	return queries.GetDailyMeditationStats(ctx, model.GetDailyMeditationStatsParams{
		FromDay: lastMonth,
		ToDay:   now,
	})
}

func (r *Repository) ListMigratedByOldUserUid(ctx context.Context, userId string) ([]model.MigratedMeditation, error) {
	queries := model.New(database.Pool)
	return queries.ListMigratedMeditationsByOldUserUid(ctx, userId)
}

func (r *Repository) ListMigratedByOldUserUidNotMatched(ctx context.Context, userId string) ([]model.MigratedMeditation, error) {
	queries := model.New(database.Pool)
	return queries.ListMigratedMeditationsByOldUserUidNotMatched(ctx, userId)
}

func (r *Repository) ConvertMeditations(ctx context.Context, userId int64, oldUserUid string) error {
	queries := model.New(database.Pool)
	return queries.ConvertMeditations(ctx, model.ConvertMeditationsParams{
		OldUserUid: oldUserUid,
		NewUserID:  userId,
	})
}

func (r *Repository) DeleteMeditationByUserBeforeDate(ctx context.Context, userId int64, date time.Time) error {
	queries := model.New(database.Pool)
	return queries.DeleteMeditationByUserBeforeDate(ctx, model.DeleteMeditationByUserBeforeDateParams{
		UserId: userId,
		Start:  date,
	})
}

func (r *Repository) Count(ctx context.Context, userId int64) (int, error) {
	queries := model.New(database.Pool)

	result, err := queries.CountUserMeditations(ctx, userId)

	if err != nil {
		return 0, err
	}

	return int(result), nil
}

func (r *Repository) Paginate(ctx context.Context, userId int64, page int) ([]model.Meditation, error) {
	queries := model.New(database.Pool)

	perPage := MEDITATIONS_PER_PAGE

	meditations, err := queries.PaginateUserMeditations(ctx, model.PaginateUserMeditationsParams{
		Limit:  int32(perPage),
		Offset: int32((page - 1) * perPage),
		UserId: userId,
	})

	if err != nil {
		return nil, err
	}

	return meditations, nil
}
