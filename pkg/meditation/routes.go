package meditation

import (
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/user"
	"net/http"
)

type Routes struct {
	repository         *Repository
	userRepository     *user.Repository
	flashHandler       security.FlashHandler
	templating         *template.Templating
	service            *Service
	sessionValueGetter security.SessionValueGetter
}

func NewRoutes(
	repository *Repository,
	userRepository *user.Repository,
	flashHandler security.FlashHandler,
	templating *template.Templating,
	service *Service,
	sessionValueGetter security.SessionValueGetter,
) *Routes {
	return &Routes{
		repository:         repository,
		userRepository:     userRepository,
		flashHandler:       flashHandler,
		templating:         templating,
		service:            service,
		sessionValueGetter: sessionValueGetter,
	}
}

func (mr *Routes) Start(w http.ResponseWriter, r *http.Request) {
	mr.service.Start(w, r, mr.repository, mr.repository, mr.repository, mr.templating, mr.flashHandler)
}
func (mr *Routes) LogPast(w http.ResponseWriter, r *http.Request) {
	mr.service.LogPast(w, r, mr.repository, mr.repository, mr.repository, mr.templating, mr.flashHandler)
}
func (mr *Routes) Import(w http.ResponseWriter, r *http.Request) {
	mr.service.Import(w, r, mr.repository, mr.repository, mr.templating, mr.flashHandler)
}
func (mr *Routes) Convert(w http.ResponseWriter, r *http.Request) {
	mr.service.Convert(w, r, mr.repository, mr.repository, mr.repository, mr.templating, mr.flashHandler)
}
func (mr *Routes) Login(w http.ResponseWriter, r *http.Request) {
	mr.service.Login(w, r, mr.userRepository, mr.templating, mr.flashHandler)
}
func (mr *Routes) Stop(w http.ResponseWriter, r *http.Request) {
	mr.service.Stop(w, r, mr.repository, mr.repository, mr.repository, mr.templating, mr.flashHandler)
}
func (mr *Routes) Index(w http.ResponseWriter, r *http.Request) {
	mr.service.Index(w, r, mr.repository, mr.repository, mr.repository, mr.repository, mr.templating)
}
func (mr *Routes) List(w http.ResponseWriter, r *http.Request) {
	mr.service.List(w, r, mr.repository, mr.repository, mr.templating)
}
func (mr *Routes) Delete(w http.ResponseWriter, r *http.Request) {
	mr.service.Delete(w, r, mr.repository, mr.repository, mr.templating, mr.flashHandler)
}
