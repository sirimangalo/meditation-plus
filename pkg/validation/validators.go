package validation

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type ValidationErrors map[string][]string

func Required(errors ValidationErrors, field string, value string, msg string) {
	if msg == "" {
		msg = "This field is required."
	}

	if strings.TrimSpace(value) == "" {
		errors[field] = append(errors[field], msg)
	}
}

func Equal(errors ValidationErrors, field string, valueA string, valueB string, msg string) {
	if msg == "" {
		msg = "The values have to equal."
	}
	if valueA != valueB {
		errors[field] = append(errors[field], msg)
	}
}

func Min(errors ValidationErrors, field string, min int, value int, msg string) {
	if msg == "" {
		msg = fmt.Sprintf("The value should have a minimum value of %d.", min)
	}
	if value < min {
		errors[field] = append(errors[field], msg)
	}
}

func Max(errors ValidationErrors, field string, max int, value int, msg string) {
	if msg == "" {
		msg = fmt.Sprintf("The value should have a maximum value of %d.", max)
	}
	if value > max {
		errors[field] = append(errors[field], msg)
	}
}

func Length(errors ValidationErrors, field string, value string, min int, max int, msg string) {
	if msg == "" {
		if min > 0 && max > 0 {
			msg = fmt.Sprintf("The value should be between %d and %d characters long.", min, max)
		} else if min > 0 {
			msg = fmt.Sprintf("The value should be at least %d characters long.", min)
		} else if max > 0 {
			msg = fmt.Sprintf("The value should be under %d characters long.", max)
		}
	}

	if min > 0 && max > 0 && (len(value) < min || len(value) > max) {
		errors[field] = append(errors[field], msg)
	} else if min > 0 && len(value) < min {
		errors[field] = append(errors[field], msg)
	} else if max > 0 && len(value) > max {
		errors[field] = append(errors[field], msg)
	}
}

func StringOneOf(errors ValidationErrors, field string, value string, options []string, msg string) {
	if msg == "" {
		msg = fmt.Sprintf("The value should be one of %v", options)
	}

	for _, option := range options {
		if value == option {
			return
		}
	}

	errors[field] = append(errors[field], msg)
}

func Int(errors ValidationErrors, field string, value string, msg string) {
	if msg == "" {
		msg = "This value should be numeric."
	}

	_, err := strconv.Atoi(value)

	if err != nil {
		errors[field] = append(errors[field], msg)
	}
}

func Date(errors ValidationErrors, field string, value string, format string, msg string) {
	if msg == "" {
		msg = "This value is not a valid date."
	}

	_, err := time.Parse(format, value)

	if err != nil {
		errors[field] = append(errors[field], msg)
	}
}

func DateAfterOrSame(errors ValidationErrors, field string, value time.Time, after time.Time, msg string) {
	if msg == "" {
		msg = fmt.Sprintf("This date should be after %s.", after.Format(time.DateOnly))
	}

	if value.Before(after) {
		errors[field] = append(errors[field], msg)
	}
}

func DateBeforeOrSame(errors ValidationErrors, field string, value time.Time, before time.Time, msg string) {
	if msg == "" {
		msg = fmt.Sprintf("This date should be before %s.", before.Format(time.DateOnly))
	}

	if value.After(before) {
		errors[field] = append(errors[field], msg)
	}
}

func Timezone(errors ValidationErrors, field string, value string, msg string) {
	if msg == "" {
		msg = "This is an invalid timezone."
	}

	zone, err := time.LoadLocation(value)

	if zone == nil || err != nil {
		errors[field] = append(errors[field], msg)
	}
}

func Email(errors ValidationErrors, field string, value string, msg string) {
	if msg == "" {
		msg = "This is an invalid email address."
	}

	// https://github.com/asaskevich/govalidator/blob/a9d515a09cc289c60d55064edec5ef189859f172/patterns.go#L7C29-L7C1239
	regex := "^(((([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+(\\.([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|\\.|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.)+(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.?$"
	match, _ := regexp.MatchString(regex, value)

	if !match {
		errors[field] = append(errors[field], msg)
	}
}
