package validation

import (
	"testing"
	"time"
)

func TestLengthShouldIgnoreAll0(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Length(errors, "field", "value", 0, 0, "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestLengthShouldErrorOnTooLong(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Length(errors, "field", "value", 0, 3, "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestLengthShouldErrorOnTooShort(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Length(errors, "field", "value", 6, 0, "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestLengthShouldPassOnMatch(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Length(errors, "field", "value", 3, 10, "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestEqualPassing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Equal(errors, "field", "value", "value", "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestEqualFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Equal(errors, "field", "value", "value2", "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestRequiredPassing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Required(errors, "field", "value", "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestRequiredFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Required(errors, "field", " ", "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestStringOneOfFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	StringOneOf(errors, "field", "apple", []string{"orange", "banana"}, "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestStringOneOf(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	StringOneOf(errors, "field", "apple", []string{"apple", "banana"}, "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestTimezoneFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Timezone(errors, "field", "NOTEXISTING", "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestTimezone(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Timezone(errors, "field", "Europe/Berlin", "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestEmailFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Email(errors, "field", "wrong", "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestEmail(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Email(errors, "field", "works@überprüfung.com", "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestIntFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Int(errors, "field", "nonumer", "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestInt(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Int(errors, "field", "4711", "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestDateFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Date(errors, "field", "12.12.2020", time.DateOnly, "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestDate(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	Date(errors, "field", "2020-12-12", time.DateOnly, "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestDateAfterFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	value, _ := time.Parse(time.DateOnly, "2020-01-01")
	after, _ := time.Parse(time.DateOnly, "2020-01-02")
	DateAfterOrSame(errors, "field", value, after, "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestDateAfter(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	value, _ := time.Parse(time.DateOnly, "2020-01-02")
	after, _ := time.Parse(time.DateOnly, "2020-01-01")
	DateAfterOrSame(errors, "field", value, after, "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}

func TestDateBeforeFailing(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	value, _ := time.Parse(time.DateOnly, "2020-01-02")
	before, _ := time.Parse(time.DateOnly, "2020-01-01")
	DateBeforeOrSame(errors, "field", value, before, "")
	if len(errors) != 1 {
		t.Errorf("errors should contain 1 error %s", errors)
	}
}

func TestDateBefore(t *testing.T) {
	var errors ValidationErrors = make(ValidationErrors)
	value, _ := time.Parse(time.DateOnly, "2019-12-31")
	before, _ := time.Parse(time.DateOnly, "2020-01-01")
	DateBeforeOrSame(errors, "field", value, before, "")
	if len(errors) > 0 {
		t.Errorf("errors should be empty %s", errors)
	}
}
