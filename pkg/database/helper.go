package database

import (
	"log"
	"meditation-plus/db"
	"meditation-plus/etc"
	"os"
	"strings"
)

func SetUpRepositoryTest() {
	driver := os.Getenv("DATABASE_DRIVER")
	dsn := os.Getenv("DATABASE_DSN")
	database := os.Getenv("TEST_DATABASE")

	InitDb(driver, dsn)

	_, err := Pool.Exec("USE " + database)
	if err != nil {
		log.Fatalf("Could not select test database: %v", err)
	}
}

func SetUpTestDb(dbDriver string, dbDsn string) {
	InitDb(dbDriver, dbDsn)
	createTestDb()
	LoadFixtures()
}

func TearDownDbTest() {
	database := os.Getenv("TEST_DATABASE")

	_, err := Pool.Exec("DROP DATABASE IF EXISTS " + database)
	if err != nil {
		log.Fatalf("Could not drop test database: %v", err)
	}
}

func createTestDb() {
	database := os.Getenv("TEST_DATABASE")

	if database == "" {
		log.Fatal("Test database name mus be specified")
		return
	}

	TearDownDbTest()

	_, err := Pool.Exec("CREATE DATABASE " + database)
	if err != nil {
		log.Fatalf("Could not create test database: %v", err)
	}

	_, err = Pool.Exec("USE " + database)
	if err != nil {
		log.Fatalf("Could not select test database: %v", err)
	}

	loadSchema()
}

func loadSchema() {
	schema := db.Schema

	queries := strings.Split(string(schema), ";")
	for _, query := range queries {
		query = strings.TrimSpace(query)
		if query == "" {
			continue
		}

		_, err := Pool.Exec(query)
		if err != nil {
			log.Fatalf("Could not execute query: %v", err)
		}
	}
}

func LoadFixtures() {
	schema := etc.Fixtures

	queries := strings.Split(string(schema), ";")
	for _, query := range queries {
		query = strings.TrimSpace(query)
		if query == "" {
			continue
		}

		_, err := Pool.Exec(query)
		if err != nil {
			log.Fatalf("Could not execute query: %v", err)
		}
	}
}
