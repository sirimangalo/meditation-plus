package database

import (
	"database/sql"
	"log/slog"

	_ "github.com/go-sql-driver/mysql"
)

var Pool *sql.DB

func InitDb(driver string, dsn string) {
	var err error

	Pool, err = sql.Open(driver, dsn)
	if err != nil {
		slog.Error("Could not establish database connection", "driver", driver, "error", err)
		panic(err)
	}
}

type Scannable interface {
	Scan(dest ...any) error
}
