package logger

import (
	"context"
	"log/slog"
	"meditation-plus/pkg/routing"
)

type ContextHandler struct {
	slog.Handler
}

func (h ContextHandler) Handle(ctx context.Context, r slog.Record) error {
	if request, ok := ctx.Value(routing.RequestContextKey).(string); ok {
		r.AddAttrs(slog.String(string(routing.RequestContextKey), request))
	}

	return h.Handler.Handle(ctx, r)
}
