package errors

import "errors"

var ErrUserNotInGuild = errors.New("user is not in guild")
