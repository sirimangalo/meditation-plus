package errors

import (
	"errors"
	"fmt"
	"net/http"
)

type ServiceError struct {
	StatusCode   int
	WrappedError error
}

func (e *ServiceError) Error() string {
	if e.WrappedError != nil {
		return e.WrappedError.Error()
	}
	return fmt.Sprint(e.StatusCode)
}

func NewServiceError(statusCode int, wrappedError error) *ServiceError {
	return &ServiceError{
		StatusCode:   statusCode,
		WrappedError: wrappedError,
	}
}

func GetErrorHttpCode(errorToCheck error) int {
	var serviceError *ServiceError
	if errors.As(errorToCheck, &serviceError) {
		return serviceError.StatusCode
	}

	return http.StatusInternalServerError
}
