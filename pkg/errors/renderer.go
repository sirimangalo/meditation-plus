package errors

import "net/http"

type ErrorRenderer interface {
	RenderError(errorCode int, w http.ResponseWriter, r *http.Request)
}
