package timezone

import (
	"bufio"
	"log/slog"
	"os"
	"sort"
	"strings"
)

type Service struct {
	ZoneInfoPath string
}

type Lister interface {
	List() []string
}

func (t *Service) List() []string {
	timezones := make([]string, 0)
	file, err := os.Open(t.ZoneInfoPath + "/zone1970.tab")
	if err != nil {
		slog.Error("Could not read timezones.", "err", err)
		return timezones
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") || line == "" {
			continue
		}
		fields := strings.Split(line, "\t")
		if len(fields) >= 3 {
			timezones = append(timezones, fields[2])
		}
	}
	if err := scanner.Err(); err != nil {
		slog.Error("Could not read timezones.", "err", err)
		return timezones
	}

	sort.Strings(timezones)

	return timezones
}
