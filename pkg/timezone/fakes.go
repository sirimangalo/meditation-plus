package timezone

type FakeService struct {
}

func (q *FakeService) List() []string {
	return []string{"UTC"}
}
