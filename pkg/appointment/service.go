package appointment

import (
	"log/slog"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/user"
	"net/http"
	"strconv"
)

type Service struct {
}

func (s *Service) List(
	w http.ResponseWriter,
	r *http.Request,
	findByTeacher TeacherQuery,
	findAllQuery AllQuery,
	findUserByIdQuery user.IdQuery,
	findByUserIdQuery UserIdQuery,
	findAllTeacherQuery user.TeacherQuery,
	templating *template.Templating,
) {
	appointments, err := findAllQuery.FindAll(r.Context())
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not list appointments", "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	teachers, err := findAllTeacherQuery.FindTeacher(r.Context())

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not list teachers", "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	user := security.GetUser(r).(*model.User)
	userAppointment, _ := findByUserIdQuery.FindByUserId(r.Context(), user.GetId())
	userTimezone := "UTC"

	if user.Timezone.String != "" {
		userTimezone = user.Timezone.String
	}

	now := getNowOrSimulatedDateFromQuery(r)

	templating.Render("appointment/index.html", map[string]interface{}{
		"now":             now,
		"appointments":    appointments,
		"teachers":        teachers,
		"userAppointment": userAppointment,
		"userTimezone":    userTimezone,
		"minHoursBefore":  MIN_HOURS_BEFORE_APPOINTMENT,
		security.CsrfTag:  security.CsrfField(r),
	}, w, r)
}

func (s *Service) Book(
	w http.ResponseWriter,
	r *http.Request,
	findByIdQuery IdQuery,
	userHasAppointmentQuery UserIdQuery,
	bookQuery BookQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	id, _ := strconv.Atoi(r.PathValue("id"))
	appointment, err := findByIdQuery.FindById(r.Context(), id)
	userId := security.GetUser(r).GetId()

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not find appointment by id", "id", id, "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	if appointment == nil {
		slog.WarnContext(r.Context(), "Could not find appointment by id")
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}

	if appointment.UserId.Valid {
		slog.WarnContext(r.Context(), "Appointment already booked", "id", appointment.Id)
		templating.RenderError(http.StatusBadRequest, w, r)
		return
	}

	userAppointment, err := userHasAppointmentQuery.FindByUserId(r.Context(), userId)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not check for other appointment", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	if userAppointment != nil {
		slog.WarnContext(r.Context(), "User already booked another appointment", "id", appointment.Id)
		templating.RenderError(http.StatusBadRequest, w, r)
		return
	}

	err = bookQuery.Book(r.Context(), appointment, userId)

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not book appointment", "id", appointment.Id, "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	flashHandler.AddFlash(r, "notice", "You have successfully booked this appointment.")

	http.Redirect(w, r, "/appointments/", http.StatusSeeOther)
}

func (s *Service) Cancel(
	w http.ResponseWriter,
	r *http.Request,
	findByIdQuery IdQuery,
	bookQuery BookQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	id, _ := strconv.Atoi(r.PathValue("id"))
	appointment, err := findByIdQuery.FindById(r.Context(), id)

	curUser := security.GetUser(r)
	userId := curUser.GetId()

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not find appointment by id", "id", id, "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	if appointment == nil {
		slog.WarnContext(r.Context(), "Could not find appointment by id")
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}

	if !appointment.UserId.Valid {
		slog.WarnContext(r.Context(), "Appointment is not booked by any user", "appointmentUserId", appointment.UserId.Int64, "userId", userId)
		templating.RenderError(http.StatusBadRequest, w, r)
		return
	}

	isAuthorizedToRemoveOthers := curUser.IsGranted(model.USER_ROLES_TEACHER) && appointment.TeacherId == int64(userId)

	if !isAuthorizedToRemoveOthers && appointment.UserId.Int64 != int64(userId) {
		slog.WarnContext(r.Context(), "Unauthorized to remove user from appointment", "appointmentUserId", appointment.UserId.Int64, "userId", userId)
		templating.RenderError(http.StatusForbidden, w, r)
		return
	}

	doNotNotify := userId == int(appointment.TeacherId)
	err = bookQuery.Unbook(r.Context(), appointment, appointment.UserId.Int64, doNotNotify)

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not unbook appointment", "id", appointment.Id, "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	flashHandler.AddFlash(r, "notice", "You have successfully cancelled this appointment.")

	http.Redirect(w, r, "/appointments/", http.StatusSeeOther)
}
