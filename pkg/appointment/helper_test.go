package appointment

import (
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/test"
	"testing"
	"time"
)

type TestData struct {
	appointment *model.Appointment
	now         string
	expected    string
}

func TestGetNextAppointmentTime(t *testing.T) {
	testData := []TestData{
		{
			appointment: &model.Appointment{Timezone: "invalid", Time: "12:00:00", Weekday: 4},
			now:         "2024-05-02T09:00:00-04:00",
			expected:    "0001-01-01T00:00:00Z",
		},
		{
			appointment: &model.Appointment{Timezone: "America/Toronto", Time: "12:00:00", Weekday: 4},
			now:         "2024-05-02T09:00:00-04:00",
			expected:    "2024-05-02T12:00:00-04:00",
		},
		{
			appointment: &model.Appointment{Timezone: "America/Toronto", Time: "12:00:00", Weekday: 4},
			now:         "2024-04-30T12:00:01-04:00",
			expected:    "2024-05-02T12:00:00-04:00",
		},
		{
			appointment: &model.Appointment{Timezone: "America/Toronto", Time: "12:00:00", Weekday: 4},
			now:         "2024-05-02T12:00:01-04:00",
			expected:    "2024-05-09T12:00:00-04:00",
		},
		{
			appointment: &model.Appointment{Timezone: "America/Toronto", Time: "12:00:00", Weekday: 4},
			now:         "2024-05-04T12:00:00-04:00",
			expected:    "2024-05-09T12:00:00-04:00",
		},
		{
			appointment: &model.Appointment{Timezone: "America/Toronto", Time: "12:00:00", Weekday: 4},
			now:         "2024-05-05T12:00:00-04:00",
			expected:    "2024-05-09T12:00:00-04:00",
		},
		{
			appointment: &model.Appointment{Timezone: "America/Toronto", Time: "23:00:00", Weekday: 4},
			now:         "2024-11-01T23:00:00-04:00",
			expected:    "2024-11-07T23:00:00-05:00",
		},
	}

	for _, d := range testData {
		now, _ := time.Parse(time.RFC3339, d.now)
		nextTime, _ := GetNextAppointmentTime(*d.appointment, now)
		test.AssertEquals(t, nextTime.Format(time.RFC3339), d.expected)
	}
}
