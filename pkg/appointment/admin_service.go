package appointment

import (
	"context"
	"fmt"
	"log/slog"
	"meditation-plus/pkg/errors"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/user"
	"meditation-plus/pkg/validation"
	"net/http"
	"strconv"
)

func (s *Service) ListByTeacher(
	w http.ResponseWriter,
	r *http.Request,
	findUserById user.IdQuery,
	findByTeacher TeacherQuery,
	templating *template.Templating,
) {
	teacherId, _ := strconv.Atoi(r.PathValue("teacherId"))

	teacher, err := findUserById.FindById(r.Context(), teacherId)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not find teacher", "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}
	if teacher == nil {
		slog.WarnContext(r.Context(), "Could not find teacher", "teacherId", teacherId)
		templating.RenderError(http.StatusNotFound, w, r)
		return
	}

	appointments, err := findByTeacher.FindByTeacher(r.Context(), teacherId)

	if err != nil {
		slog.WarnContext(r.Context(), "Could not find appointments by teacher", "error", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	templating.Render("admin/appointment/list.html", map[string]interface{}{
		"appointments": appointments,
		"teacher":      teacher,
	}, w, r)
}

func (s *Service) New(
	w http.ResponseWriter,
	r *http.Request,
	findUserById user.IdQuery,
	persistQuery PersistQuery,
	findSlotQuery FindSlotQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	teacherId, _ := strconv.Atoi(r.PathValue("teacherId"))
	teacher, err := s.getTeacher(r, teacherId, findUserById)
	if err != nil {
		templating.RenderError(errors.GetErrorHttpCode(err), w, r)
		return
	}

	violations := make(validation.ValidationErrors)
	formRequest := &AppointmentRequest{}

	if r.Method == http.MethodPost {
		formRequest = AppointmentRequestFromForm(r)

		violations = formRequest.Validate(r.Context(), teacherId, findSlotQuery, 0)

		if len(violations) == 0 {
			err := s.create(r.Context(), formRequest, teacher, persistQuery)
			if err != nil {
				templating.RenderError(errors.GetErrorHttpCode(err), w, r)
				return
			}

			flashHandler.AddFlash(r, "notice", "The appointment has been created.")
			http.Redirect(w, r, fmt.Sprintf("/admin/appointments/%d/", teacherId), http.StatusSeeOther)

			return
		}
	}

	templating.Render("admin/appointment/new.html", map[string]interface{}{
		"teacher":        teacher,
		"errors":         violations,
		"data":           formRequest,
		"weekdays":       s.getWeekdays(),
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) getTeacher(r *http.Request, teacherId int, findUserById user.IdQuery) (*model.User, error) {
	curUser := security.GetUser(r)
	teacher, err := findUserById.FindById(r.Context(), teacherId)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not find user by id", "error", err)
		return nil, errors.NewServiceError(http.StatusInternalServerError, err)
	}
	if teacher == nil {
		slog.WarnContext(r.Context(), "Could not find user by id", "userId", teacherId)
		return nil, errors.NewServiceError(http.StatusNotFound, nil)
	}

	if curUser.GetId() != teacherId && !curUser.IsGranted(model.USER_ROLES_ADMIN) {
		slog.WarnContext(r.Context(), "Access denied while creating appointment", "curUser", curUser.GetId(), "teacherId", teacherId)
		return nil, errors.NewServiceError(http.StatusForbidden, nil)
	}

	return teacher, nil
}

func (s *Service) create(ctx context.Context, formRequest *AppointmentRequest, teacher *model.User, persistQuery PersistQuery) error {
	appointment, err := formRequest.ToModel(teacher)
	if err != nil {
		slog.ErrorContext(ctx, "Could not convert request to model", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	err = persistQuery.Persist(ctx, appointment)

	if err != nil {
		slog.ErrorContext(ctx, "Could not save appointment", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	return nil
}

func (s *Service) Edit(
	w http.ResponseWriter,
	r *http.Request,
	findById IdQuery,
	persistQuery PersistQuery,
	findSlotQuery FindSlotQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	appointmentId, _ := strconv.Atoi(r.PathValue("appointmentId"))

	appointment, err := s.getAppointmentToEdit(r, appointmentId, findById)
	if err != nil {
		templating.RenderError(errors.GetErrorHttpCode(err), w, r)
		return
	}

	violations := make(validation.ValidationErrors)
	formRequest := AppointmentRequestFromModel(appointment)

	if r.Method == http.MethodPost {
		formRequest = AppointmentRequestFromForm(r)

		violations = formRequest.Validate(r.Context(), int(appointment.TeacherId), findSlotQuery, appointment.Id)

		if len(violations) == 0 {
			err := s.update(r.Context(), formRequest, appointment, persistQuery)
			if err != nil {
				templating.RenderError(errors.GetErrorHttpCode(err), w, r)
				return
			}

			flashHandler.AddFlash(r, "notice", "The appointment has been updated.")
			http.Redirect(w, r, fmt.Sprintf("/admin/appointments/%d/", appointment.TeacherId), http.StatusSeeOther)
			return
		}
	}

	templating.Render("admin/appointment/edit.html", map[string]interface{}{
		"appointment":    appointment,
		"errors":         violations,
		"data":           formRequest,
		"weekdays":       s.getWeekdays(),
		security.CsrfTag: security.CsrfField(r),
	}, w, r)
}

func (s *Service) getAppointmentToEdit(r *http.Request, appointmentId int, findById IdQuery) (*model.Appointment, error) {
	curUser := security.GetUser(r)
	appointment, err := findById.FindById(r.Context(), appointmentId)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not find appointment by id", "error", err)
		return nil, errors.NewServiceError(http.StatusInternalServerError, err)
	}
	if appointment == nil {
		slog.WarnContext(r.Context(), "Could not find appointment by id", "appointmentId", appointmentId)
		return nil, errors.NewServiceError(http.StatusNotFound, nil)
	}

	if curUser.GetId() != int(appointment.TeacherId) && !curUser.IsGranted(model.USER_ROLES_ADMIN) {
		slog.WarnContext(r.Context(), "Access denied while editing appointment", "curUser", curUser.GetId(), "teacherId", appointment.TeacherId)
		return nil, errors.NewServiceError(http.StatusForbidden, nil)
	}

	return appointment, nil
}

func (s *Service) update(ctx context.Context, formRequest *AppointmentRequest, appointment *model.Appointment, persistQuery PersistQuery) error {
	err := formRequest.UpdateModel(appointment)
	if err != nil {
		slog.ErrorContext(ctx, "Could not convert request to model", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	err = persistQuery.Persist(ctx, appointment)

	if err != nil {
		slog.ErrorContext(ctx, "Could not save appointment", "error", err)
		return errors.NewServiceError(http.StatusInternalServerError, err)
	}

	return nil
}

func (s *Service) Delete(
	w http.ResponseWriter,
	r *http.Request,
	findById IdQuery,
	deleteQuery DeleteQuery,
	templating *template.Templating,
	flashHandler security.FlashHandler,
) {
	appointmentId, _ := strconv.Atoi(r.PathValue("appointmentId"))

	appointment, err := s.getAppointmentToEdit(r, appointmentId, findById)
	if err != nil {
		templating.RenderError(errors.GetErrorHttpCode(err), w, r)
		return
	}

	err = deleteQuery.Delete(r.Context(), appointment)
	if err != nil {
		slog.ErrorContext(r.Context(), "Could not delete appointment", "err", err)
		templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	flashHandler.AddFlash(r, "notice", "The appointment has been deleted.")
	http.Redirect(w, r, fmt.Sprintf("/admin/appointments/%d/", appointment.TeacherId), http.StatusSeeOther)
}

func (ar *Service) getWeekdays() map[string]string {
	return map[string]string{
		"0": "Sunday",
		"1": "Monday",
		"2": "Tuesday",
		"3": "Wednesday",
		"4": "Thursday",
		"5": "Friday",
		"6": "Saturday",
	}
}
