package appointment

import (
	"context"
	"fmt"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/user"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListByTeacher(t *testing.T) {
	userA := user.NewFakeUser(1)

	appointments := []model.Appointment{
		{
			Time: "15:04:12",
		},
	}

	cases := map[string]struct {
		Method       string
		Appointments []model.Appointment
		SecurityUser security.SecurityUser
		Result       int
	}{
		"should list": {Appointments: appointments, SecurityUser: userA, Result: http.StatusOK},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		appointmentRepository := &FakeRepository{Appointments: testCase.Appointments}
		userRepository := &user.FakeRepository{User: userA}
		templating := template.NewTemplating("test", session)

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.ListByTeacher(w, r, userRepository, appointmentRepository, templating)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestCreate(t *testing.T) {
	userA := user.NewFakeUser(1)
	userB := user.NewFakeUser(2)

	validData := map[string]string{
		"time":    "13:37",
		"weekday": "1",
	}

	validDataWithReservation := map[string]string{
		"time":               "13:37",
		"weekday":            "1",
		"reservation_reason": "Logan",
	}

	cases := map[string]struct {
		Method          string
		User            *model.User
		SecurityUser    security.SecurityUser
		FormData        map[string]string
		WithReservation bool
		CurrentSlot     *model.Appointment
		Result          int
	}{
		"404 on empty user POST": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusNotFound,
		},
		"404 on empty user GET": {
			Method:       "GET",
			SecurityUser: userA,
			Result:       http.StatusNotFound,
		},
		"200 on valid list": {
			Method:       "GET",
			User:         userA,
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"403 on user mismatch": {
			Method:       "GET",
			User:         userA,
			SecurityUser: userB,
			Result:       http.StatusForbidden,
		},
		"200 validation errors on invalid data": {
			Method:       "POST",
			User:         userA,
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"200 validation errors on already existing appointment": {
			Method:       "POST",
			User:         userA,
			SecurityUser: userA,
			FormData:     validData,
			CurrentSlot:  &model.Appointment{Id: 1},
			Result:       http.StatusOK,
		},
		"302 on valid data": {
			Method:       "POST",
			User:         userA,
			SecurityUser: userA,
			FormData:     validData,
			Result:       http.StatusSeeOther,
		},
		"302 on valid data with reservation": {
			Method:          "POST",
			User:            userA,
			SecurityUser:    userA,
			WithReservation: true,
			FormData:        validDataWithReservation,
			Result:          http.StatusSeeOther,
		},
		"403 on valid data with user mismatch": {
			Method:       "POST",
			User:         userA,
			SecurityUser: userB,
			FormData:     validData,
			Result:       http.StatusForbidden,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", test.CreateFormData(testCase.FormData))
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		if testCase.User != nil {
			request.SetPathValue("teacherId", fmt.Sprintf("%d", testCase.User.Id))
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &user.FakeRepository{User: testCase.User}
		appointmentRepository := &FakeRepository{AppointmentSlot: testCase.CurrentSlot}
		templating := template.NewTemplating("test", session)

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.New(w, r, userRepository, appointmentRepository, appointmentRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}

		if testCase.WithReservation && (appointmentRepository.AppointmentPersisted == nil || !appointmentRepository.AppointmentPersisted.ReservationReason.Valid) {
			t.Fatalf("Expected appointment to contain reservation for test case %s", index)
		}

		if !testCase.WithReservation && appointmentRepository.AppointmentPersisted != nil && appointmentRepository.AppointmentPersisted.ReservationReason.Valid {
			t.Fatalf("Expected appointment not to contain reservation for test case %s", index)
		}
	}
}

func TestEdit(t *testing.T) {
	userA := user.NewFakeUser(1)
	userB := user.NewFakeUser(2)

	appointment := &model.Appointment{
		TeacherId: userA.Id,
	}

	validData := map[string]string{
		"time":    "13:37:00",
		"weekday": "1",
	}

	cases := map[string]struct {
		Method       string
		Appointment  *model.Appointment
		SecurityUser security.SecurityUser
		FormData     map[string]string
		Result       int
	}{
		"404 on missing appointment POST": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusNotFound,
		},
		"404 on missing appointment GET": {Method: "GET",
			Appointment:  nil,
			SecurityUser: userA,
			Result:       http.StatusNotFound,
		},
		"200 on invalid access": {
			Method:       "GET",
			Appointment:  appointment,
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"403 on user mismatch": {
			Method:       "GET",
			Appointment:  appointment,
			SecurityUser: userB,
			Result:       http.StatusForbidden,
		},
		"200 on invalid data": {
			Method:       "POST",
			Appointment:  appointment,
			SecurityUser: userA,
			Result:       http.StatusOK,
		},
		"302 on valid data": {
			Method:       "POST",
			Appointment:  appointment,
			SecurityUser: userA,
			FormData:     validData,
			Result:       http.StatusSeeOther,
		},
		"403 on user mismatch POST": {
			Method:       "POST",
			Appointment:  appointment,
			SecurityUser: userB,
			FormData:     validData,
			Result:       http.StatusForbidden,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", test.CreateFormData(testCase.FormData))
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		if testCase.Appointment != nil {
			request.SetPathValue("appointmentId", fmt.Sprintf("%d", testCase.Appointment.Id))
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		appointmentRepository := &FakeRepository{Appointment: testCase.Appointment}
		templating := template.NewTemplating("test", session)

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Edit(w, r, appointmentRepository, appointmentRepository, appointmentRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestDelete(t *testing.T) {
	userA := user.NewFakeUser(1)
	userB := user.NewFakeUser(2)

	appointment := &model.Appointment{
		TeacherId: userA.Id,
	}

	cases := map[string]struct {
		Method       string
		Appointment  *model.Appointment
		SecurityUser security.SecurityUser
		Result       int
	}{
		"404 on missing appointment": {
			Method:       "POST",
			Appointment:  nil,
			SecurityUser: userA,
			Result:       http.StatusNotFound,
		},
		"302 on valid request": {
			Method:       "POST",
			Appointment:  appointment,
			SecurityUser: userA,
			Result:       http.StatusSeeOther,
		},
		"403 on invalid access": {
			Method:       "POST",
			Appointment:  appointment,
			SecurityUser: userB,
			Result:       http.StatusForbidden,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		if testCase.Appointment != nil {
			request.SetPathValue("appointmentId", fmt.Sprintf("%d", testCase.Appointment.Id))
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		appointmentRepository := &FakeRepository{Appointment: testCase.Appointment}
		templating := template.NewTemplating("test", session)

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Delete(w, r, appointmentRepository, appointmentRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}
