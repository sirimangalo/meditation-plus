package appointment

import (
	"context"
	"database/sql"
	"fmt"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/validation"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type AppointmentRequest struct {
	Time              string // Format: 15:04
	Weekday           string
	ReservationReason string
}

const MIN_HOURS_BEFORE_APPOINTMENT = 24

func (r *AppointmentRequest) Validate(ctx context.Context, teacherId int, findSlotQuery FindSlotQuery, currentId int32) validation.ValidationErrors {
	var errors validation.ValidationErrors = make(validation.ValidationErrors, 0)

	validation.Required(errors, "time", r.Time, "")
	validation.Length(errors, "time", r.Time, 5, 5, "")

	validation.Required(errors, "weekday", r.Weekday, "")
	weekday, _ := strconv.Atoi(r.Weekday)

	validation.Min(errors, "weekday", 0, weekday, "")
	validation.Max(errors, "weekday", 7, weekday, "")

	if len(errors) > 0 {
		return errors
	}

	slot, err := findSlotQuery.FindSlot(ctx, r.Time, weekday, teacherId)
	if err != nil {
		errors["time"] = append(errors["time"], "Could not check for conflicting appointments.")
	}

	if slot != nil && slot.Id != currentId {
		errors["time"] = append(errors["time"], "An appointment already exists at this time for this teacher.")
	}

	return errors
}

func (r *AppointmentRequest) ToModel(teacher *model.User) (*model.Appointment, error) {
	weekday, _ := strconv.Atoi(r.Weekday)

	return &model.Appointment{
		Time:              r.Time,
		Weekday:           time.Weekday(weekday),
		Timezone:          teacher.Timezone.String,
		TeacherId:         teacher.Id,
		ReservationReason: sql.NullString{Valid: r.ReservationReason != "", String: r.ReservationReason},
		BookedAt:          sql.NullTime{Valid: false},
		NotifiedAt:        sql.NullTime{Valid: false},
		CreatedAt:         time.Now(),
	}, nil
}

func (r *AppointmentRequest) UpdateModel(appointment *model.Appointment) error {
	weekday, _ := strconv.Atoi(r.Weekday)

	appointment.Weekday = time.Weekday(weekday)
	appointment.Time = r.Time
	appointment.ReservationReason = sql.NullString{Valid: r.ReservationReason != "", String: r.ReservationReason}

	return nil
}

func AppointmentRequestFromForm(r *http.Request) *AppointmentRequest {
	return &AppointmentRequest{
		Time:              removeSeconds(r.PostFormValue("time")),
		Weekday:           r.PostFormValue("weekday"),
		ReservationReason: r.PostFormValue("reservation_reason"),
	}
}

func AppointmentRequestFromModel(appointment *model.Appointment) *AppointmentRequest {
	return &AppointmentRequest{
		Time:              removeSeconds(appointment.Time),
		Weekday:           fmt.Sprintf("%d", appointment.Weekday),
		ReservationReason: appointment.ReservationReason.String,
	}
}

func removeSeconds(timeStr string) string {
	parts := strings.Split(timeStr, ":")
	if len(parts) < 2 {
		return timeStr
	}
	return fmt.Sprintf("%s:%s", parts[0], parts[1])
}
