package appointment

import (
	"context"
	"database/sql"
	"meditation-plus/pkg/database"
	"meditation-plus/pkg/model"
	"time"
)

type Repository struct{}

type BookQuery interface {
	Book(ctx context.Context, a *model.Appointment, userId int) error
	Unbook(ctx context.Context, a *model.Appointment, userId int64, doNotNotify bool) error
}
type IdQuery interface {
	FindById(ctx context.Context, id int) (*model.Appointment, error)
}
type UserIdQuery interface {
	FindByUserId(ctx context.Context, userId int) (*model.Appointment, error)
}
type AllQuery interface {
	FindAll(ctx context.Context) ([]model.ListAppointmentsRow, error)
}
type TeacherQuery interface {
	FindByTeacher(ctx context.Context, teacherId int) ([]model.FindAppointmentByTeacherRow, error)
}
type PersistQuery interface {
	Persist(ctx context.Context, a *model.Appointment) error
}
type DeleteQuery interface {
	Delete(ctx context.Context, a *model.Appointment) error
}
type NotifiedQuery interface {
	Notified(ctx context.Context, a *model.Appointment, when sql.NullTime) error
}
type FindSlotQuery interface {
	FindSlot(ctx context.Context, timeStr string, weekday int, teacherId int) (*model.Appointment, error)
}

func NewRepository() *Repository {
	return &Repository{}
}

func (r *Repository) Persist(ctx context.Context, a *model.Appointment) error {
	queries := model.New(database.Pool)

	if a.Id != 0 {
		_, err := queries.UpdateAppointment(ctx, model.UpdateAppointmentParams{
			Time:              a.Time,
			Weekday:           a.Weekday,
			Timezone:          a.Timezone,
			TeacherId:         a.TeacherId,
			UserId:            a.UserId,
			ReservationReason: a.ReservationReason,
			BookedAt:          a.BookedAt,
			NotifiedAt:        a.NotifiedAt,
			CreatedAt:         a.CreatedAt,
			Id:                a.Id,
		})

		if err != nil {
			return err
		}
	} else {
		result, err := queries.CreateAppointment(ctx, model.CreateAppointmentParams{
			Time:              a.Time,
			Weekday:           a.Weekday,
			Timezone:          a.Timezone,
			TeacherId:         a.TeacherId,
			UserId:            a.UserId,
			ReservationReason: a.ReservationReason,
			BookedAt:          a.BookedAt,
			NotifiedAt:        a.NotifiedAt,
			CreatedAt:         a.CreatedAt,
		})

		if err != nil {
			return err
		}

		lastId, err := result.LastInsertId()

		if err != nil {
			return err
		}

		a.Id = int32(lastId)
	}

	return nil
}

func (r *Repository) FindAll(ctx context.Context) ([]model.ListAppointmentsRow, error) {
	queries := model.New(database.Pool)

	appointments, err := queries.ListAppointments(ctx)
	if err != nil {
		return nil, err
	}

	return appointments, nil
}

func (r *Repository) FindById(ctx context.Context, id int) (*model.Appointment, error) {
	queries := model.New(database.Pool)
	appointment, err := queries.FindAppointmentById(ctx, int32(id))

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &appointment, nil
}

func (r *Repository) Notified(ctx context.Context, a *model.Appointment, when sql.NullTime) error {
	queries := model.New(database.Pool)
	return queries.NotifiedAppointment(ctx, model.NotifiedAppointmentParams{
		NotifiedAt: when,
		Id:         a.Id,
	})
}

func (r *Repository) Book(ctx context.Context, a *model.Appointment, userId int) error {
	queries := model.New(database.Pool)
	return queries.BookAppointment(ctx, model.BookAppointmentParams{
		UserId:   sql.NullInt64{Int64: int64(userId), Valid: true},
		BookedAt: sql.NullTime{Time: time.Now(), Valid: true},
		Id:       a.Id,
	})
}

// Unbooks an appointment for a specific user. The doNotNotify variable indicates that the
// teacher removed the user and therefore no update notification is needed.
func (r *Repository) Unbook(ctx context.Context, a *model.Appointment, userId int64, doNotNotify bool) error {
	queries := model.New(database.Pool)

	if doNotNotify {
		return queries.UnbookAppointmentWithoutNotification(ctx, model.UnbookAppointmentWithoutNotificationParams{
			UserId: sql.NullInt64{Int64: int64(userId), Valid: true},
			Id:     a.Id,
		})
	}

	return queries.UnbookAppointment(ctx, model.UnbookAppointmentParams{
		UserId: sql.NullInt64{Int64: int64(userId), Valid: true},
		Id:     a.Id,
	})
}

func (r *Repository) FindByUserId(ctx context.Context, userId int) (*model.Appointment, error) {
	queries := model.New(database.Pool)

	appointment, err := queries.FindAppointmentByUserId(ctx, sql.NullInt64{Int64: int64(userId), Valid: true})

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &appointment, nil
}

func (r *Repository) FindByTeacher(ctx context.Context, teacherId int) ([]model.FindAppointmentByTeacherRow, error) {
	queries := model.New(database.Pool)
	appointments, err := queries.FindAppointmentByTeacher(ctx, int64(teacherId))
	if err != nil {
		return nil, err
	}

	return appointments, nil
}

func (r *Repository) FindSlot(ctx context.Context, timeStr string, weekday int, teacherId int) (*model.Appointment, error) {
	queries := model.New(database.Pool)
	result, err := queries.FindAppointmentSlot(ctx, model.FindAppointmentSlotParams{
		Time:      timeStr,
		Weekday:   time.Weekday(weekday),
		TeacherId: int64(teacherId),
	})

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &result, nil
}

func (r *Repository) Delete(ctx context.Context, a *model.Appointment) error {
	queries := model.New(database.Pool)
	return queries.DeleteAppointment(ctx, a.Id)
}
