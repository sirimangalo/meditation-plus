package appointment

import (
	"meditation-plus/pkg/model"
	"net/http"
	"time"
)

// map length of string to select layout
var allowedLayoutsForSimulatedDate = map[int]string{
	10: "2006-01-02",
	16: "2006-01-02T15:04",
	20: "2006-01-02T15:04:05Z",
}

// parse optional query param "simulateNow=<date>" which can be used for testing
// the effect of different dates on the schedule in the browser
func getNowOrSimulatedDateFromQuery(r *http.Request) time.Time {
	simulatedNowQuery := r.URL.Query().Get("simulateNow")

	if len(simulatedNowQuery) > 20 {
		return time.Now()
	}

	if layout := allowedLayoutsForSimulatedDate[len(simulatedNowQuery)]; layout != "" {
		simulatedNow, err := time.Parse(layout, simulatedNowQuery)

		if err != nil {
			return time.Now()
		}

		return simulatedNow
	}

	return time.Now()
}

// get the exact time of the next scheduled appointment
func GetNextAppointmentTime(appointment model.Appointment, now time.Time) (time.Time, error) {
	appointmentLocation, err := time.LoadLocation(appointment.Timezone)
	if err != nil {
		return time.Time{}, err
	}

	nowInAppointmentLoc := now.In(appointmentLocation)

	appointmentTime, err := time.Parse("15:04:05", appointment.Time)
	if err != nil {
		return time.Time{}, err
	}

	nextAppointmentTime := time.Date(
		nowInAppointmentLoc.Year(),
		nowInAppointmentLoc.Month(),
		nowInAppointmentLoc.Day(),
		appointmentTime.Hour(),
		appointmentTime.Minute(),
		0,
		0,
		appointmentLocation,
	)

	addDays := 0
	if nowInAppointmentLoc.Weekday() == appointment.Weekday && nowInAppointmentLoc.After(nextAppointmentTime) {
		addDays = 7
	} else {
		addDays = int(appointment.Weekday) - int(nowInAppointmentLoc.Weekday())
		if addDays < 0 {
			addDays += 7
		}
	}

	nextAppointmentTime = nextAppointmentTime.AddDate(0, 0, addDays)

	return nextAppointmentTime, nil
}
