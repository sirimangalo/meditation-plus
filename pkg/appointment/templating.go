package appointment

import (
	"cmp"
	gotemplate "html/template"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/template"
	"slices"
	"time"
)

func init() {
	funcMap := gotemplate.FuncMap{
		"SortAppointments":       SortAppointmentsList,
		"DisplayTimeInZone":      DisplayTimeInZone,
		"DisplayWeekdayInZone":   DisplayWeekdayInZone,
		"GetUpcomingTimeChanges": getUpcomingTimeChangesNow,
		"IsNextTimeSkipped":      isNextTimeSkipped,
		"GetNextAppointmentTime": GetNextAppointmentTime,
	}
	template.RegisterFuncMap(funcMap)
}

func DisplayInZone(t time.Time, timezone string, format string) string {
	loc, err := time.LoadLocation(timezone)

	if err != nil {
		return t.Format("15:04")
	}

	return t.In(loc).Format(format)
}

func DisplayTimeInZone(t time.Time, timezone string) string {
	return DisplayInZone(t, timezone, "15:04")
}

func DisplayWeekdayInZone(t time.Time, timezone string) string {
	loc, err := time.LoadLocation(timezone)

	if err != nil {
		return t.Weekday().String()
	}

	return t.In(loc).Weekday().String()
}

type timeChange struct {
	StartingAt string
	NewTime    string
	NewWeekday string
}

func getUpcomingTimeChanges(appointment model.Appointment, timezone string, now time.Time) []timeChange {
	changes := []timeChange{}
	loc, err := time.LoadLocation(timezone)

	if err != nil {
		return changes
	}

	lastNextAppointment, _ := GetNextAppointmentTime(appointment, now)

	// courses have a duration of 12-14 weeks according to current information
	// make sure the list of changes is complete with an additional buffer
	// to have that information in advance when booking the slot
	for range 17 {
		currentNextTime, err := GetNextAppointmentTime(appointment, lastNextAppointment.Add(1))
		if err != nil {
			break
		}

		isDifferentTime := lastNextAppointment.In(loc).Format("15:04") != currentNextTime.In(loc).Format("15:04")
		isDifferentWeekday := lastNextAppointment.In(loc).Weekday() != currentNextTime.In(loc).Weekday()
		if isDifferentTime || isDifferentWeekday {
			change := timeChange{
				StartingAt: currentNextTime.In(loc).Format("January 2, 2006"),
				NewTime:    DisplayTimeInZone(currentNextTime, timezone),
				NewWeekday: "",
			}

			if isDifferentWeekday {
				change.NewWeekday = DisplayWeekdayInZone(currentNextTime, timezone)
			}

			changes = append(changes, change)
		}

		lastNextAppointment = currentNextTime
	}

	return changes
}

// returns an array of next appointment times at which the local time differs from
// the current next appointment time
func getUpcomingTimeChangesNow(appointment model.Appointment, timezone string) []timeChange {
	return getUpcomingTimeChanges(appointment, timezone, time.Now())
}

func SortAppointmentsList(list []model.ListAppointmentsRow, timezone string, now time.Time) []model.ListAppointmentsRow {
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		return list
	}

	// sort by local weekday/time using monday as first day of week
	slices.SortStableFunc(list, func(a, b model.ListAppointmentsRow) int {
		aNextTime, _ := GetNextAppointmentTime(a.Appointment, now)
		bNextTime, _ := GetNextAppointmentTime(b.Appointment, now)
		aLocalTime := aNextTime.In(loc)
		bLocalTime := bNextTime.In(loc)

		if aLocalTime.Weekday() == bLocalTime.Weekday() {
			return cmp.Compare(
				aLocalTime.Hour()*100+aLocalTime.Minute(),
				bLocalTime.Hour()*100+bLocalTime.Minute(),
			)
		}

		return cmp.Compare(aLocalTime.Weekday(), bLocalTime.Weekday())
	})

	return list
}

// when an appointment is booked right before the next future slot would start,
// the user should be informed that the actual next slot will be next week.
func isNextTimeSkipped(a model.Appointment, nextTime time.Time) bool {
	if nextTime.IsZero() || !a.BookedAt.Valid || a.BookedAt.Time.IsZero() {
		return false
	}

	if nextTime.Before(a.BookedAt.Time) {
		return false
	}

	hourDiff := nextTime.Sub(a.BookedAt.Time).Hours()
	return hourDiff <= MIN_HOURS_BEFORE_APPOINTMENT
}
