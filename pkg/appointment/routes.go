package appointment

import (
	"log/slog"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/user"
	"net/http"
)

type Routes struct {
	repository     *Repository
	userRepository *user.Repository
	flashHandler   security.FlashHandler
	templating     *template.Templating
	service        *Service
}

func NewRoutes(
	repository *Repository,
	userRepository *user.Repository,
	flashHandler security.FlashHandler,
	templating *template.Templating,
	service *Service,
) *Routes {
	return &Routes{
		repository:     repository,
		userRepository: userRepository,
		flashHandler:   flashHandler,
		templating:     templating,
		service:        service,
	}
}

func (ar *Routes) Index(w http.ResponseWriter, r *http.Request) {
	ar.service.List(
		w,
		r,
		ar.repository,
		ar.repository,
		ar.userRepository,
		ar.repository,
		ar.userRepository,
		ar.templating,
	)
}

func (ar *Routes) Cancel(w http.ResponseWriter, r *http.Request) {
	ar.service.Cancel(
		w,
		r,
		ar.repository,
		ar.repository,
		ar.templating,
		ar.flashHandler,
	)
}

func (ar *Routes) Book(w http.ResponseWriter, r *http.Request) {
	ar.service.Book(
		w,
		r,
		ar.repository,
		ar.repository,
		ar.repository,
		ar.templating,
		ar.flashHandler,
	)
}

func (ar *Routes) AdminIndex(w http.ResponseWriter, r *http.Request) {
	teachers, err := ar.userRepository.FindTeacher(r.Context())

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not find teacher", "error", err)
		ar.templating.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	ar.templating.Render("admin/appointment/index.html", map[string]interface{}{
		"teachers": teachers,
	}, w, r)
}

func (ar *Routes) AdminList(w http.ResponseWriter, r *http.Request) {
	ar.service.ListByTeacher(w, r, ar.userRepository, ar.repository, ar.templating)
}

func (ar *Routes) New(w http.ResponseWriter, r *http.Request) {
	ar.service.New(w, r, ar.userRepository, ar.repository, ar.repository, ar.templating, ar.flashHandler)
}

func (ar *Routes) Edit(w http.ResponseWriter, r *http.Request) {
	ar.service.Edit(w, r, ar.repository, ar.repository, ar.repository, ar.templating, ar.flashHandler)
}

func (ar *Routes) Delete(w http.ResponseWriter, r *http.Request) {
	ar.service.Delete(w, r, ar.repository, ar.repository, ar.templating, ar.flashHandler)
}
