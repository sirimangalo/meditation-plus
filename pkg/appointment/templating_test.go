package appointment

import (
	"database/sql"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/test"
	"testing"
	"time"
)

func TestShouldDisplayTime(t *testing.T) {
	testTime, _ := time.Parse(time.RFC3339, "2024-05-02T11:34:00Z")
	timeInvalidLoc := DisplayTimeInZone(testTime, "invalid")
	test.AssertEquals(t, "11:34", timeInvalidLoc)

	timeLocalized := DisplayTimeInZone(testTime, "Europe/Berlin")
	test.AssertEquals(t, "13:34", timeLocalized)
}

func TestShouldDisplayWeekday(t *testing.T) {
	testTime, _ := time.Parse(time.RFC3339, "2024-05-02T23:00:00Z")
	test.AssertEquals(t, "Thursday", DisplayWeekdayInZone(testTime, "invalid"))
	test.AssertEquals(t, "Friday", DisplayWeekdayInZone(testTime, "Europe/Berlin"))
}

func TestShouldGetUpcomingAppointmentChanges(t *testing.T) {
	testNow, _ := time.Parse(time.RFC3339, "2024-10-23T11:34:00-02:00")
	testAppointment := model.Appointment{Timezone: "America/Toronto", Time: "18:30:00", Weekday: 5}
	changes := getUpcomingTimeChanges(testAppointment, "Europe/Berlin", testNow)
	test.AssertEquals(t, len(changes), 2)

	// right now it should be on Saturday, 00:30; CET changes Oct 27, both time and day change
	test.AssertEquals(t, changes[0].StartingAt, "November 1, 2024")
	test.AssertEquals(t, changes[0].NewTime, "23:30")
	test.AssertEquals(t, changes[0].NewWeekday, "Friday")

	// ET change on Nov 2, so it changes back again after that
	test.AssertEquals(t, changes[1].StartingAt, "November 9, 2024")
	test.AssertEquals(t, changes[1].NewTime, "00:30")
	test.AssertEquals(t, changes[1].NewWeekday, "Saturday")
}

func TestShouldSortAppointments(t *testing.T) {
	testNow, _ := time.Parse(time.RFC3339, "2024-08-23T11:34:00-02:00")

	appointments := []model.ListAppointmentsRow{
		{Appointment: model.Appointment{Timezone: "America/Toronto", Time: "17:30:00", Weekday: 4}},
		{Appointment: model.Appointment{Timezone: "America/Toronto", Time: "16:31:00", Weekday: 4}},
		{Appointment: model.Appointment{Timezone: "America/Toronto", Time: "16:30:00", Weekday: 4}},
		{Appointment: model.Appointment{Timezone: "America/Toronto", Time: "17:30:00", Weekday: 5}},
		{Appointment: model.Appointment{Timezone: "America/Toronto", Time: "18:30:00", Weekday: 5}},
		{Appointment: model.Appointment{Timezone: "America/Toronto", Time: "12:00:00", Weekday: 6}},
		{Appointment: model.Appointment{Timezone: "America/Toronto", Time: "18:30:00", Weekday: 6}},
	}

	for i := range appointments {
		appointments[i].Appointment.Id = int32(i + 1)
	}

	sorted := SortAppointmentsList(appointments, "Europe/Berlin", testNow)

	test.AssertEquals(t, len(sorted), 7)
	test.AssertEquals(t, sorted[0].Appointment.Id, int32(7))
	test.AssertEquals(t, sorted[1].Appointment.Id, int32(3))
	test.AssertEquals(t, sorted[2].Appointment.Id, int32(2))
	test.AssertEquals(t, sorted[3].Appointment.Id, int32(1))
	test.AssertEquals(t, sorted[4].Appointment.Id, int32(4))
	test.AssertEquals(t, sorted[5].Appointment.Id, int32(5))
	test.AssertEquals(t, sorted[6].Appointment.Id, int32(6))
}

func TestShouldShowNextTime(t *testing.T) {
	test.AssertEquals(t, isNextTimeSkipped(model.Appointment{}, time.Time{}), false)
	test.AssertEquals(t, isNextTimeSkipped(model.Appointment{BookedAt: sql.NullTime{Time: time.Time{}, Valid: false}}, time.Time{}), false)
	test.AssertEquals(t, isNextTimeSkipped(model.Appointment{}, time.Time{}), false)

	bookingDate1, _ := time.Parse(time.RFC3339, "2024-08-22T11:41:00-02:00")
	nextTimeDate1, _ := time.Parse(time.RFC3339, "2024-08-23T11:40:00-02:00")
	test.AssertEquals(t, isNextTimeSkipped(model.Appointment{
		BookedAt: sql.NullTime{Time: bookingDate1, Valid: true},
	}, nextTimeDate1), true)

	bookingDate2, _ := time.Parse(time.RFC3339, "2024-08-23T11:40:00-02:00")
	nextTimeDate2, _ := time.Parse(time.RFC3339, "2024-08-23T11:00:00-02:00")
	test.AssertEquals(t, isNextTimeSkipped(model.Appointment{
		BookedAt: sql.NullTime{Time: bookingDate2, Valid: true},
	}, nextTimeDate2), false)

	bookingDate3, _ := time.Parse(time.RFC3339, "2024-08-22T11:59:00-02:00")
	nextTimeDate3, _ := time.Parse(time.RFC3339, "2024-08-23T12:00:00-02:00")
	test.AssertEquals(t, isNextTimeSkipped(model.Appointment{
		BookedAt: sql.NullTime{Time: bookingDate3, Valid: true},
	}, nextTimeDate3), false)
}
