package appointment

import (
	"context"
	"database/sql"
	"fmt"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/test"
	"meditation-plus/pkg/user"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func TestList(t *testing.T) {
	userA := user.NewFakeUser(1)
	userB := user.NewFakeUser(2)
	teacherA := user.NewFakeUserWithRole(3, model.USER_ROLES_TEACHER)

	appointmentsUserA := []model.ListAppointmentsRow{
		{
			Appointment: model.Appointment{
				UserId:    sql.NullInt64{Int64: userA.Id, Valid: true},
				TeacherId: teacherA.Id,
				Time:      "15:04:05",
			},
			UserName: sql.NullString{
				Valid:  true,
				String: "userA",
			},
			UserDisplayName: sql.NullString{
				Valid:  true,
				String: "userA",
			},
		},
	}

	appointmentsUserB := []model.ListAppointmentsRow{
		{
			Appointment: model.Appointment{
				UserId:    sql.NullInt64{Int64: userB.Id, Valid: true},
				TeacherId: teacherA.Id,
				Time:      "15:04:05",
			},
			UserName: sql.NullString{
				Valid:  true,
				String: "userB",
			},
			UserDisplayName: sql.NullString{
				Valid:  true,
				String: "userB",
			},
		},
	}

	cases := map[string]struct {
		Method          string
		Appointments    []model.ListAppointmentsRow
		SecurityUser    security.SecurityUser
		TextEndContent  string
		BodyContains    string
		BodyNotContains string
		Result          int
	}{
		"200 on list for user": {
			Method:          "POST",
			SecurityUser:    userA,
			Appointments:    appointmentsUserA,
			BodyContains:    "Show taken",
			BodyNotContains: "Show free",
			TextEndContent:  "userA",
			Result:          http.StatusOK,
		},
		"200 on list for teacher": {
			Method:          "POST",
			SecurityUser:    teacherA,
			Appointments:    appointmentsUserA,
			BodyContains:    "Show free",
			BodyNotContains: "Show taken",
			Result:          http.StatusOK,
		},
		"should redact usernames": {
			Method:         "POST",
			SecurityUser:   userA,
			Appointments:   appointmentsUserB,
			TextEndContent: "Taken",
			Result:         http.StatusOK,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		userRepository := &user.FakeRepository{}
		appointmentRepository := &FakeRepository{AppointmentList: testCase.Appointments}
		templating := template.NewTemplating("test", session)

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.List(w, r, appointmentRepository, appointmentRepository, userRepository, appointmentRepository, userRepository, templating)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}

		bodyString := recorder.Body.String()
		body, err := html.Parse(recorder.Body)
		if err != nil {
			t.Fatalf("Could not parse body: %v", err)
		}
		node := test.GetFirstElementByClassName(body, "text-end")
		node = test.GetFirstElement(node, func(n *html.Node) bool {
			return true
		})
		if node == nil || !test.NodeContainsText(node, testCase.TextEndContent) {
			fmt.Printf("Body %s", bodyString)
			t.Fatalf(`Expected .text-end to contain text "%s" for test case '%s'`, testCase.TextEndContent, index)
		}

		if testCase.BodyContains != "" && !strings.Contains(bodyString, testCase.BodyContains) {
			fmt.Printf("Body %s", bodyString)
			t.Fatalf(`Expected body to contain text "%s" for test case '%s'`, testCase.BodyContains, index)
		}

		if testCase.BodyNotContains != "" && strings.Contains(bodyString, testCase.BodyNotContains) {
			fmt.Printf("Body %s", bodyString)
			t.Fatalf(`Expected body not to contain text "%s" for test case '%s'`, testCase.BodyNotContains, index)
		}
	}
}

func TestBook(t *testing.T) {
	userA := user.NewFakeUser(1)

	bookedAppointment := &model.Appointment{
		UserId: sql.NullInt64{Int64: userA.Id, Valid: true},
	}
	freeAppointment := &model.Appointment{}

	cases := map[string]struct {
		Method            string
		Appointment       *model.Appointment
		AppointmentByUser *model.Appointment
		SecurityUser      security.SecurityUser
		Result            int
	}{
		"404 on missing appointment": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusNotFound,
		},
		"400 on already booked appointment": {
			Method:       "POST",
			Appointment:  bookedAppointment,
			SecurityUser: userA,
			Result:       http.StatusBadRequest,
		},
		"400 when user already booked another appointment": {
			Method:            "POST",
			Appointment:       freeAppointment,
			AppointmentByUser: bookedAppointment,
			SecurityUser:      userA,
			Result:            http.StatusBadRequest,
		},
		"should book": {
			Method:       "POST",
			Appointment:  freeAppointment,
			SecurityUser: userA,
			Result:       http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		appointmentRepository := &FakeRepository{Appointment: testCase.Appointment, AppointmentByUser: testCase.AppointmentByUser}
		templating := template.NewTemplating("test", session)

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Book(w, r, appointmentRepository, appointmentRepository, appointmentRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			fmt.Printf("Body %s", recorder.Body)
			t.Fatalf("Expected status %d to match %d for test case %s", recorder.Code, testCase.Result, index)
		}
	}
}

func TestCancel(t *testing.T) {
	userA := user.NewFakeUser(1)
	userB := user.NewFakeUser(2)
	teacherA := user.NewFakeUserWithRole(3, model.USER_ROLES_TEACHER)
	teacherB := user.NewFakeUserWithRole(4, model.USER_ROLES_TEACHER)

	bookedAppointment := &model.Appointment{
		UserId:    sql.NullInt64{Int64: userA.Id, Valid: true},
		TeacherId: teacherA.Id,
	}
	bookedBAppointment := &model.Appointment{
		UserId:    sql.NullInt64{Int64: userB.Id, Valid: true},
		TeacherId: teacherA.Id,
	}
	freeAppointment := &model.Appointment{}

	cases := map[string]struct {
		Method       string
		Appointment  *model.Appointment
		SecurityUser security.SecurityUser
		Result       int
	}{
		"404 on missing appointment": {
			Method:       "POST",
			SecurityUser: userA,
			Result:       http.StatusNotFound,
		},
		"403 on appointment of other user": {
			Method:       "POST",
			Appointment:  bookedBAppointment,
			SecurityUser: userA,
			Result:       http.StatusForbidden,
		},
		"403 on appointment of other teacher": {
			Method:       "POST",
			Appointment:  bookedBAppointment,
			SecurityUser: teacherB,
			Result:       http.StatusForbidden,
		},
		"400 on empty appointment": {
			Method:       "POST",
			Appointment:  freeAppointment,
			SecurityUser: userA,
			Result:       http.StatusBadRequest,
		},
		"should cancel for own appointment": {
			Method:       "POST",
			Appointment:  bookedAppointment,
			SecurityUser: userA,
			Result:       http.StatusSeeOther,
		},
		"should cancel for appointment of own table": {
			Method:       "POST",
			Appointment:  bookedAppointment,
			SecurityUser: teacherA,
			Result:       http.StatusSeeOther,
		},
	}

	for index, testCase := range cases {
		recorder := httptest.NewRecorder()
		request, err := http.NewRequest(testCase.Method, "/", nil)
		if err != nil {
			t.Fatalf("Error creating request: %v", err)
		}

		ctx := context.WithValue(request.Context(), security.UserContextKey, testCase.SecurityUser)
		request = request.WithContext(ctx)
		session := &test.FakeSessionStore{}
		service := &Service{}
		appointmentRepository := &FakeRepository{Appointment: testCase.Appointment}
		templating := template.NewTemplating("test", session)

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			service.Cancel(w, r, appointmentRepository, appointmentRepository, templating, session)
		})

		handler.ServeHTTP(recorder, request)

		if recorder.Code != testCase.Result {
			t.Fatalf("Expected status %d to match %d for test case '%s'", recorder.Code, testCase.Result, index)
		}
	}
}
