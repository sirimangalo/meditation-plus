package appointment

import (
	"context"
	"database/sql"
	"meditation-plus/pkg/model"
)

type FakeRepository struct {
	Appointment          *model.Appointment
	AppointmentList      []model.ListAppointmentsRow
	Appointments         []model.Appointment
	AppointmentByTeacher []model.FindAppointmentByTeacherRow
	AppointmentByUser    *model.Appointment
	AppointmentSlot      *model.Appointment
	AppointmentPersisted *model.Appointment
}

func (q *FakeRepository) Persist(ctx context.Context, appointment *model.Appointment) error {
	q.AppointmentPersisted = appointment
	return nil
}
func (q *FakeRepository) Delete(ctx context.Context, appointment *model.Appointment) error {
	return nil
}
func (q *FakeRepository) FindById(ctx context.Context, id int) (*model.Appointment, error) {
	return q.Appointment, nil
}
func (q *FakeRepository) FindAll(ctx context.Context) ([]model.ListAppointmentsRow, error) {
	return q.AppointmentList, nil
}
func (q *FakeRepository) FindByTeacher(ctx context.Context, teacherId int) ([]model.FindAppointmentByTeacherRow, error) {
	return q.AppointmentByTeacher, nil
}
func (q *FakeRepository) Book(ctx context.Context, a *model.Appointment, userId int) error {
	return nil
}
func (q *FakeRepository) Unbook(ctx context.Context, a *model.Appointment, userId int64, doNotNotify bool) error {
	return nil
}
func (q *FakeRepository) Notified(ctx context.Context, na *model.Appointment, when sql.NullTime) error {
	return nil
}
func (q *FakeRepository) FindByUserId(ctx context.Context, userId int) (*model.Appointment, error) {
	return q.AppointmentByUser, nil
}
func (q *FakeRepository) FindSlot(ctx context.Context, time string, weekday int, teacherId int) (*model.Appointment, error) {
	return q.AppointmentSlot, nil
}
