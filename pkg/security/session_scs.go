package security

import (
	"database/sql"
	"net/http"
	"time"

	"github.com/alexedwards/scs/mysqlstore"
	"github.com/alexedwards/scs/v2"
)

type ScsSessionStore struct {
	sessionManager *scs.SessionManager
}

func (ss *ScsSessionStore) Get(r *http.Request, name string) interface{} {
	return ss.sessionManager.Get(r.Context(), name)
}

func (ss *ScsSessionStore) Add(r *http.Request, name string, value interface{}) {
	ss.sessionManager.Put(r.Context(), name, value)
}

func (ss *ScsSessionStore) AddFlash(r *http.Request, flashType string, message string) {
	ss.sessionManager.Put(r.Context(), "flash_"+flashType, message)
}

func (ss *ScsSessionStore) GetFlash(r *http.Request, flashType string) string {
	return ss.sessionManager.PopString(r.Context(), "flash_"+flashType)
}

func (ss *ScsSessionStore) Destroy(r *http.Request) error {
	return ss.sessionManager.Destroy(r.Context())
}

func (ss *ScsSessionStore) Middleware(next http.Handler) http.Handler {
	return ss.sessionManager.LoadAndSave(next)
}

func NewScsSessionStore(appEnv string, db *sql.DB) *ScsSessionStore {
	sessionManager := scs.New()
	sessionManager.Lifetime = 24 * time.Hour * 7
	sessionManager.Store = mysqlstore.New(db)
	sessionManager.Cookie.Secure = appEnv != "test"

	return &ScsSessionStore{
		sessionManager: sessionManager,
	}
}
