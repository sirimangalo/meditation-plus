package security

import "net/http"

type FakeSecurityUser struct {
	Id   int
	Role string
}

func (u *FakeSecurityUser) GetId() int {
	return u.Id
}
func (u *FakeSecurityUser) IsGranted(role string) bool {
	return u.Role == role
}

type FakeUserProvider struct {
	User *FakeSecurityUser
}

func (p *FakeUserProvider) GetUser(r *http.Request) SecurityUser {
	if p.User == nil {
		return nil
	}
	return p.User
}

type FakeSessionValueGetter struct {
	Value interface{}
}

func (g *FakeSessionValueGetter) Get(r *http.Request, name string) interface{} {
	return g.Value
}
