package security

import (
	"meditation-plus/pkg/test"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestShouldRedirectOnEmptyUser(t *testing.T) {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("Hello, world!"))
	})

	userProvider := &FakeUserProvider{
		User: nil,
	}
	errorRenderer := &test.FakeErrorRenderer{}
	session := &test.FakeSessionStore{}
	middleware := NewAuthMiddleware(userProvider, "/", errorRenderer, session)
	wrappedHandler := middleware.Handle(handler)

	wrappedHandler.ServeHTTP(recorder, request)

	test.AssertEquals(t, http.StatusSeeOther, recorder.Code)
}

func TestShouldCallHandleOnValidUser(t *testing.T) {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("Hello, world!"))
	})

	userProvider := &FakeUserProvider{
		User: &FakeSecurityUser{
			Role: "USER",
		},
	}
	errorRenderer := &test.FakeErrorRenderer{}
	session := &test.FakeSessionStore{}
	middleware := NewAuthMiddleware(userProvider, "/", errorRenderer, session)
	wrappedHandler := middleware.Handle(handler)

	wrappedHandler.ServeHTTP(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
}

func TestShouldShow401OnRoleMismatch(t *testing.T) {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("Hello, world!"))
	})

	userProvider := &FakeUserProvider{
		User: &FakeSecurityUser{
			Role: "USER",
		},
	}
	errorRenderer := &test.FakeErrorRenderer{}
	session := &test.FakeSessionStore{}
	middleware := NewAuthMiddleware(userProvider, "/", errorRenderer, session)
	wrappedHandler := middleware.HandleRole(handler, []string{"ADMIN"})

	wrappedHandler.ServeHTTP(recorder, request)

	test.AssertEquals(t, http.StatusForbidden, recorder.Code)
}

func TestShouldCallHandleOnValidUserRoles(t *testing.T) {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("Hello, world!"))
	})

	userProvider := &FakeUserProvider{
		User: &FakeSecurityUser{
			Role: "USER",
		},
	}
	errorRenderer := &test.FakeErrorRenderer{}
	session := &test.FakeSessionStore{}
	middleware := NewAuthMiddleware(userProvider, "/", errorRenderer, session)
	wrappedHandler := middleware.HandleRole(handler, []string{"ADMIN", "USER"})

	wrappedHandler.ServeHTTP(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
}
