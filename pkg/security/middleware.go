package security

import (
	"context"
	"meditation-plus/pkg/errors"
	"net/http"

	"github.com/justinas/nosurf"
)

type userContextKey string

const UserContextKey = userContextKey("user")
const SessionKeyPageRequested = "auth_page_requested"

/**
 * This method should only be used in handlers where
 * it's ensured that a user context is present.
 */
func GetUser(r *http.Request) SecurityUser {
	return r.Context().Value(UserContextKey).(SecurityUser)
}

func CsrfMiddleware(appEnv string, handler http.Handler) http.Handler {
	csrf := nosurf.New(handler)
	csrf.SetBaseCookie(http.Cookie{
		Name:     "csrf_token",
		Secure:   appEnv != "test",
		HttpOnly: true,
	})
	return csrf
}

type AuthMiddleware interface {
	Handle(next http.HandlerFunc) http.HandlerFunc
	HandleRole(next http.HandlerFunc, roles []string) http.HandlerFunc
}

func NewAuthMiddleware(
	userProvider SessionUserProvider,
	loginRoute string,
	errorRenderer errors.ErrorRenderer,
	sessionStore SessionStore,
) AuthMiddleware {
	return &authMiddlewareImpl{
		userProvider,
		loginRoute,
		errorRenderer,
		sessionStore,
	}
}

type authMiddlewareImpl struct {
	userProvider  SessionUserProvider
	loginRoute    string
	errorRenderer errors.ErrorRenderer
	sessionStore  SessionStore
}

func (a *authMiddlewareImpl) Handle(next http.HandlerFunc) http.HandlerFunc {
	return a.HandleRole(next, []string{"USER"})
}

func (a *authMiddlewareImpl) HandleRole(next http.HandlerFunc, roles []string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user := a.userProvider.GetUser(r)

		if user == nil {
			a.sessionStore.Add(r, SessionKeyPageRequested, r.URL.String())
			http.Redirect(w, r, a.loginRoute, http.StatusSeeOther)
			return
		}

		ctx := context.WithValue(r.Context(), UserContextKey, user)

		matchedRole := false
		for _, role := range roles {
			if user.IsGranted(role) {
				matchedRole = true
				break
			}
		}

		if !matchedRole {
			a.errorRenderer.RenderError(http.StatusForbidden, w, r.WithContext(ctx))
			return
		}

		next(w, r.WithContext(ctx))
	}
}
