package security

import (
	"bytes"
	"errors"
	"io"
	"meditation-plus/pkg/test"
	"net/http"
	"net/http/httptest"
	"testing"

	"golang.org/x/oauth2"
)

type FakeUser struct {
	SecurityUser
	Identifier string
}

func (u *FakeUser) GetId() int {
	return 0
}

type FakeTemplating struct {
}

func (t *FakeTemplating) RenderError(errorCode int, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(errorCode)
}

type FakeUserRepository struct {
	ProvidedUser  SecurityUser
	CreatedUser   SecurityUser
	CreateUserErr error
}

func (p *FakeUserRepository) ProvideUserByIdentifier(identifier string) (SecurityUser, error) {
	if fakeUser, ok := p.ProvidedUser.(*FakeUser); ok {
		fakeUser.Identifier = identifier
		return fakeUser, nil
	}
	return p.ProvidedUser, nil
}

func (p *FakeUserRepository) CreateUser(userData map[string]interface{}) (SecurityUser, error) {
	if p.CreateUserErr != nil {
		return nil, p.CreateUserErr
	}
	if fakeUser, ok := p.CreatedUser.(*FakeUser); ok {
		fakeUser.Identifier = userData["id"].(string)
		return fakeUser, nil
	}
	return p.CreatedUser, nil
}
func (p *FakeUserRepository) UpdateUser(user SecurityUser, userData map[string]interface{}) error {
	return nil
}

type FakeAccessTokenChecker struct {
	Error error
}

func (f *FakeAccessTokenChecker) CheckAccessToken(accessToken *oauth2.Token, userInfo map[string]interface{}) error {
	return f.Error
}

type FakeEndpoint struct {
	StatusCode int
	Body       io.ReadCloser
}

func (e *FakeEndpoint) Exchange(oauthConfig *oauth2.Config, token string) (*oauth2.Token, error) {
	return nil, nil
}

func (e *FakeEndpoint) Get(oauthConfig *oauth2.Config, token *oauth2.Token, endpoint string) (resp *http.Response, err error) {
	return &http.Response{
		StatusCode: e.StatusCode,
		Body:       e.Body,
	}, nil
}

func createAuthenticator(userInfoResponse string, providedUser SecurityUser, createdUser SecurityUser, createUserError error, accessTokenCheckerError error) *OAuth2Authenticator {
	config := &oauth2.Config{
		ClientID:     "your-client-id",
		ClientSecret: "your-client-secret",
		RedirectURL:  "your-redirect-url",
		Endpoint: oauth2.Endpoint{
			TokenURL: "token-url",
		},
	}
	endpoint := &FakeEndpoint{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader([]byte(userInfoResponse))),
	}
	userRepo := &FakeUserRepository{
		ProvidedUser:  providedUser,
		CreatedUser:   createdUser,
		CreateUserErr: createUserError,
	}
	auth := &OAuth2Authenticator{
		session: &test.FakeSessionStore{
			GetData: "oauthState",
		},
		errorRenderer:   &FakeTemplating{},
		oauthConfig:     config,
		userProvider:    userRepo,
		userCreator:     userRepo,
		userInfoGetter:  endpoint,
		exchangeHandler: endpoint,
		accessTokenChecker: &FakeAccessTokenChecker{
			Error: accessTokenCheckerError,
		},
		shouldCreateUser: true,
		userEndpoint:     "endpoint",
	}

	return auth
}

func TestShouldFailOnInvalidJson(t *testing.T) {
	auth := createAuthenticator("invalidJson", nil, nil, nil, nil)
	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/auth", nil)
	if err != nil {
		t.Fatalf("Error creating request: %v", err)
	}
	request.Form = map[string][]string{"code": {"valid-code"}, "state": {"oauthState"}}

	user, _ := auth.Authenticate(recorder, request)

	test.AssertEquals(t, http.StatusInternalServerError, recorder.Code)
	test.AssertEquals(t, nil, user)
}

func TestShouldReturnUserWhenFound(t *testing.T) {
	user := &FakeUser{}
	auth := createAuthenticator(`{"id": "identifier"}`, user, nil, nil, nil)
	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/auth", nil)
	if err != nil {
		t.Fatalf("Error creating request: %v", err)
	}
	request.Form = map[string][]string{"code": {"valid-code"}, "state": {"oauthState"}}

	returnedUser, _ := auth.Authenticate(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
	test.AssertEquals(t, user, returnedUser)
	test.AssertEquals(t, "identifier", returnedUser.(*FakeUser).Identifier)
}

func TestShouldCreateUserWhenNotFound(t *testing.T) {
	user := &FakeUser{}
	auth := createAuthenticator(`{"id": "identifier"}`, nil, user, nil, nil)
	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/auth", nil)
	if err != nil {
		t.Fatalf("Error creating request: %v", err)
	}
	request.Form = map[string][]string{"code": {"valid-code"}, "state": {"oauthState"}}

	returnedUser, _ := auth.Authenticate(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
	test.AssertEquals(t, user, returnedUser)
	test.AssertEquals(t, "identifier", returnedUser.(*FakeUser).Identifier)
}

func TestShouldErrorWhenUserCouldNotBeCreated(t *testing.T) {
	auth := createAuthenticator(`{"id": "identifier"}`, nil, nil, nil, nil)
	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/auth", nil)
	if err != nil {
		t.Fatalf("Error creating request: %v", err)
	}
	request.Form = map[string][]string{"code": {"valid-code"}, "state": {"oauthState"}}

	returnedUser, err := auth.Authenticate(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
	test.AssertEquals(t, nil, returnedUser)
	if !errors.Is(ErrCouldNotLogin, err) {
		t.Errorf("Got %q, want %q", err, ErrCouldNotLogin)
	}
}

func TestShouldRedirectToHomepageAndShowErrorWhenInvalidUserData(t *testing.T) {
	auth := createAuthenticator(`{"id": "identifier"}`, nil, nil, ErrUserDataInvalid, nil)
	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/auth", nil)
	if err != nil {
		t.Fatalf("Error creating request: %v", err)
	}
	request.Form = map[string][]string{"code": {"valid-code"}, "state": {"oauthState"}}

	returnedUser, err := auth.Authenticate(recorder, request)

	test.AssertEquals(t, http.StatusOK, recorder.Code)
	test.AssertEquals(t, nil, returnedUser)
	if !errors.Is(ErrUserDataInvalid, err) {
		t.Errorf("Got %q, want %q", err, ErrCouldNotLogin)
	}
}
