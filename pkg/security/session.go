package security

import (
	"net/http"
)

type SessionDestroyer interface {
	Destroy(r *http.Request) error
}

type SessionValueGetter interface {
	Get(r *http.Request, name string) interface{}
}

type FlashHandler interface {
	AddFlash(r *http.Request, flashType string, message string)
	GetFlash(r *http.Request, flashType string) string
}

type SessionStore interface {
	SessionDestroyer
	SessionValueGetter
	FlashHandler
	Add(r *http.Request, name string, value interface{})
	Middleware(next http.Handler) http.Handler
}

type SessionUserProvider interface {
	GetUser(r *http.Request) SecurityUser
}
