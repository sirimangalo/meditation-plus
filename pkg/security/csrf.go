package security

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/justinas/nosurf"
)

var CsrfTag string = "csrfField"

func CsrfField(r *http.Request) template.HTML {
	return template.HTML(fmt.Sprintf(`<input type="hidden" name="csrf_token" value="%s">`, nosurf.Token(r)))
}
