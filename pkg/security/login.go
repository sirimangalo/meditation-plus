package security

import (
	stdErrors "errors"
	"log/slog"
	"meditation-plus/pkg/errors"
	"net/http"
)

type SecurityUser interface {
	GetId() int
	IsGranted(role string) bool
}

type UserProvider interface {
	ProvideUserByIdentifier(identifier string) (SecurityUser, error)
}

type UserCreator interface {
	CreateUser(userData map[string]interface{}) (SecurityUser, error)
	UpdateUser(user SecurityUser, userData map[string]interface{}) error
}

type Authenticator interface {
	Authenticate(w http.ResponseWriter, r *http.Request) (SecurityUser, error)
}

type TestAuthenticator struct {
	userProvider UserProvider
}

func NewTestAuthenticator(userProvider UserProvider) *TestAuthenticator {
	return &TestAuthenticator{
		userProvider: userProvider,
	}
}

func (t *TestAuthenticator) Authenticate(w http.ResponseWriter, r *http.Request) (SecurityUser, error) {
	user, err := t.userProvider.ProvideUserByIdentifier(r.URL.Query().Get("user"))

	if err != nil {
		return nil, err
	}

	return user, nil
}

type LoginRequest struct {
	Username string
	Password string
}

func HandleAuth(
	auth Authenticator,
	session SessionStore,
) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, err := auth.Authenticate(w, r)

		if err != nil {
			slog.WarnContext(r.Context(), "Could not authenticate user.", "error", err)

			if stdErrors.Is(err, ErrUserDataInvalid) {
				session.AddFlash(r, "error", "The user data received from Discord is incomplete. Please make sure that you've added an email address to your Discord account.")
			} else if stdErrors.Is(err, errors.ErrUserNotInGuild) {
				session.AddFlash(r, "error", "You need to join the Sirimangalo Discord server. Please click on the link below.")
			} else {
				session.AddFlash(r, "error", "Could not authenticate with Discord. Please try again later.")
			}

			http.Redirect(w, r, "/login/", http.StatusSeeOther)
			return
		}

		login(w, r, user, session)
	})
}

func login(
	w http.ResponseWriter,
	r *http.Request,
	user SecurityUser,
	session SessionStore,
) {
	if user == nil {
		return
	}

	session.Add(r, "user", user)

	requestedPage, ok := session.Get(r, SessionKeyPageRequested).(string)

	if ok && requestedPage != "" {
		http.Redirect(w, r, requestedPage, http.StatusSeeOther)
		return
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)
}
