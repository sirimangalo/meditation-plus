package security

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	stdErrors "errors"
	"fmt"
	"io"
	"log/slog"
	"meditation-plus/pkg/errors"
	"net/http"

	"golang.org/x/oauth2"
)

var ErrUserDataInvalid = stdErrors.New("user data is inavlid")
var ErrCouldNotLogin = stdErrors.New("could not create or login user")

type OAuth2Authenticator struct {
	session            SessionStore
	errorRenderer      errors.ErrorRenderer
	oauthConfig        *oauth2.Config
	userProvider       UserProvider
	userCreator        UserCreator
	exchangeHandler    OAuth2ExchangeHandler
	userInfoGetter     OAuth2UserInfoGetter
	accessTokenChecker OAuth2AccessTokenChecker
	shouldCreateUser   bool
	userEndpoint       string
	loginUrl           string
}

type OAuth2ExchangeHandler interface {
	Exchange(oauthConfig *oauth2.Config, token string) (*oauth2.Token, error)
}

type OAuth2UserInfoGetter interface {
	Get(oauthConfig *oauth2.Config, token *oauth2.Token, endpoint string) (resp *http.Response, err error)
}

type OAuth2AccessTokenChecker interface {
	CheckAccessToken(accessToken *oauth2.Token, userInfo map[string]interface{}) error
}

type DefaultOAuth2Endpoint struct{}

func (e *DefaultOAuth2Endpoint) Exchange(oauthConfig *oauth2.Config, code string) (*oauth2.Token, error) {
	return oauthConfig.Exchange(context.Background(), code)
}

func (e *DefaultOAuth2Endpoint) Get(oauthConfig *oauth2.Config, token *oauth2.Token, endpoint string) (resp *http.Response, err error) {
	return oauthConfig.Client(context.Background(), token).Get(endpoint)
}

func NewOAuth2Authenticator(
	session SessionStore,
	errorRenderer errors.ErrorRenderer,
	oauthConfig *oauth2.Config,
	userProvider UserProvider,
	userCreator UserCreator,
	accessTokenChecker OAuth2AccessTokenChecker,
	userEndpoint string,
	loginUrl string,
) *OAuth2Authenticator {
	defaultEndpointImplementation := &DefaultOAuth2Endpoint{}
	return &OAuth2Authenticator{
		session:            session,
		errorRenderer:      errorRenderer,
		oauthConfig:        oauthConfig,
		userProvider:       userProvider,
		userCreator:        userCreator,
		userInfoGetter:     defaultEndpointImplementation,
		exchangeHandler:    defaultEndpointImplementation,
		accessTokenChecker: accessTokenChecker,
		shouldCreateUser:   true,
		userEndpoint:       userEndpoint,
		loginUrl:           loginUrl,
	}
}

func (a *OAuth2Authenticator) Authenticate(w http.ResponseWriter, r *http.Request) (SecurityUser, error) {
	if r.FormValue("code") == "" {
		a.init(w, r)
		return nil, nil
	}

	accessToken, userInfo := a.getAccessToken(w, r)

	err := a.accessTokenChecker.CheckAccessToken(accessToken, userInfo)

	if err != nil {
		return nil, err
	}

	user, err := a.getUser(userInfo, w, r)
	if err != nil {
		return nil, fmt.Errorf("could not get user from access token: %v", err)
	}

	if user != nil {
		return user, nil
	}

	return a.createUser(userInfo, w, r)
}

func (a *OAuth2Authenticator) init(w http.ResponseWriter, r *http.Request) {
	bytes, err := generateRandomBytes(32)
	state := base64.RawStdEncoding.EncodeToString(bytes)

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not generate random bytes", "error", err)
		a.errorRenderer.RenderError(http.StatusInternalServerError, w, r)
		return
	}

	a.session.Add(r, "oauthState", state)

	http.Redirect(w, r, a.oauthConfig.AuthCodeURL(state), http.StatusTemporaryRedirect)
}

func (a *OAuth2Authenticator) getAccessToken(w http.ResponseWriter, r *http.Request) (*oauth2.Token, map[string]interface{}) {
	state := a.session.Get(r, "oauthState")

	if r.FormValue("state") != state {
		slog.ErrorContext(r.Context(), "OAuth2 State does not match", "state", r.FormValue("state"), "oauthState", state)
		a.errorRenderer.RenderError(http.StatusInternalServerError, w, r)
		return nil, nil
	}

	token, err := a.exchangeHandler.Exchange(a.oauthConfig, r.FormValue("code"))

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not exchange", "error", err, "code", r.FormValue("code"))
		a.errorRenderer.RenderError(http.StatusBadRequest, w, r)
		return nil, nil
	}

	res, err := a.userInfoGetter.Get(a.oauthConfig, token, a.userEndpoint)

	if err != nil || res == nil || res.StatusCode != 200 {
		slog.ErrorContext(r.Context(), "Could not read profile", "error", err)
		a.errorRenderer.RenderError(http.StatusInternalServerError, w, r)
		return nil, nil
	}

	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)

	if err != nil {
		slog.ErrorContext(r.Context(), "Could not read response", "error", err)
		a.errorRenderer.RenderError(http.StatusInternalServerError, w, r)
		return nil, nil
	}

	var data map[string]interface{}
	if err := json.Unmarshal(body, &data); err != nil {
		slog.ErrorContext(r.Context(), "Could not unmarshal response", "error", err)
		a.errorRenderer.RenderError(http.StatusInternalServerError, w, r)
	}

	return token, data
}

func (a *OAuth2Authenticator) getUser(userData map[string]interface{}, w http.ResponseWriter, r *http.Request) (SecurityUser, error) {
	identifier, validIdentifier := userData["id"].(string)

	if !validIdentifier {
		return nil, fmt.Errorf("received invalid identifier: %v", userData["id"])
	}
	user, err := a.userProvider.ProvideUserByIdentifier(identifier)

	if err != nil {
		return nil, fmt.Errorf("could not read user by identifier: %v", err)
	}

	if user != nil {
		slog.InfoContext(r.Context(), "Found existing user. Updating and returing", "user", user.GetId())
		err := a.userCreator.UpdateUser(user, userData)

		if err != nil {
			return nil, fmt.Errorf("could not update user: %v", err)
		}

		return user, nil
	}

	return nil, nil
}

func (a *OAuth2Authenticator) createUser(userData map[string]interface{}, w http.ResponseWriter, r *http.Request) (SecurityUser, error) {
	user, err := a.userCreator.CreateUser(userData)

	if err == ErrUserDataInvalid {
		return nil, ErrUserDataInvalid
	}

	if err != nil {
		return nil, fmt.Errorf("could not create user: %v", err)
	}

	if user != nil {
		slog.InfoContext(r.Context(), "Created user", "user", user.GetId())
		return user, nil
	}

	return nil, ErrCouldNotLogin
}

func generateRandomBytes(n uint32) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}
