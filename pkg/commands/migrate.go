package commands

import (
	"database/sql"
	"fmt"
	"log/slog"
	"os/exec"
	"time"
)

func Migrate(atlasPath string, dbDriver string, dbDsn string, dbDsnAtlas string) {
	err := waitForDatabase(dbDriver, dbDsn, 10, 5*time.Second)
	if err != nil {
		return
	}

	dsn := fmt.Sprintf("%s://%s", dbDriver, dbDsnAtlas)
	cmd, err := exec.Command(atlasPath, "migrate", "apply", "--url", dsn, "--dir", "file://migrations").CombinedOutput()
	if err != nil {
		slog.Error("Error while running migration", "error", err, "output", string(cmd))
		return
	}

	slog.Info("Migrations completed", "output", string(cmd))
}

func waitForDatabase(dbDriver string, dbDsn string, maxRetries int, delay time.Duration) error {
	for i := 0; i < maxRetries; i++ {
		slog.Info("Trying to connect to database", "try", i+1, "maxTries", maxRetries)
		db, err := sql.Open(dbDriver, dbDsn)
		if err != nil {
			slog.Error("Error while opening the database connection", "error", err)
			time.Sleep(delay)
			continue
		}
		defer db.Close()

		err = db.Ping()
		if err == nil {
			slog.Info("Successfully connected to database", "try", i+1, "maxTries", maxRetries)
			return nil
		}

		slog.Error("Database not reachable", "try", i+1, "error", err)
		time.Sleep(delay)
	}
	return fmt.Errorf("Database not reachable after %d tries", maxRetries)
}
