package commands

import (
	"context"
	"database/sql"
	"log/slog"
	"meditation-plus/pkg/appointment"
	"meditation-plus/pkg/mail"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/user"
	"time"
)

func GenerateNotifications(
	now time.Time,
	findAllTeacherQuery user.TeacherQuery,
	findAppointmentQuery appointment.TeacherQuery,
) ([]mail.TemplateScheduleUpdate, error) {
	teachers, err := findAllTeacherQuery.FindTeacher(context.Background())
	if err != nil {
		return nil, err
	}

	notifications := []mail.TemplateScheduleUpdate{}
	for _, teacher := range teachers {
		appointments, err := findAppointmentQuery.FindByTeacher(context.Background(), int(teacher.Id))
		if err != nil {
			return nil, err
		}

		newAppointments := []model.Appointment{}
		canceledAppointments := []model.Appointment{}

		for _, appointWithDetails := range appointments {
			appoint := appointWithDetails.Appointment
			if appoint.UserId.Valid && !appoint.NotifiedAt.Valid {
				// user has booked the appointment and there wasn't a notification yet
				newAppointments = append(newAppointments, appoint)
				continue
			}

			if !appoint.UserId.Valid && appoint.NotifiedAt.Valid {
				// user has unbooked the appointment and there wasn't an update notification yet
				canceledAppointments = append(canceledAppointments, appoint)
				continue
			}
		}

		if len(newAppointments) == 0 && len(canceledAppointments) == 0 {
			continue
		}

		notifications = append(notifications, mail.TemplateScheduleUpdate{
			RecipientUser:        &teacher,
			NewAppointments:      newAppointments,
			CanceledAppointments: canceledAppointments,
			Now:                  now,
		})
	}

	return notifications, nil
}

// This function is meant to be run by a cronjob with a suitable interval. Using a cronjob instead of
// sending one email directly when booking/canceling an appointment is meant to reduce the amount of
// emails sent by bundling all changes between two cronjob runs. The function collects for each teacher
// the appointments with a newly registered user or canceled ones since the last time this function was
// called. The 'notified_at' field in the appointment schema is used to determine which ones are new.
// Canceling appointments should leave this field unchanged (except the person canceling would be the one
// receiving the email). The combination of (no user, non-empty notified_at field) marks a canceled appointment.
// Appointments with an empty user and empty notified_at field generate no notifications, so booking and unbooking
// between two cronjob runs does not generate a notification.
func SendAppointmentNotifications(
	now time.Time,
	findAllTeacherQuery user.TeacherQuery,
	findAppointmentQuery appointment.TeacherQuery,
	appointmentNotifiedQuery appointment.NotifiedQuery,
	mailer mail.Mailer,
) {
	slog.Info("Checking for new notifications...")

	notifications, err := GenerateNotifications(now, findAllTeacherQuery, findAppointmentQuery)
	if err != nil {
		slog.Error("Could generate notifications", "error", err)
		panic(err)
	}

	if len(notifications) == 0 {
		slog.Info("Nothing new. Exiting.")
		return
	}

	for _, notification := range notifications {
		slog.Info(
			"Sending email",
			"user", notification.RecipientUser.Id,
			"new_appointments", len(notification.NewAppointments),
			"canceled_appointments", len(notification.CanceledAppointments),
		)

		err := mail.SendScheduleUpdateMail(notification.RecipientUser.Email, notification, mailer)
		if err != nil {
			slog.Error("Could not send notification", "user", notification.RecipientUser.Id, "error", err)

			// the email is probably not sent, so it shouldn't be tracked as a sent notification
			continue
		}

		for _, a := range notification.NewAppointments {
			err := appointmentNotifiedQuery.Notified(context.Background(), &a, sql.NullTime{
				Valid: true,
				Time:  now,
			})
			if err != nil {
				slog.Error("Failed to update appointment model for newly registered ones", "error", err)
				// panic here because the problem might affect the next user
				// not updating the db means duplicate emails will be sent
				panic(err)
			}
		}

		for _, a := range notification.CanceledAppointments {
			err := appointmentNotifiedQuery.Notified(context.Background(), &a, sql.NullTime{
				Valid: false,
			})
			if err != nil {
				slog.Error("Failed to update appointment model for canceled ones", "error", err)
				panic(err)
			}
		}
	}

	slog.Info("Done.")
}
