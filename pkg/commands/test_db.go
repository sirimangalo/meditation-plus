package commands

import (
	"meditation-plus/pkg/database"
	"os"
	"strings"
	"time"
)

func TestDb(dbDriver string, dbDsn string, replaceDsn bool) {
	db := os.Getenv("TEST_DATABASE")
	if replaceDsn {
		dbDsn = strings.ReplaceAll(dbDsn, db, "")
	}
	err := waitForDatabase(dbDriver, dbDsn, 10, 5*time.Second)
	if err != nil {
		return
	}

	database.SetUpTestDb(dbDriver, dbDsn)
}
