// This command is for the manual import of the exported mongodb collection
// from the old app. It should be executed locally and manually imported
// to the new app.

package commands

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"meditation-plus/pkg/database"
	"meditation-plus/pkg/model"
	"os"
	"time"
)

const importPath = "/tmp/meditations.json"

type Record struct {
	Sitting   int       `json:"sitting"`
	Walking   int       `json:"walking"`
	CreatedAt DateField `json:"createdAt"`
	End       DateField `json:"end"`
	User      IDField   `json:"user"`
}

type IDField struct {
	Oid string `json:"$oid"`
}

type DateField struct {
	Date time.Time `json:"$date"`
}

func (df *DateField) UnmarshalJSON(data []byte) error {
	var tmp struct {
		Date string `json:"$date"`
	}
	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}
	parsedTime, err := time.Parse(time.RFC3339, tmp.Date)
	if err != nil {
		return err
	}
	df.Date = parsedTime
	return nil
}

func Convert() {
	queries := model.New(database.Pool)
	cnt := 0

	file, err := os.Open(importPath)
	if err != nil {
		fmt.Println("Error opening JSON file:", err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	tx, err := database.Pool.Begin()
	if err != nil {
		fmt.Printf("Transaction error: %v\n", err)
	}

	defer func() {
		err := tx.Rollback()
		if err != nil {
			fmt.Println("Could not Rollback transaction:", err)
		}
	}()

	queries = queries.WithTx(tx)
	for scanner.Scan() {
		var record Record
		line := scanner.Text()

		err := json.Unmarshal([]byte(line), &record)
		if err != nil {
			fmt.Println("Error parsing JSON line:", err)
			continue
		}

		startTime := record.CreatedAt.Date
		endTime := record.End.Date
		timezone := record.CreatedAt.Date.Location().String()

		res, err := queries.CreateMigratedMeditation(context.Background(), model.CreateMigratedMeditationParams{
			Walking:  int32(record.Walking),
			Sitting:  int32(record.Sitting),
			Start:    startTime,
			End:      endTime,
			Timezone: timezone,
			UserId:   record.User.Oid,
		})

		if res == nil || err != nil {
			fmt.Printf("Transaction error: %v", err)
		}

		cnt++

		if cnt%1000 == 0 {
			err = tx.Commit()
			if err != nil {
				fmt.Printf("Transaction error: %v\n", err)
			}

			tx, err = database.Pool.Begin()
			if err != nil {
				fmt.Printf("Transaction error: %v\n", err)
			}

			defer func() {
				err := tx.Rollback()
				if err != nil {
					fmt.Println("Could not Rollback transaction:", err)
				}
			}()

			queries = queries.WithTx(tx)
		}
	}

	err = tx.Commit()
	if err != nil {
		fmt.Printf("Transaction error: %v\n", err)
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
	}

	fmt.Println("%i entries have been processed.", cnt)
}
