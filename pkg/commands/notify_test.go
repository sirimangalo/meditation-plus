package commands

import (
	"database/sql"
	"meditation-plus/pkg/appointment"
	"meditation-plus/pkg/mail"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/user"
	"testing"
	"time"
)

func TestNoNotifications(t *testing.T) {
	userA := user.NewFakeUser(1)
	userB := user.NewFakeUser(2)
	teacherA := user.NewFakeUserWithRole(3, model.USER_ROLES_TEACHER)
	teacherA.DisplayName = "Bob"

	now, _ := time.Parse(time.RFC3339, "2024-07-02T10:00:00Z")

	notifiedAt1, _ := time.Parse(time.RFC3339, "2024-07-01T10:00:00Z")
	appointmentAlreadyNotified := &model.FindAppointmentByTeacherRow{
		Appointment: model.Appointment{
			UserId:    sql.NullInt64{Int64: userA.Id, Valid: true},
			TeacherId: teacherA.Id,
			Time:      "13:00:00",
			Weekday:   0,
			NotifiedAt: sql.NullTime{
				Valid: true,
				Time:  notifiedAt1,
			},
		},
	}
	appointmentUnnotified := &model.FindAppointmentByTeacherRow{
		Appointment: model.Appointment{
			UserId:    sql.NullInt64{Int64: userB.Id, Valid: true},
			TeacherId: teacherA.Id,
			Time:      "13:00:00",
			Weekday:   1,
			NotifiedAt: sql.NullTime{
				Valid: false,
			},
		},
	}
	appointmentCanceled := &model.FindAppointmentByTeacherRow{
		Appointment: model.Appointment{
			TeacherId: teacherA.Id,
			Time:      "13:00:00",
			Weekday:   2,
			NotifiedAt: sql.NullTime{
				Valid: true,
				Time:  notifiedAt1,
			},
		},
	}
	freeAppointment := &model.FindAppointmentByTeacherRow{
		Appointment: model.Appointment{},
	}

	testCases := map[string]struct {
		Appointments []model.FindAppointmentByTeacherRow
		Teachers     []model.User
		ExpectedBody string
	}{
		"should include unnotified appointment": {Teachers: []model.User{*teacherA},
			Appointments: []model.FindAppointmentByTeacherRow{
				*appointmentAlreadyNotified,
				*appointmentUnnotified,
				*freeAppointment,
			},
			ExpectedBody: "Hello Bob,\n\nthere are new updates in your appointment schedule:\n\nNew booked appointments:\n  - Monday, 13:00 (next: 08 Jul)\n\n",
		},
		"should include canceled appointment": {Teachers: []model.User{*teacherA},
			Appointments: []model.FindAppointmentByTeacherRow{
				*appointmentAlreadyNotified,
				*appointmentCanceled,
				*appointmentUnnotified,
				*freeAppointment,
			},
			ExpectedBody: "Hello Bob,\n\nthere are new updates in your appointment schedule:\n\nNew booked appointments:\n  - Monday, 13:00 (next: 08 Jul)\n\nCanceled appointments:\n  - Tuesday, 13:00\n",
		},
	}

	for index, testCase := range testCases {
		appointmentRepository := &appointment.FakeRepository{AppointmentByTeacher: testCase.Appointments}
		userRepository := &user.FakeRepository{Teacher: testCase.Teachers}
		mailer := &mail.FakeMailer{SentEmails: []*mail.FakeSentEmail{}}

		SendAppointmentNotifications(
			now,
			userRepository,
			appointmentRepository,
			appointmentRepository,
			mailer,
		)

		if len(mailer.SentEmails) > 1 {
			t.Fatalf("More than one email sent for test case '%s'", index)
		}

		if mailer.SentEmails[0].Body != testCase.ExpectedBody {
			t.Fatalf("Expected '%s' as email body but got '%s' for test case '%s'", testCase.ExpectedBody, mailer.SentEmails[0].Body, index)
		}
	}
}
