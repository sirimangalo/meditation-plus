package commands

import (
	"flag"
	"meditation-plus/pkg/appointment"
	"meditation-plus/pkg/mail"
	"meditation-plus/pkg/user"
	"time"
)

func RunCommands(mailer mail.Mailer, atlasPath string, dbDriver string, dbDsn string, dbDsnAtlas string) bool {
	migrateCmd := flag.Bool("migrate", false, "Perform database migrations")
	withMigrateCmd := flag.Bool("with-migrate", false, "Perform database migrations, but also start the app")
	createTestDbCmd := flag.Bool("create-test-db", false, "Create test database, load schema and fixtures")
	withTestDbCmd := flag.Bool("with-test-db", false, "Create test database, load schema, fixtures and start the app")
	sendAppointmentNotifications := flag.Bool("send-notifications", false, "Generate and send email notifications about schedule updates to teachers")
	convertFlag := flag.Bool("convert", false, "Import /tmp/meditations.json MongoDB dump from the old app into the database")

	flag.Parse()

	if *sendAppointmentNotifications {
		userRepository := user.NewRepository()
		appointmentRepository := appointment.NewRepository()
		SendAppointmentNotifications(time.Now(), userRepository, appointmentRepository, appointmentRepository, mailer)
		return true
	}

	if *convertFlag {
		Convert()
		return true
	}

	if *migrateCmd {
		Migrate(atlasPath, dbDriver, dbDsn, dbDsnAtlas)
		return true
	}

	if *withMigrateCmd {
		Migrate(atlasPath, dbDriver, dbDsn, dbDsnAtlas)
		return false
	}

	if *createTestDbCmd {
		TestDb(dbDriver, dbDsn, true)
		return true
	}

	if *withTestDbCmd {
		TestDb(dbDriver, dbDsn, false)
		return false
	}

	return false
}
