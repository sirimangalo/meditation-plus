module meditation-plus

go 1.23.0

toolchain go1.24.1

require (
	github.com/alexedwards/scs/mysqlstore v0.0.0-20250212122300-421ef1d8611c
	github.com/alexedwards/scs/v2 v2.8.0
	github.com/go-sql-driver/mysql v1.9.0
	github.com/justinas/nosurf v1.1.1
	golang.org/x/net v0.36.0
	golang.org/x/oauth2 v0.27.0
)

require filippo.io/edwards25519 v1.1.0 // indirect
