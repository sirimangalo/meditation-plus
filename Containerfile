ARG BUILD_REF_NAME="dev"
ARG BUILD_REF="dev"

FROM docker.io/library/node:23.9-alpine AS frontend-builder
WORKDIR /usr/app
COPY . .
RUN corepack enable
RUN pnpm i --frozen-lockfile
RUN pnpm build

FROM docker.io/library/golang:1.24.1-alpine AS builder

RUN mkdir -m 777 /scratchtmp

ARG BUILD_REF_NAME
ARG BUILD_REF

WORKDIR $GOPATH/src/app/
COPY . .
RUN apk update && apk add --no-cache make tzdata
COPY --from=frontend-builder /usr/app/web/static/ web/static/
RUN GOOS=linux GOARCH=amd64 make build

FROM scratch
COPY --from=builder /scratchtmp /tmp
COPY --from=builder /go/src/app/bin/app /go/bin/os
COPY --from=docker.io/arigaio/atlas:0.31.0 /atlas /go/bin/atlas
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/share/zoneinfo/ /usr/share/zoneinfo/
COPY --from=builder /go/src/app/migrations /migrations
ENV LISTEN_HOST=0.0.0.0
ENV PORT=8080
ENTRYPOINT ["/go/bin/os", "--with-migrate"]
EXPOSE 8080
