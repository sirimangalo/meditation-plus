all: test lint build

run:
	env $$(cat .env $$(if test -f .env.local; then echo .env.local; fi) | xargs) go run .

run-e2e:
	env $$(cat .env.test $$(if test -f .env.test.local; then echo .env.test.local; fi) | xargs) go run . --with-test-db

watch:
	env $$(cat .env $$(if test -f .env.local; then echo .env.local; fi) | xargs) gow -e=go,mod,html run .

migrate:
	env $$(cat .env $$(if test -f .env.local; then echo .env.local; fi) | xargs) go run . --migrate

convert:
	env $$(cat .env $$(if test -f .env.local; then echo .env.local; fi) | xargs) go run . --convert

with-migrate:
	env $$(cat .env $$(if test -f .env.local; then echo .env.local; fi) | xargs) go run . --with-migrate

send-notifications:
	env $$(cat .env $$(if test -f .env.local; then echo .env.local; fi) | xargs) go run . --send-notifications

unit-tests:
	go test ./... -skip ^TestRepository*

tests:
	env $$(cat .env.test $$(if test -f .env.test.local; then echo .env.test.local; fi) | xargs) go run . --create-test-db && env $$(cat .env.test $$(if test -f .env.test.local; then echo .env.test.local; fi) | xargs) go test ./...

lint: golangci-lint fmt

golangci-lint:
	golangci-lint run

fmt:
	@files=$$(gofmt -s -l .); \
  if [ -n "$$files" ]; then \
    gofmt -d .; \
    exit 1; \
  fi

audit:
	govulncheck ./...

build:
	go get -v ./...
	APP_ENV=prod go build -ldflags="-w -s -X 'main.Version=${BUILD_REF_NAME}' -X 'main.GitHash=${BUILD_REF}'" -o bin/app main.go
