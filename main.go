package main

import (
	"encoding/gob"
	"fmt"
	"log"
	"log/slog"
	"meditation-plus/pkg/appointment"
	"meditation-plus/pkg/banner"
	"meditation-plus/pkg/commands"
	"meditation-plus/pkg/database"
	"meditation-plus/pkg/logger"
	"meditation-plus/pkg/mail"
	"meditation-plus/pkg/meditation"
	"meditation-plus/pkg/model"
	"meditation-plus/pkg/routing"
	"meditation-plus/pkg/security"
	"meditation-plus/pkg/template"
	"meditation-plus/pkg/timezone"
	"meditation-plus/pkg/user"
	"net/http"
	"os"

	"golang.org/x/oauth2"
)

// Will be replaced via ldflags
var (
	Version string
	GitHash string
)

func main() {
	if Version == "" {
		Version = "Dev"
		GitHash = "Local"
	}

	appEnv := os.Getenv("APP_ENV")
	atlasPath := os.Getenv("ATLAS_PATH")
	port := os.Getenv("PORT")
	listenHost := os.Getenv("LISTEN_HOST")
	dbDriver := os.Getenv("DATABASE_DRIVER")
	dbDsn := os.Getenv("DATABASE_DSN")
	dbDsnAtlas := os.Getenv("DATABASE_DSN_ATLAS")
	clientId := os.Getenv("OAUTH_CLIENT_ID")
	clientSecret := os.Getenv("OAUTH_CLIENT_SECRET")
	publicUrl := os.Getenv("PUBLIC_URL")
	zoneinfoPath := os.Getenv("ZONEINFO_PATH")
	smtpUser := os.Getenv("SMTP_USER")
	smtpPassword := os.Getenv("SMTP_PASSWORD")
	smtpHost := os.Getenv("SMTP_HOST")
	smtpPort := os.Getenv("SMTP_PORT")
	smtpStartTLS := os.Getenv("SMTP_STARTTLS") == "true"
	discordGuildId := os.Getenv("DISCORD_GUILD_ID")

	setDefaultLogger(appEnv)

	database.InitDb(dbDriver, dbDsn)
	defer database.Pool.Close()

	mailer := mail.NewSmtpMailer(smtpUser, smtpPassword, smtpHost, smtpPort, smtpStartTLS)

	if commands.RunCommands(mailer, atlasPath, dbDriver, dbDsn, dbDsnAtlas) {
		return
	}

	session := security.NewScsSessionStore(appEnv, database.Pool)

	timezoneService := &timezone.Service{ZoneInfoPath: zoneinfoPath}
	userRepository := user.NewRepository()
	userService := &user.Service{}
	appointmentRepository := appointment.NewRepository()
	appointmentService := &appointment.Service{}
	meditationRepository := meditation.NewRepository()
	meditationService := &meditation.Service{}
	bannerService := &banner.Service{}

	sessionUserProvider := user.NewDatabaseSessionUserProvider(userRepository, userRepository, session)

	templating := template.NewTemplating(appEnv, session)
	globalData := map[string]interface{}{
		"publicUrl":  publicUrl,
		"appVersion": fmt.Sprintf("%s (%s)", Version, GitHash),
	}
	templating.AddGlobalData(globalData)

	oauthConfig := &oauth2.Config{
		Endpoint:     security.DiscordEndpoint,
		Scopes:       []string{security.ScopeIdentify, security.ScopeEmail, security.ScopeGuilds},
		RedirectURL:  publicUrl + "/auth/",
		ClientID:     clientId,
		ClientSecret: clientSecret,
	}
	userEndpoint := "https://discord.com/api/users/@me"
	accessTokenChecker := user.NewDiscordAccessTokenChecker(discordGuildId)
	auth := security.NewOAuth2Authenticator(
		session,
		templating,
		oauthConfig,
		userRepository,
		userRepository,
		accessTokenChecker,
		userEndpoint,
		"/login",
	)
	testAuth := security.NewTestAuthenticator(userRepository)

	imageGetter := &user.HttpDiscordImageGetter{}

	gob.Register(&model.User{})

	routes := routing.Routes(
		session,
		session,
		sessionUserProvider,
		templating,
		userRepository,
		userService,
		appointmentRepository,
		appointmentService,
		meditationRepository,
		meditationService,
		bannerService,
		timezoneService,
		auth,
		testAuth,
		imageGetter,
		appEnv,
	)

	if listenHost == "" {
		listenHost = "127.0.0.1"
	}

	listenAddr := fmt.Sprintf("%s:%s", listenHost, port)
	slog.Info(fmt.Sprintf("Server is listening for requests on %s", listenAddr))
	slog.Info(fmt.Sprintf("The configured public url is: %s", publicUrl))

	log.Fatal(http.ListenAndServe(listenAddr, session.Middleware(routes)))
}

func setDefaultLogger(appEnv string) {
	if appEnv == "prod" {
		handler := &logger.ContextHandler{Handler: slog.NewJSONHandler(os.Stdout, nil)}
		clogger := slog.New(handler)
		clogger = clogger.With("version", Version)
		slog.SetDefault(clogger)
		return
	}

	handler := &logger.ContextHandler{Handler: slog.NewTextHandler(os.Stdout, nil)}
	clogger := slog.New(handler)
	slog.SetDefault(clogger)
}
