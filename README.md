<div align="center">
![Logo](https://gitlab.com/sirimangalo/meditation-plus/-/raw/main/assets/img/logo.svg)

<h1>Meditation+</h1>
</div>

**NEW CONTRIBUTORS ARE WELCOME !** This project is currently refactoring using [Golang](https://go.dev/). If you're interested in helping, please checkout [the contributing guide](https://gitlab.com/sirimangalo/meditation-plus/-/blob/master/CONTRIBUTING.md) for getting into contact.

## Development Requirements

The following tools need to be installed:
- [pnpm](https://pnpm.io/installation)
- [go](https://go.dev/doc/install) (v1.23+)
- [atlas](https://atlasgo.io) (Database migration tool)
- [sqlc](https://sqlc.dev) (When database changes are required)

## Getting Started
- Install npm dependencies: `pnpm i --frozen-lockfile`
- Build css/js: `pnpm build` (`pnpm build --watch` to watch for changes)
- `cp .env .env.local` and edit `.env.local` to match you local environment.
- Run a local database instance (`docker-compose up -d db`)
- `make with-migrate`
- Navigate your browser to http://127.0.0.1:8080

### Setting up Login with Discord

To setup login with Discord, go to Discord's [developer portal](https://discord.com/developers/applications) and create a new test application. In the "OAuth2" settings, copy the client ID and client secret to `.env.local` and add "http://127.0.0.1:8080/auth" as a redirect URL.

## Make Commands
- `make run`: Builds and starts the app.
- `make watch`: Builds and starts the app - restarts on file changes (requires [`gow`](https://github.com/mitranim/gow) to be in `$PATH`).
- `make tests`: Runs the unit tests.
- `make build`: Builds a production release of the app.
- `make migrate`: This executes the database migrations which aren't executed on the current database.
- `make with-migrate`: This executes the database migrations, but also starts the app.
- `make run-e2e`: Runs the application in the test env with `--with-test-db` to initialize the test database.

## Database Fixtures

Load the `etc/fixtures.sql` with the database tool of your choice:

```
mysql meditation < etc/fixtures.sql
```

## Database changes

Change the file `db/schema.sql` to the final database schema. Run `sqlc generate` to update generated code and then run the following command to generate migration files:
```
atlas migrate diff name_of_the_migration \
  --dir "file://migrations" \
  --to "file://db/schema.sql" \
  --dev-url "EMTPY_DATABASE_DSN"
```

`atlas` needs an empty database to validate it's changes. After that, update your local database with `make migrate`.
