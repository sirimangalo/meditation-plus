import { expect, test } from "@playwright/test";
import { userFile } from "./const";

test.describe(() => {
	test.use({ storageState: userFile });

	test("should show dialog to cancel appointment", async ({ page }) => {
		await page.goto("/appointments");
		await page.getByRole("button", { name: "Cancel" }).click();
		await expect(page.locator("dialog")).toBeVisible();
		await page.getByRole("button", { name: "No, cancel." }).click();
		await expect(page.locator("dialog")).not.toBeVisible();
		await expect(page.getByRole("button", { name: "Cancel" })).toBeVisible();
	});

	test("should cancel appointment", async ({ page }) => {
		await page.goto("/appointments");
		await page.getByRole("button", { name: "Cancel" }).click();
		await expect(page.locator("dialog")).toBeVisible();
		await page.getByRole("button", { name: "Yes, continue." }).click();
		await expect(page.getByRole("alert")).toContainText(
			"You have successfully",
		);
	});
});
