import { expect, test } from "@playwright/test";
import { userFile } from "./const";

test.describe(() => {
	test.use({ storageState: { cookies: [], origins: [] } });
	test("has title", async ({ page }) => {
		await page.goto("/");
		await expect(page).toHaveTitle(/Login/);
	});
});

test.describe(() => {
	test.use({ storageState: userFile });

	test("should use test authenticator", async ({ page }) => {
		await page.goto("/");
		await expect(page.locator("h3")).toContainText("Recent Meditations");
	});
});
