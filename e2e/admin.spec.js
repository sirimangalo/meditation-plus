import { expect, test } from "@playwright/test";
import { adminFile, teacherFile } from "./const";

test.describe(() => {
	test.use({ storageState: adminFile });

	test("should show admin section", async ({ page }) => {
		await page.goto("/admin/");
		await expect(page.getByRole("link", { name: "Users" })).toBeVisible();
	});
});

test.describe(() => {
	test.use({ storageState: teacherFile });

	test("should show teacher section", async ({ page }) => {
		await page.goto("/admin/");
		await expect(page.getByRole("link", { name: "Users" })).not.toBeVisible();
		await expect(
			page.getByRole("link", { name: "Appointments" }),
		).toBeVisible();
	});
});
