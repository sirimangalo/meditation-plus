import { test as setup } from "@playwright/test";
import { adminFile, teacherFile, userFile } from "./const";

setup("authenticate as admin", async ({ page }) => {
	await page.goto("/auth/test?user=admin");
	await page.waitForURL("/");

	await page.context().storageState({ path: adminFile });
});

setup("authenticate as user", async ({ page }) => {
	await page.goto("/auth/test?user=user");
	await page.waitForURL("/");

	await page.context().storageState({ path: userFile });
});

setup("authenticate as teacher", async ({ page }) => {
	await page.goto("/auth/test?user=teacher");
	await page.waitForURL("/");

	await page.context().storageState({ path: teacherFile });
});
