import { expect, test } from "@playwright/test";
import { userFile } from "./const";

test.describe(() => {
	test.use({ storageState: userFile });

	test("should be visible", async ({ page }) => {
		await page.goto("/profile/user");
		await expect(page).toHaveTitle(/user/);
	});
});
