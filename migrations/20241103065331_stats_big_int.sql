-- Modify "meditation_stats_daily" table
ALTER TABLE `meditation_stats_daily` MODIFY COLUMN `walking_all_time` bigint NOT NULL, MODIFY COLUMN `sitting_all_time` bigint NOT NULL, MODIFY COLUMN `count_all_time` bigint NOT NULL;
