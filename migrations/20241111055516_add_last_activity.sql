-- Modify "users" table
ALTER TABLE `users` ADD COLUMN `last_activity` timestamp NULL, ADD INDEX `activity_idx` (`last_activity`);
