-- Create "migrated_meditations" table
CREATE TABLE `migrated_meditations` (`id` int NOT NULL AUTO_INCREMENT, `walking` int NOT NULL, `sitting` int NOT NULL, `end` timestamp NOT NULL, `start` timestamp NOT NULL, `timezone` varchar(255) NOT NULL, `user_id` varchar(255) NOT NULL, PRIMARY KEY (`id`), INDEX `IDX_USER` (`user_id`)) CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;
