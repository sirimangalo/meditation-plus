-- Create "meditation_stats_daily" table
CREATE TABLE `meditation_stats_daily` (`day` date NOT NULL, `walking` int NOT NULL, `sitting` int NOT NULL, `count` int NOT NULL, `walking_all_time` int NOT NULL, `sitting_all_time` int NOT NULL, `count_all_time` int NOT NULL, PRIMARY KEY (`day`)) CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;
