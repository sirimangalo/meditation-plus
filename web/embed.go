package web

import "embed"

//go:embed static/favicon/*
var Favicon embed.FS

//go:embed static/assets/*
var Content embed.FS

//go:embed static/data/*
var Data embed.FS

//go:embed templates/*
var Templates embed.FS

//go:embed static/assets/manifest.json
var ManifestJson []byte
