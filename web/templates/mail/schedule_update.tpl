Hello {{ .RecipientUser.DisplayName }},

there are new updates in your appointment schedule:
{{ if ne (len .NewAppointments) 0 }}
New booked appointments:
  {{- range .NewAppointments }}
    {{- $nextTime := GetNextAppointmentTime . $.Now }}
    {{- $weekday := DisplayWeekdayInZone $nextTime $.RecipientUser.Timezone.String }}
    {{- $time := DisplayInZone $nextTime $.RecipientUser.Timezone.String "15:04" }}
    {{- $date := DisplayInZone $nextTime $.RecipientUser.Timezone.String "02 Jan" }}
  - {{ $weekday }}, {{ $time }} (next: {{ $date }})
  {{- end }}
{{- end}}
{{ if ne (len .CanceledAppointments) 0 }}
Canceled appointments:
  {{- range .CanceledAppointments }}
    {{- $nextTime := GetNextAppointmentTime . $.Now }}
    {{- $weekday := DisplayWeekdayInZone $nextTime $.RecipientUser.Timezone.String }}
    {{- $time := DisplayInZone $nextTime $.RecipientUser.Timezone.String "15:04" }}
  - {{ $weekday }}, {{ $time }}
  {{- end }}
{{- end }}
