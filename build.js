import { writeFileSync } from "node:fs";
import path from "node:path";
import { context } from "esbuild";

const buildDir = "web/static/assets/";

const ctx = await context({
	entryPoints: [
		"assets/js/main.js",
		"assets/js/components/timer.js",
		"assets/js/pages/appointment.js",
		"assets/js/pages/meditation.js",
		"assets/js/pages/meditation/form.js",
	],
	bundle: true,
	metafile: true,
	minify: true,
	logLevel: "info",
	assetNames: "[name]-[hash]",
	chunkNames: "[name]-[hash]",
	entryNames: "[name]-[hash]",
	outdir: `${buildDir}`,
	loader: {
		".svg": "file",
		".mp3": "file",
	},
	outbase: "",
	plugins: [manifestPlugin()],
});

if (process.argv[2] === "--watch") {
	await ctx.watch();
} else {
	await ctx.rebuild();
	await ctx.dispose();
}

function manifestPlugin() {
	return {
		name: "manifest",
		setup(build) {
			build.onEnd((result) => {
				const pluginOptions = { filename: "manifest.json" };

				if (!result.metafile) {
					throw new Error("Metafile is missing.");
				}

				const outputs = result.metafile.outputs;

				if (!outputs) {
					throw new Error("No outputs found.");
				}

				if (build.initialOptions.outdir === undefined) {
					throw new Error(
						"You must specify an outdir when generating a manifest file.",
					);
				}

				if (build.initialOptions.write === false) {
					throw new Error("Plugin does not support write=false option.");
				}

				const { outdir } = build.initialOptions;

				const fullOutdir = path.resolve(process.cwd(), outdir);

				const manifest = {};

				for (const generatedAsset in outputs) {
					if (outputs[generatedAsset].entryPoint) {
						manifest[outputs[generatedAsset].entryPoint] = {
							js: generatedAsset,
						};
					}
					if (outputs[generatedAsset].cssBundle) {
						manifest[outputs[generatedAsset].entryPoint].css =
							outputs[generatedAsset].cssBundle;
					}

					if (!outputs[generatedAsset].entryPoint) {
						for (const input in outputs[generatedAsset].inputs) {
							if (manifest[input]) {
								manifest[input].out = generatedAsset;
								continue;
							}

							manifest[input] = { out: generatedAsset };
						}
					}
				}

				const fullPath = path.resolve(fullOutdir, pluginOptions.filename);
				const text = JSON.stringify(manifest, null, 4);

				writeFileSync(fullPath, text);
			});
		},
	};
}
