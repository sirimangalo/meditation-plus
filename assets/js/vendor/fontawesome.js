import { config, dom, library } from "@fortawesome/fontawesome-svg-core";
import {
	faBars,
	faChevronRight,
	faClock,
	faCog,
	faHome,
	faPersonWalking,
	faPowerOff,
	faTimes,
	faUser,
	faUsers,
} from "@fortawesome/free-solid-svg-icons";

export function initFontawesome() {
	config.autoAddCss = false;
	config.autoReplaceSvg = "nest";
	config.keepOriginalSource = false;

	library.add([
		faBars,
		faCog,
		faHome,
		faClock,
		faPowerOff,
		faTimes,
		faUser,
		faUsers,
		faChevronRight,
		faPersonWalking,
	]);

	dom.watch();
}
