/** @type {HTMLDivElement} */
const counter = document.getElementById("online-counter");

export function initOnlineCounter() {
	if (counter) {
		updateCounter();
	}
}

async function updateCounter() {
	const resp = await fetch("/online/count");
	const count = await resp.text();
	if (resp.ok) {
		counter.innerText = count;
	}
	window.setTimeout(updateCounter, 10000);
}
