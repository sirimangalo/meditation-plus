const dialogTemplate = `
  <form>
    <h1 class="confirmation-title"></h1>
    <p class="confirmation-text"></p>
    <div>
      <button value="cancel" formmethod="dialog" class="btn btn-outline-primary btn-cancel"></button>
      <button value="default" formmethod="dialog" class="btn btn-primary btn-ok"></button>
    </div>
  </form>
`;

export function initDialogHandler() {
	const submits = document.querySelectorAll(".confirm-submit");

	for (const form of submits) {
		handleConfirmSubmit(form);
	}
}

/** @returns {HTMLDialogElement} */
export function createDialog(title, message, okText, cancelText) {
	const dialog = document.createElement("dialog");
	dialog.innerHTML = dialogTemplate;

	const cancelBtn = dialog.querySelector(".btn-cancel");
	dialog.querySelector(".confirmation-title").textContent = title;
	dialog.querySelector(".confirmation-text").textContent = message;
	dialog.querySelector(".btn-ok").textContent = okText;

	if (cancelText) {
		cancelBtn.textContent = cancelText;
	} else {
		cancelBtn.classList.add("d-none");
	}

	document.body.appendChild(dialog);
	return dialog;
}

/** @param{HTMLFormElement} form */
function handleConfirmSubmit(form) {
	form.addEventListener("submit", (evt) => {
		evt.preventDefault();

		const localTitle = form.getAttribute("data-confirm-title");
		const localMessage = form.getAttribute("data-confirm-message");
		const title = localTitle || "Are you sure?";
		const message =
			localMessage ||
			"Please confirm that you would like to proceed with this action.";
		const dialog = createDialog(
			title,
			message,
			"Yes, continue.",
			"No, cancel.",
		);

		dialog.addEventListener("close", () => {
			if (dialog.returnValue === "cancel" || dialog.returnValue === "") {
				dialog.remove();
				return;
			}

			form.submit();
			dialog.remove();
		});
		dialog.showModal();
	});
}
