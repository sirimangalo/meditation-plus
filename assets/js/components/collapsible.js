export function initCollapsibles() {
	/** @type {HTMLDivElement[]} */
	const collapsibles = document.querySelectorAll(".collapse");

	for (const collapsible of collapsibles) {
		/** @type {HTMLAnchorElement} */
		const collapseToggle = collapsible.querySelector(".collapse-toggle");

		collapseToggle.addEventListener("click", (evt) => {
			evt.preventDefault();
			toggle(
				document.querySelector(collapseToggle.getAttribute("data-target")),
			);
		});

		/** @type {HTMLDivElement} */
		const collapseExcerp = collapsible.querySelector(".collapse-excerp");

		collapseExcerp.addEventListener("click", (evt) => {
			if (["svg", "A", "path"].includes(evt.target.nodeName)) {
				return;
			}
			toggle(collapseExcerp.parentElement);
		});
	}
}

/** @param {HTMLElement} */
function toggle(elem) {
	elem.classList.toggle("collapse-open");
	elem.dispatchEvent(new CustomEvent("collapse-toggle"));
}
