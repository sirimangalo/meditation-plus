const animationDuration = 200;
const backdrop = document.createElement("div");
const menuBtn = document.getElementById("menu-btn");
const closeBtn = document.getElementById("close-btn");
const sidebar = document.getElementById("sidebar");
let show = false;

export function initHeader() {
	if (!menuBtn) {
		return;
	}

	backdrop.classList.add("offcanvas-backdrop", "fade");

	backdrop.addEventListener("click", () => close());
	backdrop.addEventListener("keydown", (evt) => handleBackdropKeydown(evt));
	window.addEventListener("keydown", (evt) => handleBackdropKeydown(evt));

	menuBtn.addEventListener("click", (evt) => open(evt));
	closeBtn.addEventListener("click", () => close());
}

/** @param {KeyboardEvent} evt */
function open(evt) {
	evt.preventDefault();
	show = true;
	menuBtn.parentNode.appendChild(backdrop);
	backdrop.classList.add("show");
	sidebar.classList.add("show");
}

function close() {
	backdrop.classList.remove("show");
	sidebar.classList.remove("show");
	sidebar.classList.add("hiding");
	window.setTimeout(() => {
		show = false;
		backdrop.remove();
		sidebar.classList.remove("hiding");
		sidebar.classList.remove("show");
	}, animationDuration);
}

/** @param {KeyboardEvent} evt */
export function handleBackdropKeydown(evt) {
	if (!show || evt.key !== "Escape") {
		return;
	}
	close();
}
