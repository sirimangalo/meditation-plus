const isAndroid = /Android/i.test(window.navigator.userAgent || "");
const isIos = /iPad|iPhone|iPod/i.test(window.navigator.userAgent || "");
const isDesktop = !isAndroid && !isIos;
const isMobile = isAndroid || isIos;
const hasLocalStorage = typeof window.localStorage !== "undefined";
const hasSessionStorage = typeof window.sessionStorage !== "undefined";

export const Capabilities = {
	isAndroid,
	isIos,
	isDesktop,
	isMobile,
	hasLocalStorage,
	hasSessionStorage,
};
