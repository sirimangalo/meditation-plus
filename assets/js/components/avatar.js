export class UserAvatar {
	/** @type {string} */
	displayName;

	/** @type {string|null} */
	avatarUsername;

	/** @type {string|null} */
	backgroundColor;

	/** @type {string|null} */
	initials;

	/**
	 * @param {string} displayName
	 * @param {string|null} avatarUsername
	 */
	constructor(displayName, avatarUsername) {
		this.displayName = displayName;
		this.avatarUsername = avatarUsername;

		if (!avatarUsername) {
			this.backgroundColor = UserAvatar.getBackgroundColor(displayName);
			this.initials = UserAvatar.getInitials(displayName);
		}
	}

	/**
	 * @param {string} displayName
	 * @returns {string}
	 */
	static getBackgroundColor(displayName) {
		if (displayName === "") {
			return "white";
		}

		let hash = 0;
		const saturation = 30;
		const lightness = 60;

		for (let i = 0; i < displayName.length; i++) {
			hash = displayName.charCodeAt(i) + ((hash << 5) - hash);
		}

		let h = hash % 360;
		h = h < 0 ? h + 360 : h;

		return `hsl(${h},${saturation}%, ${lightness}%)`;
	}

	/**
	 * @param {string} displayName
	 * @returns {string}
	 */
	static getInitials(displayName) {
		return displayName
			.split(" ")
			.filter((str) => str)
			.reduce((result, name, index, arr) => {
				let newResult = result;
				if (index === 0 || index === arr.length - 1) {
					newResult = result.concat(name.charAt(0).toUpperCase());
				}

				return newResult;
			}, "");
	}
}

class UserAvatarComponent extends HTMLElement {
	constructor() {
		super();

		const displayName = this.getAttribute("display-name");
		const avatarUsername = this.getAttribute("avatar-username");
		const size = this.getAttribute("size");
		const userAvatar = new UserAvatar(displayName, avatarUsername);
		const retinaSize = Number.parseInt(size, 10) * 2;
		const textContent = this.textContent;

		const wrapper = document.createElement("div");
		wrapper.classList.add("user-avatar");
		wrapper.classList.add("d-flex");
		wrapper.classList.add("align-items-center");

		if (size) {
			wrapper.classList.add(`user-avatar-${size}`);
		}

		wrapper.style.backgroundColor = userAvatar.backgroundColor;

		if (displayName === "") {
			wrapper.style.border = "1px solid var(--default-avatar-bg)";
			this.textContent = "";
		}

		if (avatarUsername) {
			const img = document.createElement("img");
			img.srcset = `/avatar/${avatarUsername}/${size} 1x, /avatar/${avatarUsername}/${retinaSize} 2x`;
			img.ariaHidden = true;
			img.alt = "";
			wrapper.appendChild(img);
			this.append(wrapper);
			return;
		}

		const initials = document.createElement("div");
		if (!userAvatar.initials) {
			initials.classList.add("user-avatar-initials-empty");
		}
		initials.classList.add("user-avatar-initials");
		initials.textContent = userAvatar.initials || textContent;
		wrapper.appendChild(initials);

		this.append(wrapper);
	}
}

export function initAvatar() {
	customElements.define("user-avatar", UserAvatarComponent);
}
