import { Capabilities } from "./capabilities";

const vars = document.getElementById("gotmpl-vars");
const timer = document.getElementById("timer");
const bell1Path = vars.getAttribute("data-bell1-path");

if (timer) {
	handleMeditation();
}

function handleMeditation() {
	const status = document.querySelector(".status");
	const end = new Date(status.getAttribute("data-end-time"));
	const walking = Number.parseInt(status.getAttribute("data-walking"), 10);
	const sitting = Number.parseInt(status.getAttribute("data-sitting"), 10);
	const endBtn = document.getElementById("endBtn");
	const endEarlyForm = document.getElementById("endEarlyForm");
	const walkingProgress = document.getElementById("walkingProgress");
	const sittingProgress = document.getElementById("sittingProgress");
	const walkingTimeLeft = document.getElementById("walkingTimeLeft");
	const sittingTimeLeft = document.getElementById("sittingTimeLeft");

	let timeLeft = calcLeft();
	let playedSwitchBell = false;

	startTimerOrBell();

	function diff(date1, date2) {
		const diffInMilliseconds = Math.abs(date2 - date1);

		let hours = Math.floor(diffInMilliseconds / (1000 * 60 * 60));
		let minutes = Math.floor(
			(diffInMilliseconds % (1000 * 60 * 60)) / (1000 * 60),
		);
		let seconds = Math.floor((diffInMilliseconds % (1000 * 60)) / 1000);

		hours = format(hours);
		minutes = format(minutes);
		seconds = format(seconds);
		const formattedTime = `${
			hours !== "00" ? `${hours}:` : ""
		}${minutes}:${seconds}`;

		return formattedTime;
	}

	function format(number) {
		return `${number}`.padStart(2, "0");
	}

	function timerEnded() {
		timer.textContent = "Finished.";
		endBtn.classList.remove("d-none");
		endEarlyForm.classList.add("d-none");

		if (Capabilities.isDesktop) {
			const audio = new Audio(bell1Path);
			audio.play();
		}
	}

	function updateTimer() {
		const now = new Date();
		timeLeft = calcLeft();

		walkingTimeLeft.textContent = `${timeLeft.walking}m`;
		sittingTimeLeft.textContent = `${timeLeft.sitting}m`;
		const walkingPercentage = Math.round(
			100 - (100 * timeLeft.walking) / walking,
		);
		const sittingPercentage = Math.round(
			100 - (100 * timeLeft.sitting) / sitting,
		);

		walkingProgress.style.setProperty("--progress", `${walkingPercentage}%`);
		sittingProgress.style.setProperty("--progress", `${sittingPercentage}%`);

		if (end <= now) {
			timerEnded();
			return;
		}

		const switchedToSitting =
			walking > 0 && timeLeft.walking === 0 && timeLeft.sitting === sitting;

		if (switchedToSitting && Capabilities.isDesktop && !playedSwitchBell) {
			const audio = new Audio(bell1Path);
			audio.play();
			playedSwitchBell = true;
		}

		timer.textContent = diff(end, now);
		setTimeout(() => updateTimer(), 1000);
	}

	function startTimerOrBell() {
		if (
			Capabilities.isMobile &&
			Capabilities.hasLocalStorage &&
			localStorage.getItem("useNativeTimer") === "true"
		) {
			setTimeout(() => startNativeTimer(), 100);
			return;
		}

		const audio = new Audio(bell1Path);
		audio.play();
	}

	function startNativeTimer() {
		if (Capabilities.isAndroid) {
			// https://github.com/yuttadhammo/BodhiTimer/blob/master/fastlane/metadata/android/en-US/changelogs/90.txt#L4
			location.href = `bodhi://timer?times=${timeLeft.walking * 60},${timeLeft.sitting * 60}`;
		}

		if (Capabilities.isIos) {
			// https://www.timerplusapp.com/help/BkG3d6F_d-/
			location.href = `timerplus://app/quick-timers/new?minutes=${timeLeft.walking + timeLeft.sitting}&name=Meditation`;
		}
	}

	/**
	 * @returns {{walking: number, sitting: number}}
	 */
	function calcLeft() {
		const now = new Date();
		const totalTimeMillis = end - now;
		const totalTime = totalTimeMillis / 1000 / 60;

		const sittingLeft = Math.ceil(Math.min(sitting, totalTime));
		const walkingLeft = Math.ceil(
			Math.max(0, Math.min(walking, totalTime - sittingLeft)),
		);

		return {
			walking: walkingLeft,
			sitting: sittingLeft,
		};
	}

	updateTimer();
}
