export function initDropdown() {
	/** @type {HTMLElement[]} */
	const toggles = document.querySelectorAll(".dropdown-toggle");

	for (const toggle of toggles) {
		const dropdown = document.querySelector(toggle.getAttribute("data-target"));
		toggle.addEventListener("click", (evt) => {
			evt.preventDefault();
			dropdown.classList.toggle("show");
		});

		document.addEventListener("click", (event) => {
			if (!dropdown.contains(event.target) && !toggle.contains(event.target)) {
				dropdown.classList.remove("show");
			}
		});
	}
}
