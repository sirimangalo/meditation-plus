import "bootstrap/dist/css/bootstrap.css";
import "@fortawesome/fontawesome-svg-core/styles.css";

import "../css/abstracts/variables.css";

import "../css/components/avatar.css";
import "../css/components/buttons.css";
import "../css/components/callouts.css";
import "../css/components/cards.css";
import "../css/components/circle.css";
import "../css/components/collapse.css";
import "../css/components/dialog.css";
import "../css/components/dropdown.css";
import "../css/components/forms.css";
import "../css/components/home-tabs.css";
import "../css/components/nav.css";
import "../css/components/pagination.css";
import "../css/components/progress.css";
import "../css/components/messages.css";
import "../css/components/session-cards.css";
import "../css/components/tables.css";

import "../css/pages/login.css";

import "../css/main.css";
