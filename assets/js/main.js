import "./styles";
import "./assets";

import { initAvatar } from "./components/avatar";
import { initCollapsibles } from "./components/collapsible";
import { initDialogHandler } from "./components/dialog";
import { initDropdown } from "./components/dropdown";
import { initHeader } from "./components/header";
import { initOnlineCounter } from "./components/online";
import { initFontawesome } from "./vendor/fontawesome";

initHeader();
initDialogHandler();
initFontawesome();
initCollapsibles();
initAvatar();
initDropdown();
initOnlineCounter();
