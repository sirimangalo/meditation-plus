import bell1 from "../audio/bell-1.mp3";
import indexLogo from "../img/index-logo.svg";
import sittingIcon from "../img/sitting.svg";

// Prevent esbuild from Tree Shaking imported assets resulting
// in them not being added to the output directory. Only the asset
// paths are stored in cachedAssets.
window.cachedAssets = [indexLogo, bell1, sittingIcon];
