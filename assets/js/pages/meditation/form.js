import { Capabilities } from "../../components/capabilities";

const nativeTimerCallout = document.getElementById("nativeTimerCallout");
const androidNativeTimer = document.getElementById("androidNativeTimer");
const iosNativeTimer = document.getElementById("iosNativeTimer");
/** @type {HTMLInputElement} */
const useNativeTimer = document.getElementById("useNativeTimer");

if (
	Capabilities.hasLocalStorage &&
	localStorage.getItem("nativeTimerCollapseClosed") === "true"
) {
	nativeTimerCallout.classList.remove("collapse-open");
}

nativeTimerCallout.addEventListener("collapse-toggle", () => {
	Capabilities.hasLocalStorage &&
		localStorage.setItem(
			"nativeTimerCollapseClosed",
			nativeTimerCallout.classList.contains("collapse-open") ? "false" : "true",
		);
});

if (!Capabilities.isMobile) {
	nativeTimerCallout.classList.remove("show");
}

if (Capabilities.isIos) {
	iosNativeTimer.classList.remove("d-none");
}

if (Capabilities.isAndroid) {
	androidNativeTimer.classList.remove("d-none");
}

if (
	Capabilities.hasLocalStorage &&
	localStorage.getItem("useNativeTimer") === "true"
) {
	useNativeTimer.checked = true;
}

useNativeTimer.addEventListener("change", () => {
	Capabilities.hasLocalStorage &&
		localStorage.setItem("useNativeTimer", useNativeTimer.checked);
});
