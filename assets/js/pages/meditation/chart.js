import {
	CategoryScale,
	Chart,
	LineController,
	LineElement,
	LinearScale,
	PointElement,
	Tooltip,
} from "chart.js";

/**
 * @typedef {Object} ServerData
 * @property {string} Day
 * @property {number} Count
 */

/**
 * @param {any}
 * @param {any[]}
 */
function prepend(value, array) {
	const newArray = array.slice();
	newArray.unshift(value);
	return newArray;
}

/**
 * @param {ServerData[]} serverData
 * @returns {ServerData[]}
 */
function fillServerDataWithMonth(serverData) {
	let filledServerData = serverData;

	while (filledServerData.length < 30) {
		const firstEntry = filledServerData[0] ?? { Day: new Date().toString() };
		const firstDate = new Date(firstEntry.Day);
		const newDate = new Date().setDate(firstDate.getDate() - 1);
		filledServerData = prepend(
			{
				Day: newDate,
				Count: 0,
			},
			filledServerData,
		);
	}

	return filledServerData;
}

/** @type {HTMLCanvasElement|null} */
const chart = document.getElementById("chart");

if (chart !== null) {
	/** @type {ServerData[]} */
	let serverData =
		JSON.parse(document.getElementById("chart-data").innerHTML) ?? [];
	serverData = fillServerDataWithMonth(serverData);

	Chart.register(
		LineController,
		CategoryScale,
		LinearScale,
		PointElement,
		LineElement,
		Tooltip,
	);

	new Chart(chart, {
		type: "line",
		options: {
			maintainAspectRatio: false,
			scales: {
				y: {
					ticks: {
						stepSize: 1,
					},
					grid: {
						display: false,
					},
					min: 0,
				},
				x: {
					grid: {
						display: false,
					},
				},
			},
		},
		data: {
			labels: serverData.map((row) => {
				const date = String(new Date(row.Day).getUTCDate());
				return `${date.padStart(2, "0")}.`;
			}),
			datasets: [
				{
					label: "Meditations",
					data: serverData.map((row) => row.Count),
				},
			],
		},
	});
}
