/** @type {HTMLDivElement} */
const quoteContainer = document.getElementById("daily-quote-container");
const quoteDataUrl = quoteContainer.getAttribute("data-quotes-path");
const quoteCollapseExcerp = document
	.getElementById("daily-quote")
	.getElementsByClassName("collapse-excerp")[0];

/** @param {Date} */
function dayOfYear(date) {
	// get the day number (1-365) in the current year
	// see https://stackoverflow.com/a/40975730
	return (
		(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()) -
			Date.UTC(date.getFullYear(), 0, 0)) /
		24 /
		60 /
		60 /
		1000
	);
}

let quotes = undefined;
async function loadDailyQuote() {
	if (!quotes) {
		const resp = await fetch(quoteDataUrl);
		quotes = await resp.json();
	}

	let quoteIndex = dayOfYear(new Date()) - 1;
	if (quoteIndex >= quotes.length) {
		// pick an arbitrary quote for the leap year day
		quoteIndex = new Date().getFullYear() ** 2 % quotes.length;
	}

	quoteContainer.innerHTML = quotes[quoteIndex];
}

quoteCollapseExcerp.addEventListener("click", (evt) => {
	evt.preventDefault();
	loadDailyQuote().catch((err) =>
		console.error("Error loading daily quote", err),
	);
});
