/** @type {HTMLSelectElement} */
const teachers = document.getElementById("teachers");
const appointments = document.querySelectorAll(".appointment");
const weekdayHeadlines = document.querySelectorAll(".weekday-headline");

/** @type {HTMLInputElement|null} */
const showFreeChk = document.getElementById("show-free");
/** @type {HTMLInputElement|null} */
const showTakenChk = document.getElementById("show-taken");

function updateWeekdayHeadlines() {
	for (const headline of weekdayHeadlines) {
		const appointments =
			headline.nextElementSibling.querySelectorAll(".appointment");
		const hiddenAppointments = headline.nextElementSibling.querySelectorAll(
			".appointment.d-none",
		);
		if (appointments.length === hiddenAppointments.length) {
			headline.classList.add("d-none");
			continue;
		}

		headline.classList.remove("d-none");
	}
}

function updateFilter() {
	const teacher = teachers.selectedOptions.item(0);
	localStorage.setItem("teacherSelection", teacher.value);

	if (showFreeChk) {
		localStorage.setItem("showFree", showFreeChk.checked ? "true" : "false");
	}

	if (showTakenChk) {
		localStorage.setItem("showTaken", showTakenChk.checked ? "true" : "false");
	}

	for (let i = 0; i < appointments.length; i++) {
		appointments.item(i).classList.remove("d-none");
	}

	for (let i = 0; i < appointments.length; i++) {
		const row = appointments.item(i);

		const teacherFiltered =
			teacher.value && row.getAttribute("data-teacher-name") !== teacher.value;
		const takenFiltered =
			showTakenChk &&
			!showTakenChk.checked &&
			row.getAttribute("data-taken") === "true";
		const freeFiltered =
			showFreeChk &&
			!showFreeChk.checked &&
			row.getAttribute("data-taken") !== "true";
		if (teacherFiltered || takenFiltered || freeFiltered) {
			row.classList.add("d-none");
		}
	}

	updateWeekdayHeadlines();
}

teachers.addEventListener("change", () => updateFilter());

if (showFreeChk) {
	if (localStorage.getItem("showFree") === "true") {
		showFreeChk.checked = true;
	}
	showFreeChk.addEventListener("change", () => updateFilter());
}
if (showTakenChk) {
	if (localStorage.getItem("showTaken") === "true") {
		showTakenChk.checked = true;
	}
	showTakenChk.addEventListener("change", () => updateFilter());
}

if (localStorage.getItem("teacherSelection")) {
	for (const option of teachers.options) {
		if (option.value === localStorage.getItem("teacherSelection")) {
			teachers.selectedIndex = option.index;
			break;
		}
	}
}

updateFilter();
