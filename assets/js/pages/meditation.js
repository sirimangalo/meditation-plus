import "./meditation/chart.js";
import "./meditation/quotes.js";

import { createDialog } from "../components/dialog";

const vars = document.getElementById("gotmpl-vars");

if (
	vars &&
	vars.getAttribute("data-empty-old-uid") === "true" &&
	window.localStorage !== "undefined" &&
	window.localStorage.getItem("hasNotifiedAboutConvert") !== "true"
) {
	const dialog = createDialog(
		"Convert old data",
		`You haven't used the new meditation import feature to transfer your sessions from the old app. If you imported your sessions via the CSV file, the data may be incomplete. Please use the new import feature for a more accurate transfer. If you didn't have an account in the old app, you can disregard this message.`,
		"Convert",
		"Don't show again",
	);

	dialog.addEventListener("close", () => {
		window.localStorage.setItem("hasNotifiedAboutConvert", "true");

		if (dialog.returnValue === "cancel" || dialog.returnValue === "") {
			dialog.remove();
			return;
		}

		location.href = "/meditations/convert";
	});
	dialog.showModal();
}
