package db

import "embed"

//go:embed *
var Content embed.FS

//go:embed schema.sql
var Schema []byte
