CREATE TABLE users (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(20) NOT NULL UNIQUE,
  email VARCHAR(255) NOT NULL UNIQUE,
  upstream_id VARCHAR(255) NOT NULL UNIQUE,
  role VARCHAR(30) NOT NULL,
  display_name VARCHAR(30) NOT NULL,
  welcomed BOOLEAN DEFAULT false NOT NULL,
  timezone VARCHAR(255),
  image_id VARCHAR(255),
  old_user_uid VARCHAR(255),
  last_activity TIMESTAMP,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

  INDEX activity_idx (last_activity)
);

CREATE TABLE sessions (
  token CHAR(43) PRIMARY KEY,
  data BLOB NOT NULL,
  expiry TIMESTAMP(6) NOT NULL,

  INDEX sessions_expiry_idx (expiry)
);

CREATE TABLE appointments (
  id INT AUTO_INCREMENT PRIMARY KEY,
  time TIME NOT NULL,
  weekday INT NOT NULL,
  timezone VARCHAR(255) NOT NULL,
  reservation_reason VARCHAR(255),
  teacher_id BIGINT NOT NULL,
  user_id BIGINT,
  booked_at TIMESTAMP,
  notified_at TIMESTAMP,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

  INDEX IDX_TEACHER (teacher_id),
  INDEX IDX_USER (user_id),

  CONSTRAINT FK_TEACHER FOREIGN KEY (teacher_id)
    REFERENCES users (id)
    ON DELETE CASCADE,
  CONSTRAINT FK_USER FOREIGN KEY (user_id)
    REFERENCES users (id)
    ON DELETE SET NULL,
  CONSTRAINT TEACHER_ENTRY UNIQUE (time,weekday,teacher_id)
);

CREATE TABLE meditations (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  walking INT(5) NOT NULL,
  sitting INT(5) NOT NULL,
  end TIMESTAMP NOT NULL,
  start TIMESTAMP NOT NULL,
  timezone VARCHAR(255) NOT NULL,
  user_id BIGINT NOT NULL,
  imported BOOLEAN DEFAULT false NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

  INDEX IDX_USER (user_id),

  CONSTRAINT FK_MEDITATION_USER FOREIGN KEY (user_id)
      REFERENCES users (id)
      ON DELETE CASCADE
);

CREATE TABLE migrated_meditations (
  id INT AUTO_INCREMENT PRIMARY KEY,
  walking INT(5) NOT NULL,
  sitting INT(5) NOT NULL,
  end TIMESTAMP NOT NULL,
  start TIMESTAMP NOT NULL,
  timezone VARCHAR(255) NOT NULL,
  user_id VARCHAR(255) NOT NULL,
  INDEX IDX_USER (user_id)
);

CREATE TABLE meditation_stats_daily (
  day DATE PRIMARY KEY,
  walking INT NOT NULL,
  sitting INT NOT NULL,
  count INT NOT NULL,
  walking_all_time BIGINT NOT NULL,
  sitting_all_time BIGINT NOT NULL,
  count_all_time BIGINT NOT NULL
);
