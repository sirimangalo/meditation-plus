-- name: CreateMeditation :execresult
INSERT INTO meditations (walking, sitting, end, start, timezone, user_id, imported, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?);

-- name: UpdateMeditation :execresult
UPDATE meditations SET walking = ?, sitting = ?, end = ?, start = ?, timezone = ?, user_id = ?, imported = ?, created_at = ? WHERE id = ?;

-- name: FindMeditationById :one
SELECT * FROM meditations WHERE id = ?;

-- name: FindActiveMeditationByUserId :one
SELECT * FROM meditations WHERE user_id = ? AND end > ? LIMIT 1;

-- name: FindCurrentMeditations :many
SELECT m.id, m.walking, m.sitting, u.id AS user_id, u.display_name, u.username AS user_name, u.image_id, TIMESTAMPDIFF(MINUTE, sqlc.arg(now), m.end) AS remaining_minutes
  FROM meditations m
  INNER JOIN users u ON u.id = m.user_id
  WHERE m.end > sqlc.arg(now);

-- name: FindPastMeditations :many
SELECT m.id, m.walking, m.sitting, u.id AS user_id, u.display_name, u.username AS user_name, u.image_id, 0 AS remaining_minutes
  FROM meditations m
  INNER JOIN users u ON u.id = m.user_id
  WHERE m.end <= ? AND m.end > ?;

-- name: UserHasMeditationInRange :one
SELECT COUNT(id) FROM meditations WHERE user_id = ? AND start >= ? AND end <= ?;

-- name: DeleteMeditation :exec
DELETE FROM meditations WHERE id = ?;

-- name: DeleteMeditationByUserBeforeDate :exec
DELETE FROM meditations WHERE user_id = ? AND start < ?;

-- name: ConvertMeditations :exec
INSERT INTO meditations (walking, sitting, end, start, timezone, user_id, imported, created_at)
SELECT mm.walking, mm.sitting, mm.end, mm.start, mm.timezone, sqlc.arg(new_user_id), true, NOW()
FROM migrated_meditations mm
WHERE mm.user_id = sqlc.arg(old_user_uid);

-- name: CreateDailyMeditationStats :exec
INSERT INTO meditation_stats_daily (day, walking, sitting, count, walking_all_time, sitting_all_time, count_all_time) VALUES (sqlc.arg(day), 0, 0, 0, 0, 0, 0);

-- name: UpdateDailyMeditationStats :exec
UPDATE meditation_stats_daily
SET
    walking = (SELECT SUM(im.walking) FROM meditations im WHERE DATE(im.start) = DATE(sqlc.arg(day))),
    sitting = (SELECT SUM(im.sitting) FROM meditations im WHERE DATE(im.start) = DATE(sqlc.arg(day))),
    count = (SELECT COUNT(im.id) FROM meditations im WHERE DATE(im.start) = DATE(sqlc.arg(day))),
    walking_all_time = (SELECT SUM(im.walking) FROM meditations im WHERE DATE(im.start) <= DATE(sqlc.arg(day))),
    sitting_all_time = (SELECT SUM(im.sitting) FROM meditations im WHERE DATE(im.start) <= DATE(sqlc.arg(day))),
    count_all_time = (SELECT COUNT(im.id) FROM meditations im WHERE DATE(im.start) <= DATE(sqlc.arg(day)))
WHERE day = DATE(sqlc.arg(day));

-- name: GetDailyMeditationStats :many
SELECT *
  FROM meditation_stats_daily
  WHERE day BETWEEN DATE(sqlc.arg(from_day)) AND DATE(sqlc.arg(to_day));

-- name: CreateMigratedMeditation :execresult
INSERT INTO migrated_meditations (walking, sitting, end, start, timezone, user_id) VALUES (?, ?, ?, ?, ?, ?);

-- name: ListMigratedMeditationsByOldUserUid :many
SELECT * FROM migrated_meditations WHERE user_id = ?;

-- name: ListMigratedMeditationsByOldUserUidNotMatched :many
SELECT mm.*
FROM migrated_meditations mm
INNER JOIN users u ON u.old_user_uid = mm.user_id
LEFT JOIN meditations m
    ON m.walking = mm.walking
    AND m.sitting = mm.sitting
    AND m.start = mm.start
    AND m.end = mm.end
WHERE mm.user_id = ?
  AND m.id IS NULL
LIMIT 300;

-- name: CountUserMeditations :one
SELECT COUNT(id) FROM meditations
  WHERE user_id = ?;

-- name: PaginateUserMeditations :many
SELECT * FROM meditations
  WHERE user_id = ?
  ORDER BY created_at DESC
  LIMIT ? OFFSET ?;
