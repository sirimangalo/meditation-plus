-- name: CreateUser :execresult
INSERT INTO users (
  username,
  email,
  role,
  display_name,
  upstream_id,
  welcomed,
  timezone,
  image_id,
  old_user_uid,
  last_activity,
  created_at
) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);

-- name: UpdateUser :execresult
UPDATE users SET
  username = ?,
  email = ?,
  role = ?,
  display_name = ?,
  upstream_id = ?,
  welcomed = ?,
  timezone = ?,
  image_id = ?,
  old_user_uid = ?,
  last_activity = ?,
  created_at = ?
WHERE id = ?;

-- name: ListUsers :many
SELECT * FROM users;

-- name: FindUserByUsername :one
SELECT * FROM users WHERE username = ?;

-- name: FindUserById :one
SELECT * FROM users WHERE id = ?;

-- name: ListTeacher :many
SELECT * FROM users WHERE role IN (?, ?);

-- name: PaginateUsers :many
SELECT * FROM users
  WHERE username LIKE sqlc.arg(search)
  OR email LIKE sqlc.arg(search)
  OR display_name LIKE sqlc.arg(search)
  LIMIT ? OFFSET ?;

-- name: FindUserByUpsteamId :one
SELECT * FROM users WHERE upstream_id = ?;

-- name: CountUsers :one
SELECT COUNT(id) FROM users
  WHERE username LIKE sqlc.arg(search)
  OR email LIKE sqlc.arg(search)
  OR display_name LIKE sqlc.arg(search);

-- name: GetUserStats :one
SELECT
  CAST(IFNULL(SUM(m.sitting), 0) AS UNSIGNED) AS sitting,
  CAST(IFNULL(SUM(m.walking), 0) AS UNSIGNED) AS walking,
  (SELECT COUNT(*) FROM meditations i WHERE i.user_id = ?) AS sessions
  FROM meditations m WHERE m.user_id = ? AND m.end <= ?;

-- name: DeleteUser :exec
DELETE FROM users WHERE id = ?;

-- name: ListOnlineUsers :many
SELECT * FROM users
  WHERE last_activity IS NOT NULL
  AND last_activity >= UTC_TIMESTAMP() - INTERVAL 10 MINUTE;
