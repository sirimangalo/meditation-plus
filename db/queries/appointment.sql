-- name: CreateAppointment :execresult
INSERT INTO appointments (time, weekday, timezone, reservation_reason, teacher_id, user_id, booked_at, notified_at, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);

-- name: UpdateAppointment :execresult
UPDATE appointments SET time = ?, weekday = ?, timezone = ?, reservation_reason = ?, teacher_id = ?, user_id = ?, booked_at = ?, notified_at = ?, created_at = ? WHERE id = ?;

-- name: ListAppointments :many
SELECT
  sqlc.embed(a),
  u.display_name AS user_display_name,
  t.display_name AS teacher_name,
  t.username AS teacher_username,
  t.image_id AS teacher_image_id,
  u.username AS user_name
FROM appointments a
INNER JOIN users t ON t.id = a.teacher_id
LEFT JOIN users u ON u.id = a.user_id
ORDER BY
  a.weekday, a.time;

-- name: FindAppointmentById :one
SELECT * FROM appointments WHERE id = ?;

-- name: NotifiedAppointment :exec
UPDATE appointments SET notified_at = ? WHERE id = ?;

-- name: BookAppointment :exec
UPDATE appointments SET user_id = ?, booked_at = ?, notified_at = NULL WHERE id = ?;

-- name: UnbookAppointment :exec
UPDATE appointments SET user_id = NULL, booked_at = NULL WHERE id = ? AND user_id = ?;

-- name: UnbookAppointmentWithoutNotification :exec
UPDATE appointments SET user_id = NULL, booked_at = NULL, notified_at = NULL WHERE id = ? AND user_id = ?;

-- name: FindAppointmentByUserId :one
SELECT * FROM appointments WHERE user_id = ? LIMIT 1;

-- name: FindAppointmentByTeacher :many
SELECT
  sqlc.embed(a),
  u.display_name AS user_display_name,
  u.username AS user_name,
  u.image_id AS user_image_id
FROM appointments a
INNER JOIN users t ON t.id = a.teacher_id
LEFT JOIN users u ON u.id = a.user_id
WHERE t.id = ?
ORDER BY weekday, time;

-- name: FindAppointmentSlot :one
SELECT * FROM appointments WHERE time = ? AND weekday = ? AND teacher_id = ?;

-- name: DeleteAppointment :exec
DELETE FROM appointments WHERE id = ?;
