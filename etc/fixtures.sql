INSERT INTO `users` VALUES
(1,'user','testmail@sirimangalo.org','user','USER','user',1,NULL,NULL,NULL,NULL,NOW()),
(2,'teacher','testmail2@sirimangalo.org','teacher','TEACHER','teacher',1,NULL,NULL,NULL,NULL,NOW()),
(3,'admin','testmail3@sirimangalo.org','admin','ADMIN','admin',1,NULL,NULL,NULL,NULL,NOW());

INSERT INTO `appointments` VALUES
(1,'01:00',1,'UTC',NULL,1,NULL,NULL,NULL,NOW()),
(2,'03:00',4,'UTC',NULL,2,1,NULL,NULL,NOW()),
(3,'01:30',2,'UTC',NULL,1,NULL,NULL,NULL,NOW());
