package etc

import "embed"

//go:embed *
var Content embed.FS

//go:embed fixtures.sql
var Fixtures []byte
